# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function
from Network.SimpleNetwork import *

#from Network.CQ.CQNetwork import *
#from Network.CQ.CQDynamicNetwork import *
#from Network.CQ.CQGCNNetwork import *
from Network.DRML.DRMLNetwork import *
#from Network.DRML.DRMLSplitNetwork import *
#from Network.DRML.LSTMDRMLNetwork import *
#from Network.DRML.DRMLDynamicNetwork import *
#from Network.DRML.DRMLNetworkParams import *
#from Network.DRML.DRMLDynamicNetworkParams import *
#from Network.LK.LKNetwork import *
#from Network.LK.MLKNetwork import *
#from Network.LK.CMLKNetwork import *
#from Network.LK.EMLKNetwork import *
#from Network.LK.LKDNetwork import *
#from Network.LK.RLKNetwork import *
#from Network.LK.LKNetworkNoPool import *
#from Network.DPCC.DPCCNetwork import *
#from Network.LK.LKNetworkNoPool import *
#from Network.PN.PointNetwork import *
#from Network.PN.MultiPointNetwork import *
#from Network.PN.GlobalMultiPointNetwork import *
#from Network.PN.GlobalPointNetwork import *
#from Network.PN.AnchorPointNetwork import *
#from Network.DPCC.PNwithDPCCNetwork import *

from Network.LocalSimpleNetwork import *


class BaseNetworkGenerator(object):

    def generateNetwork(self, networkParams, networkDataParams,
                        loadPrevious=False, previousNetworkFilename="",
                        mustFinetune=False, trainableCore=False):

        # Create network
        if networkParams.type == SimpleNetwork.NAME:
            network = SimpleNetwork(networkParams, networkDataParams)

        elif networkParams.type == DRMLNetwork.NAME:
            network = DRMLNetwork(networkParams, networkDataParams)
        #elif networkParams.type == DRMLSplitNetwork.NAME:
        #    network = DRMLSplitNetwork(networkParams, networkDataParams)
        #elif networkParams.type == DRMLDynamicNetwork.NAME:
        #    network = DRMLDynamicNetwork(networkParams, networkDataParams)
        #elif networkParams.type == LSTMDRMLNetwork.NAME:
        #    network = LSTMDRMLNetwork(networkParams, networkDataParams)
        #elif networkParams.type == CQNetwork.NAME:
        #    network = CQNetwork(networkParams, networkDataParams)
        #elif networkParams.type == CQDynamicNetwork.NAME:
        #    network = CQDynamicNetwork(networkParams, networkDataParams)
        #elif networkParams.type == LKNetwork.NAME:
        #    network = LKNetwork(networkParams, networkDataParams)
        #elif networkParams.type == MLKNetwork.NAME:
        #    network = MLKNetwork(networkParams, networkDataParams)
        #elif networkParams.type == CMLKNetwork.NAME:
        #    network = CMLKNetwork(networkParams, networkDataParams)
        # elif networkParams.type == EMLKNetwork.NAME:
        #     network = EMLKNetwork(networkParams, networkDataParams)
        # elif networkParams.type == LKDNetwork.NAME:
        #     network = LKDNetwork(networkParams, networkDataParams)
        # elif networkParams.type == RLKNetwork.NAME:
        #     network = RLKNetwork(networkParams, networkDataParams)
        # elif networkParams.type == DPCCNetwork.NAME:
        #     network = DPCCNetwork(networkParams, networkDataParams)
        # elif networkParams.type == LKNetworkNoPool.NAME:
        #     network = LKNetworkNoPool(networkParams, networkDataParams)
        # elif networkParams.type == PointNetwork.NAME:
        #     network = PointNetwork(networkParams, networkDataParams)
        # elif networkParams.type == GlobalPointNetwork.NAME:
        #     network = GlobalPointNetwork(networkParams, networkDataParams)
        # elif networkParams.type == MultiPointNetwork.NAME:
        #     network = MultiPointNetwork(networkParams, networkDataParams)
        # elif networkParams.type == GlobalMultiPointNetwork.NAME:
        #     network = GlobalMultiPointNetwork(networkParams, networkDataParams)
        # elif networkParams.type == AnchorPointNetwork.NAME:
        #     network = AnchorPointNetwork(networkParams, networkDataParams)
        # elif networkParams.type == PNwithDPCCNetwork.NAME:
        #     network = PNwithDPCCNetwork(networkParams, networkDataParams)

        elif networkParams.type == LocalSimpleNetwork.NAME:
            network = LocalSimpleNetwork(networkParams,networkDataParams)
        else:
            raise ValueError("UNKNOWN NETWORK TYPE REQUESTED:" + networkParams.type)

        # Initialize network
        network.initialize( loadPrevious=loadPrevious,
                            filename=previousNetworkFilename,
                            mustFinetune=mustFinetune,
                            trainableCore=trainableCore)

        # Return network
        return network







