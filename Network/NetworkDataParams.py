# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from Network.CommonConstants import *
from General.BaseParams import *
import numpy as np
import ast
import pickle

OUT_FLAT = "OUT_FLAT"
OUT_MULTICLASS_FLAT = "OUT_MULTICLASS_FLAT"
OUT_IMAGE = "OUT_IMAGE"
OUT_SP = "OUT_SP"


# NOTE: Usually created by the DataGenerators

class NetworkDataParams(BaseParams):
    def __init__(self):
        # Number of frames per sequence
        self.frameCnt = 1

        # Original image size BEFORE resizing in network
        self.origImageHeight = 1
        self.origImageWidth = 1

        # Number of channels in the image
        self.numChannels = 3

        # Number of classes to output
        self.outputCnt = 1

        # Last activation function
        self.lastActivationFunction = ACTIVATE_NOTHING

        # What are the bounds of the label data?
        self.labelBounds = (0, 1)

        # What is the last dimension to be added
        self.fullImageDim = (0, 0)

        self.desiredOutFormat = OUT_FLAT

        # What is the structured prediction size?
        self.s = 1

    '''
    def printParams(self, outputFile=None):
        if outputFile is None:
            print(self.__class__.__name__, ":")
            for attr, value in self.__dict__.items():
                print("\t",attr,value)
        else:
            with open(outputFile, 'w') as myFile:
                myFile.write(self.__class__.__name__ + "\n")
                for attr, value in self.__dict__.items():
                    myFile.write("\t" + str(attr) + " = " + str(value) + "\n")

    def saveParams(self, outputFile):
        with open(outputFile, 'w') as myFile:
            pickle.dump(self.__dict__, myFile)

    def loadParams(self, inputFile):
        with open(inputFile, 'r') as myFile:      
            tmpDict = pickle.load(myFile)    
            self.__dict__.update(tmpDict) 
    '''
    '''

            # Throw away first line
            myFile.readline()

            for attr, value in self.__dict__.items():
                line = myFile.readline()
                tokens = line.split("=")                
                if len(tokens) >= 2:                    
                    print("TYPE:", attr, type(value))
                    newValue = tokens[1].strip()
                    print(attr, newValue)
                    newValue = ast.literal_eval(newValue)
                    #if type(value) is not None and newValue != "None":
                    #    newValue = type(value)(newValue)
                    #else:
                    #    newValue = None

                    self.__dict__[str(attr)] = newValue
    '''


def TEST_NetworkDataParams():
    params = NetworkDataParams()
    params.numChannels = 12

    # Save params
    filename = "test.txt"
    params.saveParams(filename)

    # Load params
    newParams = NetworkDataParams()
    newParams.loadParams(filename)
    newParams.printParams()


if __name__ == "__main__": TEST_NetworkDataParams()




