# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function
from abc import ABCMeta, abstractmethod
from keras.layers import Dense, Activation, TimeDistributed, Conv2D, Reshape, Input, Lambda, Concatenate
from keras.models import Sequential
from keras.models import Model
from keras.models import load_model
import keras
import keras.backend as K
from keras.utils.generic_utils import get_custom_objects
from keras.layers import LeakyReLU
from Network.CommonConstants import *
from Network.BaseNetworkParams import *
from Network.NetworkDataParams import *
from Network.CommonFunctions import *
import Training.CommonFunctions as tcf
import copy
import tensorflow as tf


class BaseNetwork():
    __metaclass__ = ABCMeta

    # DATA ####################################################################

    # netParams = BaseNetworkParams (or any inheriting class)
    # dataParams = NetworkDataParams
    # model = Keras model
    # ready? = boolean indicating whether model is ready or not

    # CONSTRUCTORS ############################################################

    def __init__(self, netParams, dataParams):
        # Store data for class
        self.netParams = copy.deepcopy(netParams)
        self.dataParams = copy.deepcopy(dataParams)

        # NOT ready yet
        self.ready = False

        # Set custom
        self.my_custom_objects = {"tf": tf}

        # Initially, model is None
        self.model = None

    # PUBLIC METHODS ##########################################################

    def getNetworkParams(self):
        return self.netParams

    def getNetworkDataParams(self):
        return self.dataParams

    def getModel(self):
        return self.model

    def isReady(self):
        return self.ready

    def addCustomObjects(self):
        pass

    def addOutputLayer(self, x, nameSuffix=""):

        # Create output layer
        if self.dataParams.desiredOutFormat == OUT_FLAT:
            x = TimeDistributed(Dense(self.dataParams.outputCnt), name="AFTERCORE_lastFlatOuput_" + nameSuffix)(x)
        elif self.dataParams.desiredOutFormat == OUT_MULTICLASS_FLAT:
            x = TimeDistributed(Dense(self.dataParams.outputCnt), name="AFTERCORE_lastFlatOuput_" + nameSuffix)(x)
            x = TimeDistributed(Reshape((self.dataParams.outputCnt // 2, 2)), name="lastReshape_" + nameSuffix)(x)
        elif self.dataParams.desiredOutFormat == OUT_SP:
            # For full patch output
            x = TimeDistributed(
                Dense(self.dataParams.fullImageDim[0] * self.dataParams.fullImageDim[1] * self.dataParams.outputCnt),
                name="AFTERCORE_lastDense_" + nameSuffix)(x)
            x = TimeDistributed(
                Reshape((self.dataParams.fullImageDim[0], self.dataParams.fullImageDim[1], self.dataParams.outputCnt)),
                name="lastReshape_" + nameSuffix)(x)
        else:
            # For full image output
            x = TimeDistributed(Conv2D(filters=self.dataParams.outputCnt, kernel_size=(1, 1)),
                                name="AFTERCORE_lastOutputConv_" + nameSuffix)(x)

        return x

    def addLastActivationFunction(self, x, nameSuffix=""):

        # Last activation function?
        if self.dataParams.lastActivationFunction == ACTIVATE_NOTHING:
            # No activation function or scaling
            # Do nothing
            x = x

        elif self.dataParams.lastActivationFunction == ACTIVATE_SIGMOID:
            # Sigmoid with scaling
            x = TimeDistributed(Activation('sigmoid'), name="lastActivation_" + nameSuffix)(x)

            def scaleValue(x, start, end):
                x *= (end - start)
                x += start
                return x

            x = TimeDistributed(Lambda(scaleValue, arguments={'start': self.dataParams.labelBounds[0],
                                                              'end': self.dataParams.labelBounds[1]}), \
                                name="lastScale_" + nameSuffix)(x)

        elif self.dataParams.lastActivationFunction == ACTIVATE_SOFTMAX:
            # Softmax
            x = TimeDistributed(Activation('softmax'), name="lastActivation_" + nameSuffix)(x)

        elif self.dataParams.lastActivationFunction == ACTIVATE_THRESHOLD:
            # No activation per-se, just scaling
            def clipValue(x, start, end):
                return tf.clip_by_value(x, start, end)

            x = TimeDistributed(Lambda(clipValue, arguments={'start': self.dataParams.labelBounds[0],
                                                             'end': self.dataParams.labelBounds[1]}), \
                                name="lastActivation_" + nameSuffix)(x)
        else:
            raise ValueError("INVALID ACTIVATION FUNCTION!")

        return x

    # OVERRIDE TO CONTROL FINAL OUTPUTS
    def buildCompleteModel(self):
        # Build core model
        coreModel = self.buildCoreModel()

        # Add final outputs
        out = coreModel.output
        out = self.addOutputLayer(out)
        out = self.addLastActivationFunction(out)

        # If using OUT_MULTICLASS_FLAT, have to reshape
        if self.dataParams.desiredOutFormat == OUT_MULTICLASS_FLAT:
            out = TimeDistributed(Reshape((self.dataParams.outputCnt,)), name="finalFINALReshape")(out)

            # Build complete model
        finalModel = Model(inputs=coreModel.input, outputs=out)
        return finalModel

    def finetune(self, newDataParams, trainableCore=False):
        if self.ready:
            # Override values in dataParams
            self.dataParams = copy.deepcopy(newDataParams)

            # Determine last layer of core model
            # (Also, set whether core model is trainable)
            lastLayer = None
            for layer in self.model.layers:
                if "AFTERCORE_" in layer.name:
                    lastLayer = layer
                    break
                else:
                    if type(trainableCore) == type(True):
                        if trainableCore:
                            layer.trainable = True
                        else:
                            layer.trainable = False
                    else:
                        # trainableCore contains a list of trainable layers
                        if layer.name in trainableCore:
                            print("Making layer", layer.name, "trainable...")
                            layer.trainable = True
                        else:
                            layer.trainable = False

                lastLayer = layer

            # Re-add output layer and activation function
            print("Layer before final output:", lastLayer.name)
            out = lastLayer.input
            out = self.addOutputLayer(out)
            out = self.addLastActivationFunction(out)

            # Get original input
            inputLayer = self.model.input

            # Rebuild model
            self.model = Model(inputs=inputLayer, outputs=out)
        else:
            raise ValueError("Model not ready!  Cannot finetune!")

    # NOTE: Should be called before getting model
    def initialize(self, loadPrevious=False, filename="", mustFinetune=False, trainableCore=False):
        # Add any custom objects
        self.addCustomObjects()

        if not loadPrevious:
            print("Building network from scratch...")
            # Create model from scratch
            self.model = self.buildCompleteModel()
        else:
            # Save current data params (just in case)
            newDataParams = copy.deepcopy(self.dataParams)

            # Load from previous model
            print("Loading previously saved model...")
            self.load(filename)

            # Finetune?
            if mustFinetune:
                self.finetune(newDataParams, trainableCore=trainableCore)

        # Is this network trainable?
        if self.netParams.trainable == TRAINABLE_NONE:
            for layer in self.model.layers:
                layer.trainable = False
        elif self.netParams.trainable == TRAINABLE_ALL:
            for layer in self.model.layers:
                layer.trainable = True

        # Ready to roll!
        self.ready = True

    def compile(self, trainParams, classWeights, run_options=None, run_metadata=None):
        if self.ready:
            # Do we want to parallelize this?
            if len(self.netParams.gpuList) > 1:
                self.model = keras.utils.multi_gpu_model(self.model,
                                                         gpus=self.netParams.gpuList,
                                                         cpu_merge=True,
                                                         cpu_relocation=False)

            # Get optimizer and loss function
            optimizer = tcf.getOptimizer(trainParams.optimizerName, trainParams.optimizerParams)
            loss = tcf.getLossFunction(trainParams.loss, classWeights)

            # Compile!
            self.model.compile(optimizer=optimizer,
                               loss=loss,
                               metrics=trainParams.metrics,
                               options=run_options,
                               run_metadata=run_metadata)
        else:
            raise ValueError("CANNOT COMPILE; MODEL NOT READY!")

    def summary(self):
        if self.ready:
            self.model.summary(line_length=200)  # 80)
        else:
            raise ValueError("MODEL NOT READY!")

    '''
    def train(self, trainParams, datagen, trainData, trainLabels):        
        if self.ready:            
            self.model.fit_generator(datagen.flow(trainData, trainLabels,
                                                  batch_size=trainParams.batchSize),
                                steps_per_epoch=len(trainData) / trainParams.batchSize,
                                epochs=trainParams.numEpochs)
        else:
            raise ValueError("MODEL NOT READY!")
    '''

    def train(self, trainParams, trainData, trainLabels, callbacks=None):
        if self.ready:
            self.model.fit(trainData, trainLabels,
                           epochs=trainParams.numEpochs, batch_size=trainParams.batchSize,
                           callbacks=callbacks)
        else:
            raise ValueError("MODEL NOT READY!")

    def trainGen(self, trainParams, trainGen, validGen, callbacks=None):
        if self.ready:
            if validGen is not None:
                validSteps = validGen.getTotalSampleCnt() // trainParams.batchSize
            else:
                validSteps = None

            if not trainParams.useValidationGen:
                print("NOTE: NOT using validation generator by request...")
                validGen = None

            self.model.fit_generator(generator=trainGen,
                                     # steps_per_epoch=trainGen.getTotalSampleCnt() // trainParams.batchSize,
                                     epochs=trainParams.numEpochs,
                                     validation_data=validGen,
                                     callbacks=callbacks,
                                     workers=self.netParams.workers,
                                     # validation_steps=validSteps,
                                     use_multiprocessing=self.netParams.use_multiprocessing)
        else:
            raise ValueError("MODEL NOT READY!")

    def evaluate(self, data, labels, batchSize):
        if self.ready:
            return self.model.evaluate(data, labels, batch_size=batchSize)
        else:
            raise ValueError("MODEL NOT READY!")

    def evaluateGen(self, trainGen):
        if self.ready:
            return self.model.evaluate_generator(trainGen,
                                                 workers=self.netParams.workers,
                                                 use_multiprocessing=self.netParams.use_multiprocessing)
        else:
            raise ValueError("MODEL NOT READY!")

    def predictGen(self, dataGen):
        if self.ready:
            return self.model.predict_generator(dataGen,
                                                workers=self.netParams.workers,
                                                use_multiprocessing=self.netParams.use_multiprocessing)
        else:
            raise ValueError("MODEL NOT READY!")

    def save(self, filename):
        if self.ready:
            self.model.save(filename)
            self.netParams.saveParams(filename[:-3] + ".netParams")
            self.dataParams.saveParams(filename[:-3] + ".dataParams")
        else:
            raise ValueError("Model not ready!")

    def load(self, filename):
        self.model = load_model(filename, custom_objects=self.my_custom_objects)
        self.netParams.loadParams(filename[:-3] + ".netParams")
        self.dataParams.loadParams(filename[:-3] + ".dataParams")
        self.ready = True

    # ABSTRACT METHODS #########################################################

    @abstractmethod
    def buildCoreModel(self):
        pass

    @abstractmethod
    def getNetworkTypeName(self):
        pass






