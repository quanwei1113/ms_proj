# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# INITIAL CODE FROM: https://keras.io/layers/writing-your-own-keras-layers/

from __future__ import absolute_import, print_function

from keras import backend as K
from keras.engine.topology import Layer
from keras.initializers import glorot_uniform, zeros
import numpy as np
from Network.CommonFunctions import *


class DRMLRegionLayer(Layer):

    def __init__(self, output_dim, rowCnt, colCnt, activation, **kwargs):
        self.output_dim = output_dim
        self.rowCnt = rowCnt
        self.colCnt = colCnt
        self.activation = activation
        super(DRMLRegionLayer, self).__init__(**kwargs)

    def get_config(self):
        config = {
            'output_dim': self.output_dim,
            'activation': self.activation,
            'rowCnt': self.rowCnt,
            'colCnt': self.colCnt
        }
        # print("New config:", config) # Print out this dict to see what it looks like.
        base_config = super(PointNetworkTransformLayer, self).get_config()
        # print("Base config:", base_config)
        return dict(list(base_config.items()) + list(config.items()))

    def build(self, input_shape):
        # Compute necessary sizes
        # (Break up image into patches)

        print("INPUT SHAPE:", input_shape)

        # Which index has the channels?
        if K.image_data_format() == 'channels_first':
            channelCnt = input_shape[1]
            self.totalHeight = input_shape[-2]
            self.totalWidth = input_shape[-1]
        else:
            channelCnt = input_shape[-1]
            self.totalHeight = input_shape[-3]
            self.totalWidth = input_shape[-2]

        self.patchWidth = self.totalWidth // self.colCnt
        self.patchHeight = self.totalHeight // self.rowCnt

        # Create necessary weights
        self.weightsList = []
        for row in range(self.rowCnt):
            columnList = []
            for col in range(self.colCnt):
                weightBiasPair = []
                weightBiasPair.append(self.add_weight(shape=[3, 3, channelCnt, self.output_dim],
                                                      initializer=glorot_uniform(),
                                                      name="patch_" + str(row) + "_" + str(col) + "/" + "reg2_W_" + str(
                                                          row) + "_" + str(col)))
                weightBiasPair.append(self.add_weight(shape=(self.output_dim,),
                                                      initializer=zeros(),
                                                      name="patch_" + str(row) + "_" + str(col) + "/" + "reg2_b_" + str(
                                                          row) + "_" + str(col)))
                columnList.append(weightBiasPair)
            self.weightsList.append(columnList)

        super(DRMLRegionLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):

        print("X SHAPE:", x.shape)

        # Which index has the channels?
        if K.image_data_format() == 'channels_first':
            heightIndex = -2
            widthIndex = -1
        else:
            heightIndex = -3
            widthIndex = -2

        print("INDICES:", heightIndex, widthIndex)

        # Break up image into patches
        rowList = []

        for row in range(self.rowCnt):

            columnList = []

            for col in range(self.colCnt):
                # Get patch
                ystart = row * self.patchHeight
                xstart = col * self.patchWidth

                patch = K.slice(x,
                                [0, ystart, xstart, 0],
                                [-1, self.patchHeight, self.patchWidth, -1])

                # print("\t",row,",",col,":")
                # print("\t\t", [0, ystart, xstart, 0])
                # print("\t\t", [-1, self.patchHeight, self.patchWidth, -1])

                # Get mean and variance
                # mean = K.mean(patch, axis=[0, heightIndex, widthIndex], keepdims=True)
                # variance = K.var(patch, axis=[0, heightIndex, widthIndex], keepdims=True)

                # Batch normalize patch and (originally) ReLU

                # normPatch = K.batch_normalization(patch, mean, variance, beta=0.0, gamma=1.0, axis=-1, epsilon=1e-15)
                normPatch = tf.layers.batch_normalization(patch, fused=True)
                relNormPatch = getBackendActivation(self.activation)(normPatch)

                # Convolution
                W_conv = K.conv2d(relNormPatch, self.weightsList[row][col][0], padding='same')
                h_conv = K.bias_add(W_conv, self.weightsList[row][col][1])

                # Append to list for column
                columnList.append(h_conv)

                # columnList.append(patch)

            # Concatenate column
            outputColumn = K.concatenate(columnList, axis=widthIndex)

            # Add to list of rows
            rowList.append(outputColumn)

        # Concatenate for full output
        allPatchesOutput = K.concatenate(rowList, axis=heightIndex)

        # Add skip layer
        fullOutput = allPatchesOutput + x

        return fullOutput

    def compute_output_shape(self, input_shape):
        if K.image_data_format() == 'channels_first':
            return (input_shape[0], self.output_dim, input_shape[1], input_shape[2])
        else:
            return (input_shape[0], input_shape[1], input_shape[2], self.output_dim)
