# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from Network.BaseNetwork import *
from Network.DRML.DRMLRegionLayer import *
from keras.models import Sequential
from keras.layers import InputLayer, Reshape, Dense, Dropout, Flatten, Lambda, TimeDistributed, Activation, Conv2D, \
    MaxPooling2D
from keras.initializers import VarianceScaling, RandomNormal, glorot_normal
from keras import backend as K
import tensorflow as tf


class DRMLNetwork(BaseNetwork):
    # CONSTANTS ###############################################################

    NAME = "DRMLNetwork"

    # Width and height of image used in network.
    # Image will be resized to this if necessary
    INPUT_WIDTH = 170
    INPUT_HEIGHT = 170

    # PUBLIC METHODS ##########################################################

    def buildCoreModel(self):
        # Set initializer
        # default_init = VarianceScaling()
        # default_init = RandomNormal(mean=0, stddev=0.001)
        default_activation = self.netParams.activation

        # Make input layer
        model = Sequential()
        inputShape = (self.dataParams.frameCnt,
                      self.dataParams.origImageHeight,
                      self.dataParams.origImageWidth,
                      self.dataParams.numChannels)
        model.add(InputLayer(input_shape=inputShape, name="Input"))
        inputShapeNoTime = inputShape[1:]

        # Resize input images
        # NOTE: Extra parameters needed because of late-binding for lambdas
        # See: https://github.com/keras-team/keras/issues/9743
        model.add(TimeDistributed(Lambda(input_shape=inputShapeNoTime,
                                         function=lambda images, h=self.INPUT_HEIGHT, w=self.INPUT_WIDTH:
                                         tf.image.resize_images(images, [h, w])), name="Resize"))

        # Add convolutional layers
        model.add(TimeDistributed(Conv2D(filters=32, kernel_size=11, activation=default_activation), name="Conv1"))
        model.add(TimeDistributed(DRMLRegionLayer(32, 8, 8, default_activation), name="DRMLRegionLayer"))
        model.add(TimeDistributed(Activation(default_activation)))
        model.add(TimeDistributed(MaxPooling2D(pool_size=2, strides=2), name="MaxPool"))
        model.add(TimeDistributed(Lambda(lambda inputs, depth_radius=5, alpha=0.0001, beta=0.75:
                                         tf.nn.local_response_normalization(inputs, depth_radius=depth_radius,
                                                                            alpha=alpha, beta=beta)), name="LRN"))
        model.add(TimeDistributed(Conv2D(filters=16, kernel_size=8, activation=default_activation), name="Conv2"))
        model.add(TimeDistributed(Conv2D(filters=16, kernel_size=8, activation=default_activation), name="Conv3"))
        model.add(
            TimeDistributed(Conv2D(filters=16, kernel_size=6, strides=2, activation=default_activation), name="Conv4"))
        model.add(TimeDistributed(Conv2D(filters=16, kernel_size=5, activation=default_activation), name="Conv5"))

        # Add fully-connected layer
        model.add(TimeDistributed(Flatten()))
        model.add(
            TimeDistributed(Dense(self.netParams.firstFCNLayerNodeCnt, activation=default_activation), name="Dense1"))
        model.add(TimeDistributed((Dropout(0.5)), name="Dropout1"))
        model.add(
            TimeDistributed(Dense(self.netParams.secondFCNLayerNodeCnt, activation=default_activation), name="Dense2"))
        model.add(TimeDistributed((Dropout(0.5)), name="Dropout2"))

        return model

    def buildCompleteModel(self):
        print("USING MY OWN BUILD COMPLETE MODEL...")

        # Build core model
        coreModel = self.buildCoreModel()

        # Add final outputs
        out = coreModel.output
        out = self.addOutputLayer(out)
        out = self.addLastActivationFunction(out)

        # Build complete model
        finalModel = Model(inputs=coreModel.input, outputs=out)
        return finalModel

    def getNetworkTypeName(self):
        return self.NAME
