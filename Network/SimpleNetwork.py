# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from Network.BaseNetwork import *
from keras.models import Sequential
from keras.layers import InputLayer, Reshape, Dense, Dropout, Flatten, Lambda, TimeDistributed
from keras.layers import Conv2D, MaxPooling2D
from Training.CommonFunctions import *


class SimpleNetwork(BaseNetwork):
    # CONSTANTS ###############################################################

    NAME = "SimpleNetwork"

    # Width and height of image used in network.
    # Image will be resized to this if necessary
    INPUT_WIDTH = 75
    INPUT_HEIGHT = 75

    # PUBLIC METHODS ##########################################################

    def addCustomObjects(self):
        self.my_custom_objects["multioutput_categorical_crossentropy"] = multioutput_categorical_crossentropy
        self.my_custom_objects["multioutput_categorical_accuracy"] = multioutput_categorical_accuracy

    def buildCoreModel(self):
        # Make input layer
        model = Sequential()
        model.add(InputLayer(input_shape=(self.dataParams.frameCnt,
                                          self.dataParams.origImageHeight,
                                          self.dataParams.origImageWidth,
                                          self.dataParams.numChannels)))

        # Resize input images
        # NOTE: Extra parameters needed because of late-binding for lambdas
        # See: https://github.com/keras-team/keras/issues/9743
        model.add(TimeDistributed(Lambda(lambda images, h=self.INPUT_HEIGHT, w=self.INPUT_WIDTH:
                                         tf.image.resize_images(images, [h, w]))))

        # Add convolutional layers
        model.add(TimeDistributed(Conv2D(filters=32, kernel_size=3, activation='relu')))
        model.add(TimeDistributed(Conv2D(filters=32, kernel_size=3, activation='relu')))
        model.add(TimeDistributed(MaxPooling2D(pool_size=2)))

        model.add(TimeDistributed(Conv2D(filters=64, kernel_size=3, activation='relu')))
        model.add(TimeDistributed(Conv2D(filters=64, kernel_size=3, activation='relu')))
        model.add(TimeDistributed(MaxPooling2D(pool_size=2)))

        # Add fully-connected layer
        model.add(TimeDistributed(Flatten()))
        model.add(TimeDistributed(Dense(256, activation='relu')))
        model.add(TimeDistributed((Dropout(0.5))))

        return model

    def getNetworkTypeName(self):
        return self.NAME






