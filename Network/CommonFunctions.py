# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from keras import backend as K
from keras.utils.generic_utils import get_custom_objects
from keras.layers import LeakyReLU
import numpy as np
import tensorflow as tf


def swish(x):
    return (K.sigmoid(x) * x)


def leakyrelu(x):
    return LeakyReLU()(x)


get_custom_objects().update({'swish': swish})
get_custom_objects().update({'leakyrelu': leakyrelu})

backendActivation = {
    'swish': swish,
    'leakyrelu': leakyrelu,
    'relu': K.relu
}


def getBackendActivation(name):
    return backendActivation[name]


def getDimensions(shape):
    if K.image_data_format() == 'channels_first':
        channelCnt = shape[1]
        totalHeight = shape[-2]
        totalWidth = shape[-1]
    else:
        channelCnt = shape[-1]
        totalHeight = shape[-3]
        totalWidth = shape[-2]

    return totalHeight, totalWidth, channelCnt


# Dimensions of data: [ batch, frameCnt, numClouds, numPoints, numChannels ]
# Dimensions of ind:  [ batch, frameCnt, numClouds, numPoints, (2)*(K nearest neighbor indices)*(2*timeRadius + 1) ]

def getPointGatherIndices(ind, dimCnt):
    # Get shapes
    indShape = ind.get_shape().as_list()

    batchCnt = tf.shape(ind)[0]
    frameCnt = indShape[1]
    cloudCnt = indShape[2]
    pointCnt = indShape[3]
    totalNeighborCnt = indShape[4] / 2
    # dimCnt = dataShape[-1]

    '''
    print("IND SHAPE:", indShape)    
    print("Batch count:", batchCnt)
    print("Frame count:", frameCnt)
    print("Cloud count:", cloudCnt)
    print("Point count:", pointCnt)
    print("Total neighbor count:", totalNeighborCnt)
    print("Dim count:", dimCnt)
    '''

    # Reshape to (timeIndex, pointIndex)
    ind = tf.reshape(ind, (-1, 2))
    # Replicate for dim count
    ind = tf.tile(ind, [1, dimCnt])
    ind = tf.cast(ind, tf.int32)
    ind = tf.reshape(ind, (-1, 2))

    # Split into timeIndex and pointIndex
    timeIndex = tf.slice(ind, [0, 0], [-1, 1])
    pointIndex = tf.slice(ind, [0, 1], [-1, 1])

    # Get dim indices
    dimIndex = tf.range(dimCnt)
    dimIndex = tf.reshape(dimIndex, (dimCnt, 1))
    dimIndex = tf.tile(dimIndex, [batchCnt * frameCnt * cloudCnt * pointCnt * totalNeighborCnt, 1])
    dimIndex = tf.reshape(dimIndex, (-1, 1))

    # Get cloud indices
    cloudIndex = tf.range(cloudCnt)
    cloudIndex = tf.reshape(cloudIndex, (cloudCnt, 1))
    cloudIndex = tf.tile(cloudIndex, [batchCnt * frameCnt, pointCnt * totalNeighborCnt * dimCnt])
    cloudIndex = tf.reshape(cloudIndex, (-1, 1))

    # Get batch indices
    batchIndex = tf.range(batchCnt)
    batchIndex = tf.reshape(batchIndex, (batchCnt, 1))
    batchIndex = tf.tile(batchIndex, [1, cloudCnt * frameCnt * pointCnt * totalNeighborCnt * dimCnt])
    batchIndex = tf.reshape(batchIndex, (-1, 1))

    # Concatenate
    indexList = tf.concat([batchIndex, timeIndex, cloudIndex, pointIndex, dimIndex], axis=1)

    return indexList


def getPointGatherIndicesWithoutDim(ind):
    # Get shapes
    indShape = ind.get_shape().as_list()

    batchCnt = tf.shape(ind)[0]
    frameCnt = indShape[1]
    cloudCnt = indShape[2]
    pointCnt = indShape[3]
    totalNeighborCnt = indShape[4] / 2

    '''
    print("IND SHAPE:", indShape)    
    print("Batch count:", batchCnt)
    print("Frame count:", frameCnt)
    print("Cloud count:", cloudCnt)
    print("Point count:", pointCnt)
    print("Total neighbor count:", totalNeighborCnt)    
    '''

    # Reshape to (timeIndex, pointIndex)
    ind = tf.reshape(ind, (-1, 2))
    # Replicate for dim count
    # ind = tf.tile(ind, [1,dimCnt])
    ind = tf.cast(ind, tf.int32)
    # ind = tf.reshape(ind, (-1,2))

    # Split into timeIndex and pointIndex
    timeIndex = tf.slice(ind, [0, 0], [-1, 1])
    pointIndex = tf.slice(ind, [0, 1], [-1, 1])

    # Get cloud indices
    cloudIndex = tf.range(cloudCnt)
    cloudIndex = tf.reshape(cloudIndex, (cloudCnt, 1))
    cloudIndex = tf.tile(cloudIndex, [batchCnt * frameCnt, pointCnt * totalNeighborCnt])
    cloudIndex = tf.reshape(cloudIndex, (-1, 1))

    # Get batch indices
    batchIndex = tf.range(batchCnt)
    batchIndex = tf.reshape(batchIndex, (batchCnt, 1))
    batchIndex = tf.tile(batchIndex, [1, cloudCnt * frameCnt * pointCnt * totalNeighborCnt])
    batchIndex = tf.reshape(batchIndex, (-1, 1))

    # Concatenate
    indexList = tf.concat([batchIndex, timeIndex, cloudIndex, pointIndex], axis=1)

    return indexList


def getPointsFromGatherIndices(data, indexList, totalNeighborCnt):
    # Get data
    result = tf.gather_nd(data, indexList)

    # Reshape
    dataShape = data.shape
    result = tf.reshape(result, (-1, dataShape[1], dataShape[2], dataShape[3], totalNeighborCnt, dataShape[4]))

    return result


def getPointsFromIndices(data, ind):
    # Get index list
    dataShape = data.get_shape().as_list()
    indexList = getPointGatherIndices(ind, dataShape[-1])

    # Get total neighbor count
    indShape = ind.get_shape().as_list()
    totalNeighborCnt = indShape[4] / 2

    # print("Index shape:", indexList.get_shape().as_list())
    # indexList = tf.Print(indexList, [indexList], "INDEXLIST:", first_n=10)

    return getPointsFromGatherIndices(data, indexList, totalNeighborCnt)


def convolveFilterIndividual(imageTensor, allFilter, allBias, filterHeight, filterWidth, outputDim, isOneFilter):
    # Shape of image tensor: [ batch, height, width, channels ]

    # Get dimensions
    totalHeight, totalWidth, channelCnt = getDimensions(imageTensor.get_shape().as_list())

    # Extract image patches
    imagePatches = tf.extract_image_patches(imageTensor, ksizes=[1, filterHeight, filterWidth, 1], strides=[1, 1, 1, 1],
                                            rates=[1, 1, 1, 1], padding="SAME")

    # Reshape so that output channels in front
    imagePatches = tf.reshape(imagePatches, [1, -1] + imagePatches.get_shape().as_list()[1:])

    # Transpose filter so output channels are first
    allFilter = tf.transpose(allFilter, [4, 0, 1, 2, 3])

    # Is this one filter?
    if isOneFilter:
        # This is for convolving ONE generated filter across a given image/patch.
        #
        # Original shape of filter: [ batch, 1, 1, filterHeight*filterWidth*channels, outChannels]
        # Original shape of bias: [ batch, 1, 1, outChannels]

        # Quick reshape
        allFilter = tf.reshape(allFilter, (outputDim, -1, filterHeight * filterWidth * channelCnt))

        # Multiply and sum
        outMult = tf.einsum("abcde,fbe->bcdf", imagePatches, allFilter)
    else:
        # This is for convolving MULTIPLE generated filters (one for each pixel) across a given image/patch.
        #
        # Original shape of filter: [ batch, height, width, filterHeight*filterWidth*channels, outChannels]
        # Original shape of bias: [ batch, height, width, outChannels]

        # Multiply and sum
        outMult = tf.einsum("abcde,fbcde->bcdf", imagePatches, allFilter)

    print("OUT MULT SHAPE:", outMult.get_shape().as_list())

    # Add bias
    outAdd = tf.add(outMult, allBias)

    return outAdd


def convolveUniquePointFilter(pointTensor, allFilter, allBias):
    # Shape of image tensor: [ batch, height, width, channels ]

    # Get dimensions
    totalHeight, totalWidth, channelCnt = getDimensions(pointTensor.get_shape().as_list())

    # Get output dimensions
    outDim = allFilter.get_shape().as_list()[-1]

    # Reshape so that output channels in front
    reshapedPoints = tf.reshape(pointTensor, [1, -1] + pointTensor.get_shape().as_list()[1:])

    # Transpose filter so output channels are first
    allFilter = tf.transpose(allFilter, [5, 0, 1, 2, 3, 4])

    # Original shape of points: [ batch, frame, cloud, point, neighborPoints*inputChannels ]
    # Original shape of filter: [ batch, frame, cloud, point, neighborPoints*inputChannels, outDim ]
    # Original shape of bias: [ batch, frame, cloud, point, outDim ]

    #                               a/g       b         c       d       e       f
    # Modified shape of points: [   1,        batch,    frame,  cloud,  point,  neighborPoints*inputChannels ]
    # Modified shape of filter: [   outDim,   batch,    frame,  cloud,  point,  neighborPoints*inputChannels ]

    # Multiply and sum
    outMult = tf.einsum("abcdef,gbcdef->bcdeg", reshapedPoints, allFilter)

    print("OUT MULT SHAPE:", outMult.get_shape().as_list())

    # Add bias
    outAdd = tf.add(outMult, allBias)

    return outAdd
