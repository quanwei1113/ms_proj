# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function
# from Network.CommonConstants import *
import numpy as np
import pickle
from General.BaseParams import *

TRAINABLE_NONE = "TRAINABLE_NONE"  # Make NOTHING trainable
TRAINABLE_DEFAULT = "TRAINABLE_DEFAULT"  # Leave as default
TRAINABLE_ALL = "TRAINABLE_ALL"  # Make EVERYTHING trainable


class BaseNetworkParams(BaseParams):
    def __init__(self):
        # Type of network to create
        self.type = ""

        # Is this trainable?
        self.trainable = TRAINABLE_DEFAULT

        # Do it do multi-GPUs?  (An empty list or a list with one item indicates single GPU)
        # NOTE: The integers in this list are RELATIVE GPU ids (depends on visible GPUs)
        self.gpuList = []

        # How many multiprocess workers? (Only works for generator functions)
        self.workers = 20
        # Use multiprocessing? (Only works for generator functions)
        self.use_multiprocessing = True


def TEST_BaseNetworkParams():
    params = BaseNetworkParams()
    params.trainable = TRAINABLE_ALL

    # Save params
    filename = "test.txt"
    params.saveParams(filename)

    # Load params
    newParams = BaseNetworkParams()
    newParams.loadParams(filename)
    newParams.printParams()


if __name__ == "__main__": TEST_BaseNetworkParams()
