# MIT LICENSE
#
# Copyright 2018 Michael J. Reale, Xiaomai Liu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import classification_report
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import OneHotEncoder
import operator
import numpy as np
import scipy

np.set_printoptions(threshold=np.nan)


class ClassificationResults:
    def __init__(self, classNames):
        self.classNames = classNames

        self.allAccuracy = [0] * len(self.classNames)
        self.allF1Scores = [0] * len(self.classNames)
        self.allAUC = [0] * len(self.classNames)
        self.totalSampleCnt = 0
        self.allPositiveCnts = [0] * len(self.classNames)
        self.classificationReport = ""
        self.overallAcc = 0
        self.overallF1_weighted = 0
        self.overallF1_macro = 0
        self.overallF1_micro = 0
        self.confusion = 0

        self.ground = None
        self.pred = None
        self.evaluationFunc = None
        self.labelBounds = (0, 1)
        self.dataName = ""
        self.origClassNames = []

    def computeWeightedAve(self):
        aveAcc = sum(self.allAccuracy * self.allPositiveCnts) / float(sum(self.allPositiveCnts))
        aveF1 = sum(self.allF1Scores * self.allPositiveCnts) / float(sum(self.allPositiveCnts))
        aveAUC = sum(self.allAUC * self.allPositiveCnts) / float(sum(self.allPositiveCnts))
        return aveAcc, aveF1, aveAUC

    def computeUnweightedAve(self):
        aveAcc = sum(self.allAccuracy) / float(len(self.allAccuracy))
        aveF1 = sum(self.allF1Scores) / float(len(self.allF1Scores))
        aveAUC = sum(self.allAUC) / float(len(self.allAUC))
        return aveAcc, aveF1, aveAUC

    def print(self, dataName):
        aveAcc, aveF1, aveAUC = self.computeUnweightedAve()
        waveAcc, waveF1, waveAUC = self.computeWeightedAve()

        print(dataName, "ACCURACY:", self.allAccuracy)
        print(dataName, "AVE. ACCURACY (unweighted):", aveAcc)
        print(dataName, "F1 SCORES:", self.allF1Scores)
        print(dataName, "AVE. F1 SCORE (unweighted):", aveF1)
        print(dataName, "AUC SCORES:", self.allAUC)
        print(dataName, "AVE. AUC SCORE (unweighted):", aveAUC)
        print(dataName, "POSITIVE SAMPLES:", self.allPositiveCnts)
        print(dataName, "TOTAL SAMPLE CNT:", self.totalSampleCnt)

        print(dataName, "W. AVE. ACCURACY:", waveAcc)
        print(dataName, "W. AVE. F1 SCORE:", waveF1)
        print(dataName, "W. AVE. AUC SCORE:", waveAUC)

        print(dataName, "OVERALL ACCURACY (scikit):", self.overallAcc)
        print(dataName, "OVERALL F1 (scikit, weighted):", self.overallF1_weighted)
        print(dataName, "OVERALL F1 (scikit, micro):", self.overallF1_micro)
        print(dataName, "OVERALL F1 (scikit, macro):", self.overallF1_macro)
        print(dataName, "CONFUSION MATRIX:")
        print(self.confusion)
        print(dataName, "CLASSIFICATION REPORT (scikit):")
        print(self.classificationReport)

    def save(self, outputFilename):
        aveAcc, aveF1, aveAUC = self.computeUnweightedAve()

        with open(outputFilename, 'w') as f:
            f.write("Class" + "\t"
                    + "Acc" + "\t"
                    + "F1" + "\t"
                    + "AUC" + "\t"
                    + "Cnts" + "\n")
            for i in range(len(self.allAccuracy)):
                f.write(self.classNames[i] + "\t"
                        + format(self.allAccuracy[i], ".3f") + "\t"
                        + format(self.allF1Scores[i], ".3f") + "\t"
                        + format(self.allAUC[i], ".3f") + "\t"
                        + str(self.allPositiveCnts[i]) + "\n")
            f.write("UW_AVE" + "\t"
                    + format(aveAcc, ".3f") + "\t"
                    + format(aveF1, ".3f") + "\t"
                    + format(aveAUC, ".3f") + "\t"
                    + str(self.totalSampleCnt) + "\n")


class RegressionResults:
    def __init__(self, classNames):
        self.classNames = classNames

        self.allMSE = [0] * len(self.classNames)
        self.allMAE = [0] * len(self.classNames)
        self.allPCC = [(0, 0)] * len(self.classNames)
        self.totalSampleCnt = 0
        self.allPositiveCnts = [0] * len(self.classNames)

    def computeUnweightedAve(self):
        aveMSE = sum(self.allMSE) / float(len(self.allMSE))
        aveMAE = sum(self.allMAE) / float(len(self.allMAE))
        avePCC = sum(np.array(self.allPCC), axis=0) / float(len(self.allPCC))
        return aveMSE, aveMAE, avePCC

    def print(self, dataName):
        aveMSE, aveMAE, avePCC = self.computeUnweightedAve()

        print(dataName, "MSE:", self.allMSE)
        print(dataName, "AVE. MSE (unweighted):", aveMSE)
        print(dataName, "MAE:", self.allMAE)
        print(dataName, "AVE. MAE (unweighted):", aveMAE)
        print(dataName, "PCC:", self.allPCC)
        print(dataName, "AVE. PCC (unweighted):", avePCC)
        print(dataName, "POSITIVE SAMPLES:", self.allPositiveCnts)
        print(dataName, "TOTAL SAMPLE CNT:", self.totalSampleCnt)

    def save(self, outputFilename):
        aveMSE, aveMAE, avePCC = self.computeUnweightedAve()

        # Open file
        with open(outputFilename, 'w') as f:
            f.write("Class" + "\t"
                    + "MSE" + "\t"
                    + "MAE" + "\t"
                    + "PCC" + "\n")

            for i in range(len(self.allMSE)):
                f.write(self.classNames[i] + "\t"
                        + format(self.allMSE[i], ".3f") + "\t"
                        + format(self.allMAE[i], ".3f") + "\t"
                        + format(self.allPCC[i]) + "\n")

            f.write("UW_AVE" + "\t"
                    + format(aveMSE, ".3f") + "\t"
                    + format(aveMAE, ".3f") + "\t"
                    + format(avePCC) + "\t"
                    + str(self.totalSampleCnt) + "\n")


def getAverageResults(allResults):
    if isinstance(allResults[0], ClassificationResults):
        return getAverageClassificationResults(allResults)
    elif isinstance(allResults[0], RegressionResults):
        return getAverageRegressionResults(allResults)
    else:
        raise ValueError("UNKNOWN RESULTS TYPE!")


def getAverageClassificationResults(allResults):
    # Loop through to get list of pred and ground
    allPredList = []
    allGroundList = []
    for i in range(len(allResults)):
        allPredList.append(allResults[i].pred)
        allGroundList.append(allResults[i].ground)

    # Concatenate to get full list
    allPred = np.concatenate(allPredList, axis=0)
    allGround = np.concatenate(allGroundList, axis=0)

    classNames = allResults[0].origClassNames
    evaluate = allResults[0].evaluationFunc
    labelBounds = allResults[0].labelBounds
    dataName = allResults[0].dataName

    aveResults = evaluate(allGround, allPred, dataName, classNames, labelBounds=labelBounds)

    '''
    # OLD VERSION
    # Set things that are constant
    aveResults = ClassificationResults(allResults[0].classNames)    

    # Calculate totals
    for i in range(len(allResults)):
        aveResults.allAccuracy += allResults[i].allAccuracy
        aveResults.allF1Scores += allResults[i].allF1Scores
        aveResults.allAUC += allResults[i].allAUC
        aveResults.totalSampleCnt += allResults[i].totalSampleCnt
        aveResults.allPositiveCnts += allResults[i].allPositiveCnts

    # Average!
    aveResults.allAccuracy = np.array(aveResults.allAccuracy) / float(len(allResults))
    aveResults.allF1Scores = np.array(aveResults.allF1Scores) / float(len(allResults))
    aveResults.allAUC = np.array(aveResults.allAUC) / float(len(allResults))
    '''

    return aveResults


def getAverageRegressionResults(allResults):
    # Set things that are constant
    aveResults = RegressionResults(allResults[0].classNames)

    # Calculate totals
    for i in range(len(allResults)):
        aveResults.allMSE += allResults[i].allMSE
        aveResults.allMAE += allResults[i].allMAE
        aveResults.allPCC += allResults[i].allPCC
        aveResults.totalSampleCnt += allResults[i].totalSampleCnt
        aveResults.allPositiveCnts += allResults[i].allPositiveCnts

    # Average!
    aveResults.allMSE = np.array(aveResults.allMSE) / float(len(allResults))
    aveResults.allMAE = np.array(aveResults.allMAE) / float(len(allResults))
    aveResults.allPCC = np.array(aveResults.allPCC) / float(len(allResults))

    return aveResults


# NOTE: 3D arrays: [sequence][frame][classCnt]

def evaluateExclusiveOutputClassification(allOrigGroundList, allOrigPredictionList, classNames):
    '''
    Classification: Mutually exclusive
    '''

    # Convert to numpy arrays
    allPredictionList = np.array(allOrigPredictionList)
    allGroundList = np.array(allOrigGroundList)

    baseShape = allPredictionList.shape
    print("the shape of allPredictions", baseShape)

    # Get total number of samples
    totalSampleCnt = len(allGroundList) * len(allGroundList[0])
    # NOTE: This might be suspect for structured prediction

    # Create results
    results = ClassificationResults(classNames)
    results.totalSampleCnt = totalSampleCnt

    classCount = len(classNames)

    # Convert the lists to labels instead of categorical output
    allLabelPred = np.argmax(allPredictionList, axis=-1)
    allLabelGround = np.argmax(allGroundList, axis=-1)
    allLabelPred = np.reshape(allLabelPred, (-1))
    allLabelGround = np.reshape(allLabelGround, (-1))

    # print(allLabelGround)

    # Get overall results from scikit
    results.overallAcc = accuracy_score(allLabelGround, allLabelPred)
    results.overallF1_weighted = f1_score(allLabelGround, allLabelPred, average="weighted")
    results.overallF1_micro = f1_score(allLabelGround, allLabelPred, average="micro")
    results.overallF1_macro = f1_score(allLabelGround, allLabelPred, average="macro")

    # For each class...
    for i in range(classCount):
        # Get class-specific answers
        predictionList = allPredictionList[..., i:i + 1]
        groundList = allGroundList[..., i:i + 1]

        # Flatten the lists
        predictionList = np.reshape(predictionList, (-1))
        groundList = np.reshape(groundList, (-1))

        # Get AUC
        try:
            aucScore = roc_auc_score(groundList, predictionList)
        except ValueError as e:
            print(e)
            print("ERROR: AUC is invalid; using 0.5 as a default")
            aucScore = 0.5

        # Get whether this label was predicted
        labelPred = (allLabelPred == i)
        labelGround = (allLabelGround == i)
        allCorrect = sum(labelPred == labelGround)

        # print("Class", i,":", sum(labelPred), sum(labelGround), allCorrect)

        # Get accuracy (actually recall)
        # accuracy = accuracy_score(labelGround, labelPred)
        accuracy = recall_score(labelGround, labelPred)

        # Get F1 score
        aveF1Score = f1_score(labelGround, labelPred)

        # Get all positive sample counts
        positiveCnts = sum(labelGround)

        # Add to lists
        results.allAccuracy[i] = accuracy
        results.allF1Scores[i] = aveF1Score
        results.allAUC[i] = aucScore
        results.allPositiveCnts[i] = positiveCnts

    results.allAccuracy = np.array(results.allAccuracy)
    results.allF1Scores = np.array(results.allF1Scores)
    results.allAUC = np.array(results.allAUC)
    results.allPositiveCnts = np.array(results.allPositiveCnts)
    results.classificationReport = classification_report(allLabelGround, allLabelPred, target_names=classNames)
    results.confusion = confusion_matrix(allLabelGround, allLabelPred)

    return results

    '''
    # OLD VERSION
    enc = OneHotEncoder()
    allPredictionList = np.array(allPredictionList)
    allGroundList = np.array(allGroundList)

    print("the shape of allPredictions", allPredictionList.shape)

    # Debug prints
   #print("allPredictions:", allPredictionList)


    classes = []

    for i in range(len(classNames)):
        classes.append(i)
    classes = np.asarray(classes).reshape(-1,1)
    #print(classes)
    #allPredictionFlattened = allPredictionList.flatten()
    #allPredictionFlattened = np.round(allPredictionFlattened)
    #allGroundFlattened = allGroundList.flatten()
    # Get class count
    #classCnt = len(allGroundList[0][0])
    classCnt = np.asarray(allGroundList[0]).shape[-1]

    # Get total number of samples
    totalSampleCnt = len(allGroundList) * len(allGroundList[0])

    # Convert to numpy arrays
    #allPredictionList = np.array(allPredictionList)
    #allGroundList = np.array(allGroundList)

    # We are assuming that, for each sample and frame, we have N outputs.
    # Each output could be 1 or 0 (Mutually exclusive).
    # SO, we'll split each and compute the individual scores
    realAllPredictionList = np.argmax(allPredictionList, axis=-1)
    realAllGroundList = np.argmax(allGroundList, axis=-1)
    realAllPredictionList = realAllPredictionList.reshape(-1,1)
    realAllGroundList = realAllGroundList.reshape(-1,1)
    #realAllGroundList = realAllGroundList.reshape(-1, allPredictionList.shape[1])
    #realAllPredictionList = realAllPredictionList.reshape(-1, allPredictionList.shape[1])
    #print("realAllGroundList shape ", realAllGroundList.shape)
    #print("realAllPredictionList shape ", realAllPredictionList.shape)
    enc.fit(classes)
    #realAllPredictionList = realAllPredictionList.flatten()
    #realAllGroundList = realAllGroundList.flatten()
    realAllPredictionList = enc.transform(realAllPredictionList.astype(int)).toarray()
    realAllGroundList = enc.transform(realAllGroundList.astype(int)).toarray()

    # Debug prints
    #print("RealAllpredictions:", realAllPredictionList)

    #print("Real all ground shape: ", realAllGroundList.shape)
    #print(realAllPredictionList.shape)
    #print(realAllGroundList.shape)

    results = ClassificationResults(classNames)
    results.totalSampleCnt = totalSampleCnt
    #results.classificationReport = ""

    for i in range(classCnt):
        # Get class-specific answers
        print("checking class",i)
        predictionList = realAllPredictionList[...,i:i+1]
        groundList = realAllGroundList[...,i:i+1]

        # Get accuracy
        accuracy = accuracy_score(groundList, predictionList)

        # Get F1 score
        aveF1Score = f1_score(groundList, predictionList)

        # Get AUC
        try:
            aucScore = roc_auc_score(groundList, predictionList)
        except ValueError as e:
            print(e)
            print("ERROR: AUC is invalid; using 0.5 as a default")
            aucScore = 0.5

        # Get all positive sample counts
        positiveCnts = sum(groundList)

        # Add to lists
        results.allAccuracy[i] = accuracy
        results.allF1Scores[i] = aveF1Score
        results.allAUC[i] = aucScore
        results.allPositiveCnts[i] = positiveCnts


    #print("Accuracy Score: " + str(accuracy_score(allGroundList, allPredictionList)))
    #totalAccuracy = accuracy_score(allGroundFlattened, allPredictionFlattened)

    results.allAccuracy = np.array(results.allAccuracy)
    results.allF1Scores = np.array(results.allF1Scores)
    results.allAUC = np.array(results.allAUC)
    results.allPositiveCnts = np.array(results.allPositiveCnts)
    #print("All ground 1d: ", allGroundList.flatten(), allGroundList.shape)
    #print("All prediction 1d: ", allPredictionList.flatten(), allPredictionList.shape)
    results.classificationReport = classification_report(np.argmax(allGroundList, axis=-1).flatten(), np.argmax(allPredictionList, axis=-1).flatten(), target_names=classNames)
    #print(results.classificationReport)
    #results.classificationReport = classification_report(allGroundList, allPredictionList, target_names=classNames)

    return results
    '''


def evaluateAllExclusiveClassification(allGroundList, allPredictionList, dataName, classNames, labelBounds=(0, 1)):
    '''
    Classification: Mutually exclusive
    '''
    # Do threshold on allPredictionList
    allPredictionList = np.array(allPredictionList)

    nan_sample = np.isnan(allPredictionList)
    if nan_sample.any() == True:
        raise ValueError("A value of the sample is not a number")

    allPredictionList[allPredictionList > labelBounds[1]] = labelBounds[1]
    allPredictionList[allPredictionList < labelBounds[0]] = labelBounds[0]

    print("Number of prediction samples:", len(allPredictionList))
    print("Number of ground samples:", len(allGroundList))

    # Evaluate
    results = evaluateExclusiveOutputClassification(allGroundList, allPredictionList, classNames)

    return results


def evaluateMultiOutputClassification(allGroundList, allPredictionList, classNames):
    '''
    Classification: NOT mutually exclusive
    '''

    # Get class count
    classCnt = len(allGroundList[0][0])

    # Get total number of samples
    totalSampleCnt = len(allGroundList) * len(allGroundList[0])

    # Convert to numpy arrays
    allPredictionList = np.array(allPredictionList)
    allGroundList = np.array(allGroundList)

    # We are assuming that, for each sample and frame, we have N outputs.
    # Each output could be 1 or 0 (NOT mutually exclusive).
    # SO, we'll split each and compute the individual scores

    results = ClassificationResults(classNames)
    results.totalSampleCnt = totalSampleCnt

    for i in range(classCnt):
        # Get class-specific answers
        predictionList = allPredictionList[..., i:i + 1]
        groundList = allGroundList[..., i:i + 1]

        # Flatten the lists
        predictionList = np.reshape(predictionList, (-1))
        groundList = np.reshape(groundList, (-1))

        # Get AUC
        try:
            aucScore = roc_auc_score(groundList, predictionList)
        except ValueError as e:
            print(e)
            print("ERROR: AUC is invalid; using 0.5 as a default")
            aucScore = 0.5

        # Round the predictions
        predictionList = np.round(predictionList)

        # Get accuracy
        accuracy = accuracy_score(groundList, predictionList)

        # Get F1 score
        aveF1Score = f1_score(groundList, predictionList)

        # Get all positive sample counts
        positiveCnts = sum(groundList)

        # Add to lists
        results.allAccuracy[i] = accuracy
        results.allF1Scores[i] = aveF1Score
        results.allAUC[i] = aucScore
        results.allPositiveCnts[i] = positiveCnts

    results.allAccuracy = np.array(results.allAccuracy)
    results.allF1Scores = np.array(results.allF1Scores)
    results.allAUC = np.array(results.allAUC)
    results.allPositiveCnts = np.array(results.allPositiveCnts)

    return results


def evaluateAllMultiClassification(allGroundList, allPredictionList, dataName, classNames, labelBounds=(0, 1)):
    '''
    Classification: NOT mutually exclusive
    '''

    # Do threshold on allPredictionList
    allPredictionList = np.array(allPredictionList)
    allPredictionList[allPredictionList > labelBounds[1]] = labelBounds[1]
    allPredictionList[allPredictionList < labelBounds[0]] = labelBounds[0]

    print("Number of prediction samples:", len(allPredictionList))
    print("Number of ground samples:", len(allGroundList))

    # Evaluate
    results = evaluateMultiOutputClassification(allGroundList, allPredictionList, classNames)

    return results


def evaluateMultiOutputRegression(allGroundList, allPredictionList, classNames):
    '''
    Regression: MULTIPLE possible outputs
    '''

    # Get class count
    classCnt = len(allGroundList[0][0])

    # Get total number of samples
    totalSampleCnt = len(allGroundList) * len(allGroundList[0])

    # Convert to numpy arrays
    allPredictionList = np.array(allPredictionList)
    allGroundList = np.array(allGroundList)

    # We are assuming that, for each sample and frame, we have N outputs.
    # Each output is some kind of regression output (each output is not linked to the other).
    # SO, we'll split each and compute the individual scores.

    results = RegressionResults(classNames)
    results.totalSampleCnt = totalSampleCnt

    for i in range(classCnt):
        # Get class-specific answers
        predictionList = allPredictionList[..., i:i + 1]
        groundList = allGroundList[..., i:i + 1]

        # Flatten lists
        predictionList = np.reshape(predictionList, (-1))
        groundList = np.reshape(groundList, (-1))

        # MSE
        MSE = mean_squared_error(groundList, predictionList)

        # MAE
        MAE = mean_absolute_error(groundList, predictionList)

        # PCC
        PCC = scipy.stats.pearsonr(groundList, predictionList)

        # Get all positive sample counts
        positiveCnts = sum(groundList)

        # Add to lists
        results.allMSE[i] = MSE
        results.allMAE[i] = MAE
        results.allPCC[i] = PCC
        results.allPositiveCnts[i] = positiveCnts

    results.allMSE = np.array(results.allMSE)
    results.allMAE = np.array(results.allMAE)
    results.allPCC = np.array(results.allPCC)
    results.allPositiveCnts = np.array(results.allPositiveCnts)

    return results


def evaluateAllRegression(allGroundList, allPredictionList, dataName, classNames, labelBounds=(0, 1)):
    '''
    Regression: MULTIPLE possible outputs
    '''

    # Do threshold on allPredictionList
    allPredictionList = np.array(allPredictionList)
    allPredictionList[allPredictionList > labelBounds[1]] = labelBounds[1]
    allPredictionList[allPredictionList < labelBounds[0]] = labelBounds[0]

    print("Label bounds:", labelBounds)

    # Evaluate
    results = evaluateMultiOutputRegression(allGroundList, allPredictionList, classNames)

    return results


def evaluateIndExclusiveOutputClassification(allOrigGroundList, allOrigPredictionList, classNames):
    '''
    Classification: Multiple classes, but each is a binary mutually exclusive output [off, on]
    '''

    # Convert to numpy arrays
    allPredictionList = np.array(allOrigPredictionList)
    allGroundList = np.array(allOrigGroundList)

    baseShape = allPredictionList.shape
    print("the shape of allPredictions", baseShape)

    # Change from shape [-1, ... , 2*classCnt]
    # to [-1, classCnt, 2]
    classCnt = baseShape[-1]
    trueClassCount = classCnt // 2
    newBaseShape = list(baseShape[:-1]) + [trueClassCount, 2]

    allPredictionList = np.reshape(allPredictionList, newBaseShape)
    allGroundList = np.reshape(allGroundList, newBaseShape)

    print("new shape of allPredictions", allPredictionList.shape)

    # Get total number of samples
    totalSampleCnt = len(allGroundList) * len(allGroundList[0])
    # NOTE: This might be suspect for structured prediction

    # Create new list of class names
    newClassNames = classNames[1::2]

    # Create results
    results = ClassificationResults(newClassNames)
    results.totalSampleCnt = totalSampleCnt

    # Get overall results from scikit
    allLabelPred = np.argmax(allPredictionList, axis=-1)
    allLabelGround = np.argmax(allGroundList, axis=-1)
    # print("BEFORE SHAPE:", allLabelPred.shape)
    allLabelPred = np.reshape(allLabelPred, (-1, trueClassCount))
    allLabelGround = np.reshape(allLabelGround, (-1, trueClassCount))

    # print("SHAPE:", allLabelPred.shape)
    # print(allLabelGround)
    # print(allLabelPred)
    results.overallAcc = accuracy_score(allLabelGround, allLabelPred)
    results.overallF1_weighted = f1_score(allLabelGround, allLabelPred, average="weighted")
    results.overallF1_micro = f1_score(allLabelGround, allLabelPred, average="micro")
    results.overallF1_macro = f1_score(allLabelGround, allLabelPred, average="macro")

    # For each class...
    for i in range(trueClassCount):
        # Get class-specific answers
        predictionList = allPredictionList[..., i:i + 1, 0:2]
        groundList = allGroundList[..., i:i + 1, 0:2]

        # Convert the lists to labels instead of categorical output
        labelPred = np.argmax(predictionList, axis=-1)
        labelGround = np.argmax(groundList, axis=-1)

        # Flatten the lists
        labelPred = np.reshape(labelPred, (-1))
        labelGround = np.reshape(labelGround, (-1))

        # Get probability for AUC
        predictionList = np.reshape(predictionList, (-1, 2))
        groundList = np.reshape(groundList, (-1, 2))
        onProbPred = predictionList[..., 1:2]
        onProbGround = groundList[..., 1:2]

        # Get AUC
        try:
            aucScore = roc_auc_score(onProbGround, onProbPred)
        except ValueError as e:
            print(e)
            print("ERROR: AUC is invalid; using 0.5 as a default")
            aucScore = 0.5

        # Get accuracy
        accuracy = accuracy_score(labelGround, labelPred)

        # Get F1 score
        aveF1Score = f1_score(labelGround, labelPred)

        # Get all positive sample counts
        positiveCnts = sum(labelGround)

        # Add to lists
        results.allAccuracy[i] = accuracy
        results.allF1Scores[i] = aveF1Score
        results.allAUC[i] = aucScore
        results.allPositiveCnts[i] = positiveCnts

    results.allAccuracy = np.array(results.allAccuracy)
    results.allF1Scores = np.array(results.allF1Scores)
    results.allAUC = np.array(results.allAUC)
    results.allPositiveCnts = np.array(results.allPositiveCnts)

    return results


def evaluateAllIndExclusiveClassification(allGroundList, allPredictionList, dataName, classNames, labelBounds=(0, 1)):
    '''
    Classification: Multiple classes, but each is a binary mutually exclusive output [off, on]
    '''
    # Do threshold on allPredictionList
    allPredictionList = np.array(allPredictionList)

    nan_sample = np.isnan(allPredictionList)
    if nan_sample.any() == True:
        raise ValueError("A value of the sample is not a number")

    allPredictionList[allPredictionList > labelBounds[1]] = labelBounds[1]
    allPredictionList[allPredictionList < labelBounds[0]] = labelBounds[0]

    print("Number of prediction samples:", len(allPredictionList))
    print("Number of ground samples:", len(allGroundList))

    # Evaluate
    results = evaluateIndExclusiveOutputClassification(allGroundList, allPredictionList, classNames)
    # TODO: Until this is working, use the regular version
    # results = evaluateExclusiveOutputClassification(allGroundList, allPredictionList, classNames)

    return results
