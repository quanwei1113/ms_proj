# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from sklearn.model_selection import KFold
import datetime
import os.path
import shutil
import copy
from Training.AssessmentFunctions import *
from Training.CommonFunctions import *
from keras.callbacks import TensorBoard
from keras.callbacks import LearningRateScheduler


def evaluateOnDataset(dataName, dataGen, network, trainOutputDirs, trialIndex, foldIndex, writeToExperimentDrive):
    # Get the predictions
    pred = network.predictGen(dataGen)
    nan_sample = np.isnan(pred)
    if nan_sample.any() == True:
        raise ValueError("A value of the pred is not a number")

    # Get the ground truth
    ground = dataGen.getEntireLabelset()

    # Print out some data
    perClassCnts = np.sum(np.sum(ground, axis=0), axis=0)
    print(dataName + " PER-CLASS COUNTS:", perClassCnts)
    print(dataName + " TOTAL NUMBER OF SAMPLES:", dataGen.getTotalSampleCnt())

    # Get class names
    classNames = dataGen.getParams().desiredClasses

    # Get the assessment functions
    evaluate = dataGen.getEvaluationFunction()

    # Evaluate!

    results = evaluate(ground, pred, dataName, classNames, labelBounds=dataGen.getLabelValueBounds())

    # Save ground, prediction, and evaluation function
    results.ground = ground
    results.pred = pred
    results.evaluationFunc = evaluate
    results.labelBounds = dataGen.getLabelValueBounds()
    results.dataName = dataName
    results.origClassNames = classNames

    # Print
    results.print(dataName)

    # Get fold string
    foldString = trainOutputDirs.getFoldBasename(trialIndex=trialIndex, foldIndex=foldIndex)
    if len(foldString) > 0:
        foldString = "_" + foldString

        # Save
    if writeToExperimentDrive:
        saveResults(trainOutputDirs.getResultsPath(), dataName, foldString, results)

    return pred, ground, results


def saveResults(outputPath, dataName, foldString, results):
    # Get full filename
    resultsFilename = outputPath + "/RESULTS_" + dataName + foldString + ".tsv"

    # Save
    results.save(resultsFilename)


def saveGroundAndPred(outputPath, dataType, foldString, ground, pred):
    np.save(outputPath + "/" + dataType + "_GROUND" + foldString + ".npy", ground)
    np.save(outputPath + "/" + dataType + "_PRED" + foldString + ".npy", pred)


def saveOtherData(outputPath, dataType, foldString, dataGen):
    allSubjects = dataGen.getAllSubjects()
    allTasks = dataGen.getAllTasks()
    allFilenames = dataGen.getAllFilenames()

    allSubjects = np.array(allSubjects)
    allTasks = np.array(allTasks)
    allFilenames = np.array(allFilenames)

    np.save(outputPath + "/" + dataType + "_SUBJECTS" + foldString + ".npy", allSubjects)
    np.save(outputPath + "/" + dataType + "_TASKS" + foldString + ".npy", allTasks)
    np.save(outputPath + "/" + dataType + "_FILENAMES" + foldString + ".npy", allFilenames)


'''
# OLD VERSION
def performTrainingOneFold( networkParams, networkGenerator, trainParams,
                            baseDataParams, DataGeneratorType, dataPreprocessParams,
                            trainSubjects, validSubjects, testSubjects, 
                            totalSubjectCnt,
                            trainOutputDirs,
                            trialIndex,
                            foldIndex, 
                            writeToExperimentDrive):

    print(datetime.datetime.now().date())
    print(datetime.datetime.now().time())

    # Create data generators
    trainGen, trainNoAugGen, validGen, testGen = createAndPrepareDataGenerators(    baseDataParams, DataGeneratorType, 
                                                                                    dataPreprocessParams,
                                                                                    trainSubjects=trainSubjects, 
                                                                                    validSubjects=validSubjects, 
                                                                                    testSubjects=testSubjects, 
                                                                                    totalSubjectCnt=totalSubjectCnt,
                                                                                    batchSize=trainParams.batchSize,
                                                                                    useSamplingOnTest=trainParams.useSamplingOnTest,
                                                                                    useSamplingOnValid=trainParams.useSamplingOnValid)

    # Get class weights
    classWeights = trainGen.getClassWeights()

    # Create Tensorflow session that limits it to only the visible GPUs
    print("before setting device list")
    gpu_options = tf.GPUOptions(visible_device_list = trainParams.visibleDevices)
    print(trainParams.visibleDevices)
    config = tf.ConfigProto(gpu_options=gpu_options)
    #config.gpu_options.visible_device_list = trainParams.visibleDevices

    with tf.Session(config=config) as sess:
        # Set session
        K.set_session(sess)

        # Initialize variables (BECAUSE OF A STUPID BUG IN KERAS)
        #sess.run(tf.global_variables_initializer())

        # Get metadata
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()

        # Create network
        networkDataParams = trainGen.generateNetworkDataParams()    
        network = networkGenerator.generateNetwork(networkParams, networkDataParams)

        if writeToExperimentDrive:
            # Create Tensorboard instance
            if trainParams.trainType == TRAIN_TYPE_CROSS_VALID:
                foldString = "TRIAL_" + str(trialIndex) + "_FOLD_" + str(foldIndex)
            else:
                foldString = "TRAINING" 

            tensorboardPath = trainOutputDirs.getTensorboardPath() + "/" + foldString
            removeDir(tensorboardPath)
            createDir(tensorboardPath)
            #tensorboard = TensorBoard(log_dir=tensorboardPath, **trainParams.TensorboardParams)
            tensorboard = CustomTensorboard(log_dir=tensorboardPath, run_metadata=run_metadata, **trainParams.TensorboardParams)            
            callbacks = [tensorboard]
        else:
            callbacks = None

        # Compile network
        network.compile(trainParams, classWeights, run_options, run_metadata)

        # Print model summary
        network.summary()

        # Train model
        network.trainGen(trainParams, trainGen, validGen, callbacks=callbacks)

        # Evaluate
        trainPred, trainGround, trainResults = evaluateOnDataset("TRAINING", trainNoAugGen, network, trainOutputDirs, trialIndex=trialIndex, foldIndex=foldIndex, writeToExperimentDrive=writeToExperimentDrive)
        trainScores = network.evaluateGen(trainNoAugGen)
        print("TRAINING SCORES:", trainScores)

        validPred, validGround, validResults = evaluateOnDataset("VALIDATION", validGen, network, trainOutputDirs, trialIndex=trialIndex, foldIndex=foldIndex, writeToExperimentDrive=writeToExperimentDrive)
        validScores = network.evaluateGen(validGen)
        print("VALIDATION SCORES:", validScores)

        testPred, testGround, testResults = evaluateOnDataset("TESTING", testGen, network, trainOutputDirs, trialIndex=trialIndex, foldIndex=foldIndex, writeToExperimentDrive=writeToExperimentDrive)
        testScores = network.evaluateGen(testGen)
        print("TESTING SCORES:", testScores)

        # Get trial/fold string
        foldString = trainOutputDirs.getFoldBasename(trialIndex=trialIndex, foldIndex=foldIndex)
        if len(foldString) > 0:
            foldString = "_" + foldString

        # Saving model
        if writeToExperimentDrive:
            network.save(trainOutputDirs.getWeightsPath() + "/Model" + foldString + ".h5")        
            print("Model saved.")
            # Save preprocessed mean/std/pca as well
            if trainGen.getDataPreprocessor() is not None:
                trainGen.getDataPreprocessor().save(trainOutputDirs.getWeightsPath() + "/Preprocess" + foldString + ".npz")

        # Save ground and prediction data
        if writeToExperimentDrive:
            outputPath = trainOutputDirs.getOutputPath()
            saveGroundAndPred(outputPath, "TRAIN", foldString, trainGround, trainPred)
            saveGroundAndPred(outputPath, "VALID", foldString, validGround, validPred)
            saveGroundAndPred(outputPath, "TEST", foldString, testGround, testPred)

            saveOtherData(outputPath, "TRAIN", foldString, trainNoAugGen)
            saveOtherData(outputPath, "VALID", foldString, validGen)
            saveOtherData(outputPath, "TEST", foldString, testGen)

    # Clear session
    K.clear_session()

    return trainResults, validResults, testResults
'''


def performTrainingOneFoldUpdated(
        networkParams, networkGenerator, trainParams,
        allDataGen, useDataAugmentation, dataPreprocessParams,
        trainSubjects, validSubjects, testSubjects,
        trainOutputDirs,
        trialIndex,
        foldIndex,
        writeToExperimentDrive,
        loadPreviousNetwork,
        previousNetworkFilename,
        mustFinetune,
        trainableCore):
    print(datetime.datetime.now().date())
    print(datetime.datetime.now().time())

    # Create data generators
    trainGen, trainNoAugGen, validGen, testGen = createAndPrepareDataViews(
        allDataGen, useDataAugmentation, dataPreprocessParams,
        trainSubjects, validSubjects, testSubjects,
        trainParams.batchSize)
    # useSamplingOnTest=trainParams.useSamplingOnTest,
    # useSamplingOnValid=trainParams.useSamplingOnValid)

    # Get class weights
    classWeights = trainGen.getClassWeights()

    # Create Tensorflow session that limits it to only the visible GPUs
    print("before setting device list")
    gpu_options = tf.GPUOptions(visible_device_list=trainParams.visibleDevices)
    print(trainParams.visibleDevices)
    config = tf.ConfigProto(gpu_options=gpu_options)
    # config.gpu_options.visible_device_list = trainParams.visibleDevices

    with tf.Session(config=config) as sess:
        # Set session
        K.set_session(sess)

        # Initialize variables (BECAUSE OF A STUPID BUG IN KERAS)
        # sess.run(tf.global_variables_initializer())

        # Get metadata
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()

        # Create network
        networkDataParams = trainGen.generateNetworkDataParams()
        network = networkGenerator.generateNetwork(networkParams, networkDataParams,
                                                   loadPrevious=loadPreviousNetwork,
                                                   previousNetworkFilename=previousNetworkFilename,
                                                   mustFinetune=mustFinetune, trainableCore=trainableCore)

        if writeToExperimentDrive:
            # Create Tensorboard instance
            if trainParams.trainType == TRAIN_TYPE_CROSS_VALID:
                foldString = "TRIAL_" + str(trialIndex) + "_FOLD_" + str(foldIndex)
            else:
                foldString = "TRAINING"

            tensorboardPath = trainOutputDirs.getTensorboardPath() + "/" + foldString
            removeDir(tensorboardPath)
            createDir(tensorboardPath)
            # tensorboard = TensorBoard(log_dir=tensorboardPath, **trainParams.TensorboardParams)
            # tensorboard = CustomTensorboard(log_dir=tensorboardPath, **trainParams.TensorboardParams)
            tensorboard = CustomTensorboard(log_dir=tensorboardPath,
                                            log_every=1,
                                            log_epochs_only=True,
                                            run_metadata=run_metadata,
                                            **trainParams.TensorboardParams)
            callbacks = [tensorboard]
        else:
            callbacks = None

        # Add learning rate scheduler if requested
        if trainParams.learningRateFunc is not None:
            lrate = LearningRateScheduler(trainParams.learningRateFunc)
            if callbacks is None:
                callbacks = [lrate]
            else:
                callbacks += [lrate]

        # Compile network
        network.compile(trainParams, classWeights, run_options, run_metadata)

        # Print model summary
        network.summary()

        # Train model
        network.trainGen(trainParams, trainGen, validGen, callbacks=callbacks)

        # Evaluate
        trainPred, trainGround, trainResults = evaluateOnDataset("TRAINING", trainNoAugGen, network, trainOutputDirs,
                                                                 trialIndex=trialIndex, foldIndex=foldIndex,
                                                                 writeToExperimentDrive=writeToExperimentDrive)
        trainScores = network.evaluateGen(trainNoAugGen)
        print("TRAINING SCORES:", trainScores)

        validPred, validGround, validResults = evaluateOnDataset("VALIDATION", validGen, network, trainOutputDirs,
                                                                 trialIndex=trialIndex, foldIndex=foldIndex,
                                                                 writeToExperimentDrive=writeToExperimentDrive)
        validScores = network.evaluateGen(validGen)
        print("VALIDATION SCORES:", validScores)

        testPred, testGround, testResults = evaluateOnDataset("TESTING", testGen, network, trainOutputDirs,
                                                              trialIndex=trialIndex, foldIndex=foldIndex,
                                                              writeToExperimentDrive=writeToExperimentDrive)
        testScores = network.evaluateGen(testGen)
        print("TESTING SCORES:", testScores)

        # Get trial/fold string
        foldString = trainOutputDirs.getFoldBasename(trialIndex=trialIndex, foldIndex=foldIndex)
        if len(foldString) > 0:
            foldString = "_" + foldString

        # Saving model
        if writeToExperimentDrive:
            network.save(trainOutputDirs.getWeightsPath() + "/Model" + foldString + ".h5")
            print("Model saved.")
            # Save preprocessed mean/std/pca as well
            if trainGen.getDataPreprocessor() is not None:
                trainGen.getDataPreprocessor().save(
                    trainOutputDirs.getWeightsPath() + "/Preprocess" + foldString + ".npz")

        # Save ground and prediction data
        if writeToExperimentDrive:
            outputPath = trainOutputDirs.getOutputPath()
            saveGroundAndPred(outputPath, "TRAIN", foldString, trainGround, trainPred)
            saveGroundAndPred(outputPath, "VALID", foldString, validGround, validPred)
            saveGroundAndPred(outputPath, "TEST", foldString, testGround, testPred)

            saveOtherData(outputPath, "TRAIN", foldString, trainNoAugGen)
            saveOtherData(outputPath, "VALID", foldString, validGen)
            saveOtherData(outputPath, "TEST", foldString, testGen)

    # Clear session
    K.clear_session()

    return trainResults, validResults, testResults


'''
# OLD VERSION
def performTraining(    networkParams, networkGenerator, trainParams,
                        baseDataParams, DataGeneratorType, outputDirectory, dataPreprocessParams,
                        trainSubjects=None, validSubjects=None, testSubjects=None,
                        writeToExperimentDrive=True):

    print("YOUR TRAINING BEGINS NOW!")

    if writeToExperimentDrive is False:
        print("")
        print("*******************************************************")
        print("*******************************************************")
        print("WARNING: writeToExperimentDrive is FALSE!!!!")
        print("THIS MEANS THAT NOTHING WILL BE SAVED!!!!")
        print("YOU HAVE BEEN WARNED!!!!")
        print("*******************************************************")
        print("*******************************************************")
        print("")

    startTime = datetime.datetime.now()
    print(startTime)

    # Grab all subjects
    allSubjects = copy.deepcopy(baseDataParams.validSubjects)
    allSubjects = np.array(allSubjects)
    totalSubjectCnt = len(allSubjects)

    # Create directories
    trainOutputDirs = TrainingRunDirectories(outputDirectory, networkParams.type, baseDataParams, trainParams) 
    if writeToExperimentDrive:
        trainOutputDirs.initialize()  

    # Save params
    if writeToExperimentDrive:
        baseDataParams.printParams(trainOutputDirs.getBaseOutputPath() + "/" + "DataLoadParams.txt") 
        networkParams.printParams(trainOutputDirs.getBaseOutputPath() + "/" + "NetworkParams.txt") 
        trainParams.printParams(trainOutputDirs.getBaseOutputPath() + "/" + "TrainingParams.txt") 
        with open(trainOutputDirs.getBaseOutputPath() + "/" + "DataPreprocessParams.txt", 'w') as myFile:
            myFile.write("DataPreprocessParams:" + "\n")
            for attr, value in dataPreprocessParams.items():
                myFile.write("\t" + str(attr) + " = " + str(value) + "\n")

    # Are we doing cross-validation?
    if trainParams.trainType == TRAIN_TYPE_CROSS_VALID:
        # Set seed
        subjectSeed = trainParams.kFoldStartSeed

        # For each trial...
        for trialIndex in range(1, (trainParams.totalTrialCnt+1)):
            print("TRIAL", trialIndex)

            # Prepare K-fold cross-validation
            kf = prepareKFold(allSubjects, trainParams.totalFoldCnt, True, subjectSeed)

            # Increment seed
            subjectSeed += 1

            # Set up loop variables
            foldIndex = 1

            # For each fold...
            listTrainResults = []
            listValidResults = []
            listTestResults = []
            for train_index, test_index in kf.split(allSubjects):
                print("TRAINING ON FOLD", foldIndex)

                print(train_index, test_index)

                # Get subjects
                trainSubjects = allSubjects[train_index]
                validSubjects = allSubjects[train_index]
                testSubjects = allSubjects[test_index]   

                # Call training functions
                trainResults, validResults, testResults = performTrainingOneFold(   networkParams, networkGenerator, trainParams,
                                                                                    baseDataParams, DataGeneratorType, dataPreprocessParams,
                                                                                    trainSubjects, validSubjects, testSubjects, 
                                                                                    totalSubjectCnt,
                                                                                    trainOutputDirs,
                                                                                    trialIndex=trialIndex,
                                                                                    foldIndex=foldIndex,
                                                                                    writeToExperimentDrive=writeToExperimentDrive)

                listTrainResults.append(trainResults)
                listValidResults.append(validResults)
                listTestResults.append(testResults)

                foldIndex += 1

        # Get average results
        allTrainResults = getAverageResults(listTrainResults)
        allValidResults = getAverageResults(listValidResults)
        allTestResults = getAverageResults(listTestResults) 

    elif trainParams.trainType == TRAIN_TYPE_TRAIN_TEST:
        # Just have locked training and testing set
        print("TRAINING ON TRAIN/TEST SPLIT")

        if trainSubjects is None or validSubjects is None or testSubjects is None:
            raise ValueError("ONE OF THE SUBJECT LISTS IS NONE!")

        # Call training functions
        allTrainResults, allValidResults, allTestResults = performTrainingOneFold(   networkParams, networkGenerator, trainParams,
                                                                                        baseDataParams, DataGeneratorType, dataPreprocessParams,
                                                                                        trainSubjects=trainSubjects, 
                                                                                        validSubjects=validSubjects, 
                                                                                        testSubjects=testSubjects, 
                                                                                        totalSubjectCnt=totalSubjectCnt,
                                                                                        trainOutputDirs=trainOutputDirs,
                                                                                        trialIndex=1,
                                                                                        foldIndex=1,
                                                                                        writeToExperimentDrive=writeToExperimentDrive)

    elif trainParams.trainType == TRAIN_TYPE_PRE_TRAIN:
        # Just training on ONE fold, and splitting off part of it for validation

        # Prepare K-fold cross-validation
        kf = prepareKFold(allSubjects, trainParams.totalFoldCnt, True, trainParams.kFoldStartSeed)

        # Just get first fold
        for train_index, test_index in kf.split(allSubjects):

            print("PRE-TESTING")
            print("BEFORE:", train_index, test_index)


            # Split training indices
            numTrainingSubjects = len(train_index)
            numValidSubjects = int(float(numTrainingSubjects)*trainParams.validationSplit)
            numTrainingSubjects = numTrainingSubjects - numValidSubjects

            newTrainIndex = []
            newTestIndex = []

            for index in range(numTrainingSubjects):
                newTrainIndex.append(train_index[index])

            for index in range(numValidSubjects):
                newTestIndex.append(train_index[numTrainingSubjects + index])

            train_index = newTrainIndex
            test_index = newTestIndex

            print("AFTER:", train_index, test_index)

            # Get subjects
            trainSubjects = allSubjects[train_index]
            validSubjects = allSubjects[test_index]
            testSubjects = allSubjects[test_index]   

            # Call training functions
            allTrainResults, allValidResults, allTestResults = performTrainingOneFold(   networkParams, networkGenerator, trainParams,
                                                                                        baseDataParams, DataGeneratorType, dataPreprocessParams,
                                                                                        trainSubjects, validSubjects, testSubjects, 
                                                                                        totalSubjectCnt,
                                                                                        trainOutputDirs,
                                                                                        trialIndex=1,
                                                                                        foldIndex=1,
                                                                                        writeToExperimentDrive=writeToExperimentDrive)

            # Break out! (Only doing first fold)
            break

    else:
        raise ValueError("UNKNOWN TRAINING TYPE!")

    endTime = datetime.datetime.now()

    # Print overall results
    print("**************************************************************")
    print("TOTAL RESULTS")
    allTrainResults.print("OVERALL_TRAIN")
    allValidResults.print("OVERALL_VALID")
    allTestResults.print("OVERALL_TEST")

    # Save overall results
    if writeToExperimentDrive:
        saveResults(trainOutputDirs.getResultsPath(), "OVERALL_TRAIN", "", allTrainResults)
        saveResults(trainOutputDirs.getResultsPath(), "OVERALL_VALID", "", allValidResults)
        saveResults(trainOutputDirs.getResultsPath(), "OVERALL_TEST", "", allTestResults)

    diffTime = endTime - startTime
    printTimeTaken(diffTime)

    return allTrainResults, allValidResults, allTestResults
'''


def performTrainingUpdated(networkParams, networkGenerator, trainParams,
                           outputDirectory,
                           allDataGen,
                           useDataAugmentation,
                           dataPreprocessParams,
                           trainSubjects=None, validSubjects=None, testSubjects=None,
                           writeToExperimentDrive=True,
                           loadPreviousNetwork=False,
                           previousNetworkFilename="",
                           mustFinetune=False,
                           trainableCore=False):
    print("YOUR TRAINING BEGINS NOW!")

    if writeToExperimentDrive is False:
        print("")
        print("*******************************************************")
        print("*******************************************************")
        print("WARNING: writeToExperimentDrive is FALSE!!!!")
        print("THIS MEANS THAT NOTHING WILL BE SAVED!!!!")
        print("YOU HAVE BEEN WARNED!!!!")
        print("*******************************************************")
        print("*******************************************************")
        print("")

    startTime = datetime.datetime.now()
    print(startTime)

    # Get data params
    baseDataParams = allDataGen.getParams()

    # Grab all subjects
    allSubjects = copy.deepcopy(baseDataParams.validSubjects)
    allSubjects = np.array(allSubjects)
    totalSubjectCnt = len(allSubjects)

    # Create directories
    trainOutputDirs = TrainingRunDirectories(outputDirectory, networkParams.type, baseDataParams, trainParams)
    if writeToExperimentDrive:
        trainOutputDirs.initialize()

        # Save params
    if writeToExperimentDrive:
        baseDataParams.printParams(trainOutputDirs.getBaseOutputPath() + "/" + "DataLoadParams.txt")
        networkParams.printParams(trainOutputDirs.getBaseOutputPath() + "/" + "NetworkParams.txt")
        trainParams.printParams(trainOutputDirs.getBaseOutputPath() + "/" + "TrainingParams.txt")
        with open(trainOutputDirs.getBaseOutputPath() + "/" + "DataPreprocessParams.txt", 'w') as myFile:
            myFile.write("DataPreprocessParams:" + "\n")
            for attr, value in dataPreprocessParams.items():
                myFile.write("\t" + str(attr) + " = " + str(value) + "\n")

    # Are we doing cross-validation?
    if trainParams.trainType == TRAIN_TYPE_CROSS_VALID:
        # Set seed
        subjectSeed = trainParams.kFoldStartSeed

        # List of all results
        listTrainResults = []
        listValidResults = []
        listTestResults = []

        # For each trial...
        for trialIndex in range(1, (trainParams.totalTrialCnt + 1)):
            print("TRIAL", trialIndex)

            # Prepare K-fold cross-validation
            kf = prepareKFold(allSubjects, trainParams.totalFoldCnt, True, subjectSeed)

            # Increment seed
            subjectSeed += 1

            # Set up loop variables
            foldIndex = 1

            for train_index, test_index in kf.split(allSubjects):
                print("TRAINING ON FOLD", foldIndex)

                print(train_index, test_index)

                # Get subjects
                trainSubjects = allSubjects[train_index]
                validSubjects = allSubjects[test_index]
                testSubjects = allSubjects[test_index]

                # Call training functions
                trainResults, validResults, testResults = performTrainingOneFoldUpdated(
                    networkParams=networkParams,
                    networkGenerator=networkGenerator,
                    trainParams=trainParams,
                    allDataGen=allDataGen,
                    useDataAugmentation=useDataAugmentation,
                    dataPreprocessParams=dataPreprocessParams,
                    trainSubjects=trainSubjects,
                    validSubjects=validSubjects,
                    testSubjects=testSubjects,
                    trainOutputDirs=trainOutputDirs,
                    trialIndex=trialIndex,
                    foldIndex=foldIndex,
                    writeToExperimentDrive=writeToExperimentDrive,
                    loadPreviousNetwork=loadPreviousNetwork,
                    previousNetworkFilename=previousNetworkFilename,
                    mustFinetune=mustFinetune,
                    trainableCore=trainableCore)

                listTrainResults.append(trainResults)
                listValidResults.append(validResults)
                listTestResults.append(testResults)

                foldIndex += 1

        # Get average results
        allTrainResults = getAverageResults(listTrainResults)
        allValidResults = getAverageResults(listValidResults)
        allTestResults = getAverageResults(listTestResults)

    elif trainParams.trainType == TRAIN_TYPE_TRAIN_TEST:
        # Just have locked training and testing set
        print("TRAINING ON TRAIN/TEST SPLIT")

        if trainSubjects is None or validSubjects is None or testSubjects is None:
            raise ValueError("ONE OF THE SUBJECT LISTS IS NONE!")

        # Call training functions
        allTrainResults, allValidResults, allTestResults = performTrainingOneFoldUpdated(
            networkParams=networkParams,
            networkGenerator=networkGenerator,
            trainParams=trainParams,
            allDataGen=allDataGen,
            useDataAugmentation=useDataAugmentation,
            dataPreprocessParams=dataPreprocessParams,
            trainSubjects=trainSubjects,
            validSubjects=validSubjects,
            testSubjects=testSubjects,
            trainOutputDirs=trainOutputDirs,
            trialIndex=1,
            foldIndex=1,
            writeToExperimentDrive=writeToExperimentDrive,
            loadPreviousNetwork=loadPreviousNetwork,
            previousNetworkFilename=previousNetworkFilename,
            mustFinetune=mustFinetune,
            trainableCore=trainableCore)

    elif trainParams.trainType == TRAIN_TYPE_PRE_TRAIN:
        # Just training on ONE fold, and splitting off part of it for validation

        # Prepare K-fold cross-validation
        kf = prepareKFold(allSubjects, trainParams.totalFoldCnt, True, trainParams.kFoldStartSeed)

        # Just get first fold
        for train_index, test_index in kf.split(allSubjects):

            print("PRE-TESTING")
            print("BEFORE:", train_index, test_index)

            # Split training indices
            numTrainingSubjects = len(train_index)
            numValidSubjects = int(float(numTrainingSubjects) * trainParams.validationSplit)
            numTrainingSubjects = numTrainingSubjects - numValidSubjects

            newTrainIndex = []
            newTestIndex = []

            for index in range(numTrainingSubjects):
                newTrainIndex.append(train_index[index])

            for index in range(numValidSubjects):
                newTestIndex.append(train_index[numTrainingSubjects + index])

            train_index = newTrainIndex
            test_index = newTestIndex

            print("AFTER:", train_index, test_index)

            # Get subjects
            trainSubjects = allSubjects[train_index]
            validSubjects = allSubjects[test_index]
            testSubjects = allSubjects[test_index]

            # Call training functions
            allTrainResults, allValidResults, allTestResults = performTrainingOneFoldUpdated(
                networkParams=networkParams,
                networkGenerator=networkGenerator,
                trainParams=trainParams,
                allDataGen=allDataGen,
                useDataAugmentation=useDataAugmentation,
                dataPreprocessParams=dataPreprocessParams,
                trainSubjects=trainSubjects,
                validSubjects=validSubjects,
                testSubjects=testSubjects,
                trainOutputDirs=trainOutputDirs,
                trialIndex=1,
                foldIndex=1,
                writeToExperimentDrive=writeToExperimentDrive,
                loadPreviousNetwork=loadPreviousNetwork,
                previousNetworkFilename=previousNetworkFilename,
                mustFinetune=mustFinetune,
                trainableCore=trainableCore)
            # Break out! (Only doing first fold)
            break

    else:
        raise ValueError("UNKNOWN TRAINING TYPE!")

    endTime = datetime.datetime.now()

    # Print overall results
    print("**************************************************************")
    print("TOTAL RESULTS")
    allTrainResults.print("OVERALL_TRAIN")
    allValidResults.print("OVERALL_VALID")
    allTestResults.print("OVERALL_TEST")

    # Save overall results
    if writeToExperimentDrive:
        saveResults(trainOutputDirs.getResultsPath(), "OVERALL_TRAIN", "", allTrainResults)
        saveResults(trainOutputDirs.getResultsPath(), "OVERALL_VALID", "", allValidResults)
        saveResults(trainOutputDirs.getResultsPath(), "OVERALL_TEST", "", allTestResults)

    diffTime = endTime - startTime
    printTimeTaken(diffTime)

    return allTrainResults, allValidResults, allTestResults
