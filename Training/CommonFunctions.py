# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

import datetime
import os.path
import shutil
import copy
import keras.callbacks
import tensorflow as tf
from functools import partial, update_wrapper
from sklearn.model_selection import KFold
from keras.optimizers import Adam, Adagrad, SGD
from Training.TrainingParams import *
from Data.DataPreprocessor import *
from Data.Data3DPreprocessor import *
from Data.DataLoadParams import *
from Data.BP4DDataGenerator import *
from Data.BPPlusDataGenerator import *
from Data.DRIVEDataGenerator import *
# from Data.UNBCDataGenerator import *
from Data.CASME2DataGenerator import *
from Data.BU3DDataGenerator import *
from Data.BU4DDataGenerator import *
from Data.DataViewGenerator import *
from Data.DataViewParams import *
import keras.losses

DATABASE_BP4D_COLOR = "BP4D_COLOR"
DATABASE_BP4D_DEPTH = "BP4D_DEPTH"
DATABASE_BP4D_NORMAL = "BP4D_NORMAL"
DATABASE_BP4D_H5 = "BP4D_H5"
DATABASE_BP4D_3D = "BP4D_3D"
DATABASE_BP4D_PLUS = "BP4D_PLUS"
DATABASE_UNBC_COLOR = "UNBC_COLOR"
DATABASE_CASME2_COLOR = "CASME2_COLOR"
DATABASE_DRIVE = "DRIVE"
DATABASE_RITE = "RITE"
DATABASE_CASME2_3D = "CASME2_3D"
DATABASE_BU3D_3D = "BU3D_3D"
DATABASE_BU4D_3D = "BU4D_3D"

MACHINE_BIG_BEAR = "BIG_BEAR"
MACHINE_LITTLE_BEAR = "LITTLE_BEAR"
MACHINE_SANGUINIUS = "SANGUINIUS"
MACHINE_PCWEIQUAN = "PCWEIQUAN"  # FOR LOCAL


def createDir(fullPath):
    directory = os.path.dirname(fullPath + "/")
    if not os.path.exists(directory):
        os.makedirs(directory)


def removeDir(fullPath):
    directory = os.path.dirname(fullPath + "/")
    if os.path.exists(directory):
        shutil.rmtree(directory, ignore_errors=True)


class TrainingRunDirectories:

    ###############################################################################
    # CONSTRUCTOR AND PUBLIC METHODS
    ###############################################################################

    def __init__(self, dirPath, networkName, dataParams, trainParams):
        # Save directory path
        self.dirPath = dirPath

        # Save params
        self.networkName = networkName
        self.dataParams = dataParams
        self.trainParams = trainParams

        # Get basename
        self.basename = self.__deriveCoreBasename()

        # Get output directories
        self.baseOutputPath = self.dirPath + "/" + self.basename
        self.resultsPath = self.baseOutputPath + "/RESULTS"
        self.outputPath = self.baseOutputPath + "/OUTPUTS"
        self.tensorboardPath = self.baseOutputPath + "/TENSORBOARD"
        self.weightsPath = self.baseOutputPath + "/WEIGHTS"
        self.checkPointsPath = self.baseOutputPath + "/CHECKPOINTS"

        print("BASE OUTPUT PATH:", self.baseOutputPath)
        print("RESULTS PATH:", self.resultsPath)
        print("OUTPUT PATH:", self.outputPath)
        print("TENSORBOARD PATH:", self.tensorboardPath)
        print("WEIGHTS PATH:", self.weightsPath)
        print("CHECKPOINTS PATH:", self.checkPointsPath)

    def initialize(self):
        self.__prepareOutputDirectories()

    def getFoldBasename(self, trialIndex, foldIndex):
        fullBasename = ""
        if self.trainParams.trainType == TRAIN_TYPE_CROSS_VALID:
            fullBasename = "TRIAL_" + str(trialIndex) + "_FOLD_" + str(foldIndex) + "_" + self.basename

        return fullBasename

    def getResultsPath(self):
        return self.resultsPath

    def getWeightsPath(self):
        return self.weightsPath

    def getOutputPath(self):
        return self.outputPath

    def getBaseOutputPath(self):
        return self.baseOutputPath

    def getTensorboardPath(self):
        return self.tensorboardPath

    ###############################################################################
    # PRIVATE METHODS
    ###############################################################################

    def __deriveCoreBasename(self):
        basename = ""
        if self.trainParams.trainType == TRAIN_TYPE_CROSS_VALID:
            basename += "TOTALTRIALS_" + str(self.trainParams.totalTrialCnt) + "_TOTALFOLDS_" + str(
                self.trainParams.totalFoldCnt)
        elif self.trainParams.trainType == TRAIN_TYPE_PRE_TRAIN:
            basename += "PRETRAIN_TOTALTRIALS_" + str(self.trainParams.totalTrialCnt) + "_TOTALFOLDS_" + str(
                self.trainParams.totalFoldCnt)

        basename += "_" + self.networkName + "_" + self.dataParams.desiredData
        return basename

    def __prepareOutputDirectories(self):

        # Remove Tensorboard directory first
        removeDir(self.tensorboardPath)

        # Create directories if they don't exist
        createDir(self.baseOutputPath)
        createDir(self.resultsPath)
        createDir(self.outputPath)
        createDir(self.tensorboardPath)
        createDir(self.weightsPath)
        createDir(self.checkPointsPath)


###############################################################################
# CUSTOM TENSORBOARD CALLBACK
###############################################################################

# Code from: https://github.com/keras-team/keras/issues/6692

class CustomTensorboard(keras.callbacks.TensorBoard):
    def __init__(self, log_every=1, log_epochs_only=False, run_metadata=None, **kwargs):
        super(CustomTensorboard, self).__init__(**kwargs)
        self.log_every = log_every
        self.counter = 0
        self.log_epochs_only = log_epochs_only
        self.run_metadata = run_metadata

    def on_batch_end(self, batch, logs=None):
        self.counter += 1
        if self.counter % self.log_every == 0 and not self.log_epochs_only:
            for name, value in logs.items():
                if name in ['batch', 'size']:
                    continue
                summary = tf.Summary()
                summary_value = summary.value.add()
                summary_value.simple_value = value.item()
                summary_value.tag = name
                # print(name, " --> ", value.item())
                self.writer.add_summary(summary, self.counter)
            self.writer.flush()

        super(CustomTensorboard, self).on_batch_end(batch, logs)

    def set_model(self, model):
        super(CustomTensorboard, self).set_model(model)

    def on_epoch_end(self, epoch, logs=None):
        print("in common functions")

        index = epoch
        if not self.log_epochs_only:
            # Override epoch as counter
            index = self.counter

        super(CustomTensorboard, self).on_epoch_end(index, logs)

        # Save run options
        if self.run_metadata is not None:
            print("Writing meta...")
            self.writer.add_run_metadata(self.run_metadata, 'Epoch_%d' % epoch)


###############################################################################
# FUNCTIONS
###############################################################################

def getBaseDirectoryPath(machine, desiredDatabase):
    if (desiredDatabase == DATABASE_BP4D_COLOR or
            desiredDatabase == DATABASE_BP4D_DEPTH or
            desiredDatabase == DATABASE_BP4D_NORMAL):
        # BP4D
        # Determine specific kind of data
        if desiredDatabase == DATABASE_BP4D_COLOR:
            subDataType = "COLOR"
        elif desiredDatabase == DATABASE_BP4D_DEPTH:
            subDataType = "DEPTH"
        elif desiredDatabase == DATABASE_BP4D_NORMAL:
            subDataType = "NORMAL"

        if machine == MACHINE_BIG_BEAR:
            baseDir = "/media/Data2/BP4D/125-wide/" + subDataType
        elif machine == MACHINE_LITTLE_BEAR:
            baseDir = "/media/Data/BP4D/125-wide/" + subDataType
        elif machine == MACHINE_SANGUINIUS:
            baseDir = "D:/Data/75-wide/" + subDataType
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BP4D_H5:
        if machine == MACHINE_BIG_BEAR:
            baseDir = "/media/Data2/BP4D-Pose"
        elif machine == MACHINE_LITTLE_BEAR:
            baseDir = "/media/Data/BP4D/BP4D-Pose"
        elif machine == MACHINE_SANGUINIUS:
            baseDir = "D:/Data/75-wide"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BP4D_3D:
        if machine == MACHINE_BIG_BEAR:
            baseDir = "/mnt/Core/BP4D/Sequences"
        elif machine == MACHINE_LITTLE_BEAR:
            baseDir = "/media/Core/BP4D/Sequences"
        elif machine == MACHINE_SANGUINIUS:
            baseDir = "D:/Data/75-wide"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BP4D_PLUS:
        if machine == MACHINE_BIG_BEAR:
            baseDir = "/mnt/BPPlus1/Sessions1"
        elif machine == MACHINE_LITTLE_BEAR:
            baseDir = ""
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_UNBC_COLOR:
        # UNBC

        if machine == MACHINE_BIG_BEAR:
            baseDir = "/media/Data2/UNBC/TrackerOutputPNG"
        elif machine == MACHINE_LITTLE_BEAR:
            baseDir = "/media/Data/UNBC/TrackerOutputPNG"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_CASME2_COLOR:
        # CASME2 - Color
        if machine == MACHINE_BIG_BEAR:
            baseDir = "/media/Data2/CASME2/CASME2-RAW"
        elif machine == MACHINE_LITTLE_BEAR:
            # baseDir = "/media/Data/CASME2/CASME2-RAW"
            # TODO: PUT THIS BACK!!!!!
            baseDir = "/media/Data/CASME2-NORM_256"
        elif machine == MACHINE_PCWEIQUAN:
            # FOR LOCAL
            baseDir = "C:/MSData/Cropped"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_CASME2_3D:
        if machine == MACHINE_LITTLE_BEAR:
            baseDir = "/media/Data/CASME2-NORM_256"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif (desiredDatabase == DATABASE_DRIVE or
          desiredDatabase == DATABASE_RITE):
        if machine == MACHINE_BIG_BEAR:
            baseDir = "/media/Data2/DRIVE/patches"
        elif machine == MACHINE_LITTLE_BEAR:
            baseDir = "/media/Data/DRIVE/patches"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BU3D_3D:
        if machine == MACHINE_BIG_BEAR:
            baseDir = "/media/Data2/BU3D/BU_3DFE"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BU4D_3D:
        if machine == MACHINE_BIG_BEAR:
            baseDir = "/media/Data2/BU4D/BU4DFE"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    return baseDir


def getTbndDirectoryPath(machine, desiredDatabase):
    if desiredDatabase == DATABASE_BP4D_3D:
        if machine == MACHINE_BIG_BEAR:
            tbndBaseDir = "/mnt/Core/BP4D/new3DFeatures"
        elif machine == MACHINE_LITTLE_BEAR:
            tbndBaseDir = "/media/Data/BP4D/new3DFeatures"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BP4D_PLUS:
        if machine == MACHINE_BIG_BEAR:
            tbndBaseDir = "/mnt/BPPlus1/Final3DFP"
        elif machine == MACHINE_LITTLE_BEAR:
            tbndBaseDir = ""
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BU3D_3D:
        if machine == MACHINE_BIG_BEAR:
            tbndBaseDir = "/media/Data2/BU3D/BU_3DFE"
        elif machine == MACHINE_LITTLE_BEAR:
            tbndBaseDir = "/media/Data/BU3D/BU_3DFE"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_CASME2_3D:
        if machine == MACHINE_LITTLE_BEAR:
            tbndBaseDir = "/media/Data/CASME2-NORM_256"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BU4D_3D:
        if machine == MACHINE_BIG_BEAR:
            tbndBaseDir = "/media/Data2/BU4D/BU4DFE_BND_V1.1"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    else:
        raise ValueError("NO TBND LOCATION SET FOR THIS DATABASE")

    return tbndBaseDir


def getBaseGroundPath(machine, desiredDatabase):
    if (desiredDatabase == DATABASE_BP4D_COLOR or
            desiredDatabase == DATABASE_BP4D_DEPTH or
            desiredDatabase == DATABASE_BP4D_NORMAL or
            desiredDatabase == DATABASE_BP4D_H5 or
            desiredDatabase == DATABASE_BP4D_3D):
        # BP4D

        if machine == MACHINE_BIG_BEAR:
            baseGroundPath = "/media/Data1/BP4D/AUCoding"
        elif machine == MACHINE_LITTLE_BEAR:
            baseGroundPath = "/media/Data/BP4D/AUCoding"
        elif machine == MACHINE_SANGUINIUS:
            baseGroundPath = "D:/Data/AUCoding"
        elif machine == MACHINE_PCWEIQUAN:
            baseGroundPath = "C:/MSData"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BP4D_PLUS:
        if machine == MACHINE_BIG_BEAR:
            baseGroundPath = "/mnt/BPPlus1/AU_AfterReIndex"
        elif machine == MACHINE_LITTLE_BEAR:
            baseGroundPath = ""
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_UNBC_COLOR:
        # UNBC

        if machine == MACHINE_BIG_BEAR:
            baseGroundPath = "/media/Data2/UNBC/Frame_Labels"
        elif machine == MACHINE_LITTLE_BEAR:
            baseGroundPath = "/media/Data/UNBC/Frame_Labels"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_CASME2_COLOR:
        # CASME2 - Color

        if machine == MACHINE_BIG_BEAR:
            baseGroundPath = "/media/Data2/CASME2"
        elif machine == MACHINE_LITTLE_BEAR:
            baseGroundPath = "/media/Data/CASME2"
        elif machine == MACHINE_PCWEIQUAN:
            baseGroundPath = "C:/MSData"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_CASME2_3D:
        if machine == MACHINE_LITTLE_BEAR:
            baseGroundPath = "/media/Data/CASME2"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif (desiredDatabase == DATABASE_DRIVE or
          desiredDatabase == DATABASE_RITE):
        if machine == MACHINE_BIG_BEAR:
            baseGroundPath = "/media/Data2/DRIVE/patches"
        elif machine == MACHINE_LITTLE_BEAR:
            baseGroundPath = "/media/Data/DRIVE/patches"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    elif desiredDatabase == DATABASE_BU4D_3D:
        if machine == MACHINE_BIG_BEAR:
            baseGroundPath = "/media/Data2/BU4D/BU4D_GroundTruths"
        else:
            raise ValueError("INVALID MACHINE TYPE")

    return baseGroundPath


def getLostFileList(machine, desiredDatabase):
    if (desiredDatabase == DATABASE_BP4D_COLOR or
            desiredDatabase == DATABASE_BP4D_DEPTH or
            desiredDatabase == DATABASE_BP4D_NORMAL or
            desiredDatabase == DATABASE_BP4D_H5 or
            desiredDatabase == DATABASE_BP4D_3D):
        # BP4D

        badFilePath = ""

        if machine == MACHINE_BIG_BEAR:
            badFilePath = '/media/Data1/BP4D/badFiles_BP4D.txt'
        elif machine == MACHINE_LITTLE_BEAR:
            badFilePath = '/media/Data/BP4D/badFiles_BP4D.txt'
        elif machine == MACHINE_SANGUINIUS:
            badFilePath = 'D:/Data/badFiles_BP4D.txt'
        else:
            raise ValueError("INVALID MACHINE TYPE")

        # Load list of bad files
        lostFileList = open(badFilePath).readlines()

    elif desiredDatabase == DATABASE_BP4D_PLUS:
        basFilePath = ''

        if machine == MACHINE_BIG_BEAR:
            badFilePath = "/mnt/BPPlus1/BPPlus_lostFiles.txt"
        elif machine == MACHINE_LITTLE_BEAR:
            badFilePath = ""
        else:
            raise ValueError("INVALID MACHINE TYPE")

        # Load list of bad files
        lostFileList = open(badFilePath).readlines()

    elif desiredDatabase == DATABASE_UNBC_COLOR:
        # UNBC

        badFilePath = ""

        if machine == MACHINE_BIG_BEAR:
            badFilePath = "/media/Data2/UNBC/badFiles_UNBC.txt"
        elif machine == MACHINE_LITTLE_BEAR:
            badFilePath = "/media/Data/UNBC/badFiles_UNBC.txt"
        else:
            raise ValueError("INVALID MACHINE TYPE")

        # Load list of bad files
        lostFileList = open(badFilePath).readlines()

    elif desiredDatabase == DATABASE_CASME2_COLOR:
        # CASME2 - Color

        badFilePath = ""

        if machine == MACHINE_BIG_BEAR:
            badFilePath = ""  # TODO
        elif machine == MACHINE_LITTLE_BEAR:
            badFilePath = ""  # TODO
        elif machine == MACHINE_PCWEIQUAN:
            badFilePath = "C:/MSData/badFiles_CASME2.txt"  # For WQ Only
        else:
            raise ValueError("INVALID MACHINE TYPE")
        # Load list of bad files
        lostFileList = open(badFilePath).readlines()

    elif desiredDatabase == DATABASE_CASME2_3D:
        lostFileList = []

    elif (desiredDatabase == DATABASE_DRIVE or
          desiredDatabase == DATABASE_RITE):
        if machine == MACHINE_BIG_BEAR:
            badFilePath = ""
            lostFileList = []
        elif machine == MACHINE_LITTLE_BEAR:
            badFilePath = ""
            lostFileList = []
        else:
            raise ValueError("INVALID MACHINE TYPE")

    # Clean up lost filelist
    lostFileList = list(map(str.rstrip, lostFileList))

    return lostFileList


def getExperimentOutputDirectory(machine, desiredDatabase):
    if machine == MACHINE_BIG_BEAR:
        outputDirectory = '/media/Data2/EXPERIMENTS/' + desiredDatabase
    elif machine == MACHINE_LITTLE_BEAR:
        outputDirectory = '/media/Data/EXPERIMENTS/' + desiredDatabase
    elif machine == MACHINE_SANGUINIUS:
        outputDirectory = 'D:/Data/Experiments/' + desiredDatabase
    elif machine == MACHINE_PCWEIQUAN:
        outputDirectory = "C:/MSExpr/" + desiredDatabase
    else:
        raise ValueError("INVALID MACHINE TYPE")

    return outputDirectory


def getCorrectDataGeneratorType(desiredDatabase):
    if (desiredDatabase == DATABASE_BP4D_COLOR or
            desiredDatabase == DATABASE_BP4D_DEPTH or
            desiredDatabase == DATABASE_BP4D_NORMAL or
            desiredDatabase == DATABASE_BP4D_H5 or
            desiredDatabase == DATABASE_BP4D_3D):
        chosenDataGeneratorType = BP4DDataGenerator
    elif desiredDatabase == DATABASE_BP4D_PLUS:
        chosenDataGeneratorType = BPPlusDataGenerator
    elif (desiredDatabase == DATABASE_DRIVE or
          desiredDatabase == DATABASE_RITE):
        chosenDataGeneratorType = DRIVEDataGenerator
    elif desiredDatabase == DATABASE_UNBC_COLOR:
        # chosenDataGeneratorType = UNBCDataGenerator
        raise ValueError("NOT YET IMPLEMENTED!")
    elif desiredDatabase == DATABASE_CASME2_COLOR:
        chosenDataGeneratorType = CASME2DataGenerator
    elif desiredDatabase == DATABASE_CASME2_3D:
        chosenDataGeneratorType = CASME2DataGenerator
    elif desiredDatabase == DATABASE_BU3D_3D:
        chosenDataGeneratorType = BU3DDataGenerator
    elif desiredDatabase == DATABASE_BU4D_3D:
        chosenDataGeneratorType = BU4DDataGenerator

    else:
        raise ValueError("ERROR: Invalid requested database!")

    return chosenDataGeneratorType


def prepareKFold(allSubjects, totalFoldCnt, shuffleSubjects, seed):
    # Do K-fold cross-validation
    kf = KFold(n_splits=totalFoldCnt, shuffle=shuffleSubjects, random_state=seed)

    return kf


def printTimeTaken(diffTime):
    print("TIME TAKEN:")
    print(diffTime.days, "DAYS")
    print(diffTime.seconds / 3600, "HOURS")
    print((diffTime.seconds % 3600) / 60, "MINUTES")
    print((diffTime.seconds % 3600) % 60, "SECONDS")


def __createDataGen(params, DataGeneratorType, batchSize):
    dataGen = DataGeneratorType(params)
    dataGen.initialize(batchSize)
    return dataGen


def createAndPrepareDataGenerators(baseDataParams, DataGeneratorType,
                                   dataPreprocessParams,
                                   trainSubjects, validSubjects, testSubjects,
                                   totalSubjectCnt,
                                   batchSize, useSamplingOnTest, useSamplingOnValid):
    print("TRAIN SUBJECTS:", trainSubjects)
    print("VALID SUBJECTS:", validSubjects)
    print("TEST SUBJECTS:", testSubjects)

    # Compute number of samples
    totalSampleCnt = baseDataParams.samplingCount
    trainSampleCnt = int(totalSampleCnt * (float(len(trainSubjects)) / float(totalSubjectCnt)))
    validSampleCnt = int(totalSampleCnt * (float(len(validSubjects)) / float(totalSubjectCnt)))
    testSampleCnt = int(totalSampleCnt * (float(len(testSubjects)) / float(totalSubjectCnt)))

    print("TRAIN SAMPLE CNT:", trainSampleCnt)
    print("VALID SAMPLE CNT:", validSampleCnt)
    print("TEST SAMPLE CNT:", testSampleCnt)
    print("Total subject count: ", totalSubjectCnt)
    print("Total sample count: ", totalSampleCnt)
    print("len of train: ", len(trainSubjects))
    print("len of vaild: ", len(validSubjects))
    print("len of test: ", len(testSubjects))

    # Make copy of parameters
    dataParams = copy.deepcopy(baseDataParams)

    # Create Training DataGenerator
    print("Creating training generator")
    dataParams.validSubjects = trainSubjects
    dataParams.samplingCount = trainSampleCnt
    trainGen = DataGeneratorType(dataParams)
    trainGen.initialize(batchSize)

    # Make a copy of the training data generator WITHOUT data augmentation
    print("Creating train no aug generator")
    trainNoAugGen = copy.deepcopy(trainGen)
    # print("trainNoAugGen deepcopy done")
    trainNoAugGen.toggleDataAugmentation(False)
    # print("trainNoAugGen dataaugmentation done")
    trainNoAugGen.toggleShuffleIndices(False)
    # print("trainNoAugGen toggleshuffleindeces done")
    dataParams.validSubjects = trainSubjects
    dataParams.samplingCount = trainSampleCnt

    # trainNoAugGen = DataGeneratorType(dataParams)
    # trainNoAugGen.initialize(batchSize)
    # trainNoAugGen.toggleDataAugmentation(False)
    # trainNoAugGen.toggleShuffleIndices(False)
    '''
    # Create Training DataGenerator (without data augmentation)
    dataParams.validSubjects = trainSubjects
    dataParams.doDataAugmentation = False
    trainNoAugGen = DataGeneratorType(dataParams)
    trainNoAugGen.initialize(batchSize)  
    '''

    # Create Validation DataGenerator
    dataParams.validSubjects = validSubjects
    dataParams.samplingCount = validSampleCnt
    dataParams.doDataAugmentation = False
    dataParams.doShuffleIndices = False
    if not useSamplingOnValid:
        sampleBefore = dataParams.samplingCount
        balanceBefore = dataParams.doDataBalancing
        dataParams.samplingCount = 0
        dataParams.doDataBalancing = False
        print("WARNING: NOT using data sampling or balancing on validation set...")
    print("Creating validation generator")
    validGen = DataGeneratorType(dataParams)
    validGen.initialize(batchSize)

    if not useSamplingOnValid:
        dataParams.samplingCount = sampleBefore
        dataParams.doDataBalancing = balanceBefore

        # Create Testing DataGenerator
    dataParams.validSubjects = testSubjects
    dataParams.samplingCount = testSampleCnt
    dataParams.doDataAugmentation = False
    dataParams.doShuffleIndices = False
    if not useSamplingOnTest:
        sampleBefore = dataParams.samplingCount
        balanceBefore = dataParams.doDataBalancing
        dataParams.samplingCount = 0
        dataParams.doDataBalancing = False
        print("WARNING: NOT using data sampling or balancing on test set...")
    print("Creating testing generator")
    testGen = DataGeneratorType(dataParams)
    testGen.initialize(batchSize)

    if not useSamplingOnTest:
        dataParams.samplingCount = sampleBefore
        dataParams.doDataBalancing = balanceBefore

        # Make data preprocessor
    if dataParams.imageDataType == INPUT_DATATYPE_OBJ:
        dataPreprocess = Data3DPreprocessor(dataPreprocessParams, sourceDataGen=trainGen)
        # dataPreprocess = None
    else:
        dataPreprocess = DataPreprocessor(dataPreprocessParams, sourceDataGen=trainGen)
    trainGen.setDataPreprocessor(dataPreprocess)
    trainNoAugGen.setDataPreprocessor(dataPreprocess)
    validGen.setDataPreprocessor(dataPreprocess)
    testGen.setDataPreprocessor(dataPreprocess)

    return trainGen, trainNoAugGen, validGen, testGen


def createAndPrepareDataViews(allDataGen, useDataAugmentation, dataPreprocessParams,
                              trainSubjects, validSubjects, testSubjects,
                              batchSize, testWithDataAug=False):
    print("TRAIN SUBJECTS:", trainSubjects)
    print("VALID SUBJECTS:", validSubjects)
    print("TEST SUBJECTS:", testSubjects)

    # Create Training DataGenerator
    trainView = DataViewParams()
    trainView.selectedSubjects = trainSubjects
    trainView.doShuffleIndices = True
    trainView.doDataAugmentation = useDataAugmentation

    trainGen = DataViewGenerator(trainView, allDataGen)
    trainGen.initialize(batchSize)

    # Make a copy of the training data generator WITHOUT data augmentation
    trainNoAugView = DataViewParams()
    trainNoAugView.selectedSubjects = trainSubjects
    trainNoAugView.doShuffleIndices = False
    trainNoAugView.doDataAugmentation = False

    trainNoAugGen = DataViewGenerator(trainNoAugView, allDataGen)
    trainNoAugGen.initialize(batchSize)

    # Create Validation DataGenerator
    validView = DataViewParams()
    validView.selectedSubjects = validSubjects
    validView.doShuffleIndices = False
    validView.doDataAugmentation = testWithDataAug

    validGen = DataViewGenerator(validView, allDataGen)
    validGen.initialize(batchSize)

    # Create Testing DataGenerator
    testView = DataViewParams()
    testView.selectedSubjects = testSubjects
    testView.doShuffleIndices = False
    testView.doDataAugmentation = testWithDataAug

    testGen = DataViewGenerator(testView, allDataGen)
    testGen.initialize(batchSize)

    # Make data preprocessor

    if (allDataGen.getParams().imageDataType == INPUT_DATATYPE_OBJ
            or allDataGen.getParams().imageDataType == INPUT_DATATYPE_WRL):
        dataPreprocess = Data3DPreprocessor(dataPreprocessParams, sourceDataGen=trainGen)
    else:
        dataPreprocess = DataPreprocessor(dataPreprocessParams, sourceDataGen=trainGen)

    trainGen.setDataPreprocessor(dataPreprocess)
    trainNoAugGen.setDataPreprocessor(dataPreprocess)
    validGen.setDataPreprocessor(dataPreprocess)
    testGen.setDataPreprocessor(dataPreprocess)

    print("TRAINING TOTAL SAMPLES:", trainGen.getTotalSampleCnt())
    print("TRAINING (NO AUG) TOTAL SAMPLES:", trainNoAugGen.getTotalSampleCnt())
    print("VALIDATION TOTAL SAMPLES:", validGen.getTotalSampleCnt())
    print("TESTING TOTAL SAMPLES:", testGen.getTotalSampleCnt())

    return trainGen, trainNoAugGen, validGen, testGen


def getOptimizer(name, args):
    name = name.lower()

    if name == 'adam':
        return Adam(**args)
    elif name == 'adagrad':
        return Adagrad(**args)
    elif name == 'sgd':
        return SGD(**args)
    else:
        raise ValueError("UNKNOWN OPTIMIZER " + name)


def multioutput_categorical_crossentropy(y_true, y_pred):
    return - tf.reduce_sum(y_true * tf.log(y_pred), axis=-1)


def PointNet_Feature_Loss(y_true, transform):
    # Code modified from: https://github.com/charlesq34/pointnet/blob/master/models/pointnet_cls.py

    # Get transform tensor
    # Shape: (batch, cloud, outdim, outdim)
    # transform = nodes_to_track['featureTransform']

    # Enforce the transformation as orthogonal matrix
    print("TRANSFORM SHAPE:", transform.get_shape().as_list())
    print("TRANSFORM NAME:", transform.name)
    Ksize = transform.get_shape()[1].value
    mat_diff = tf.matmul(transform, tf.transpose(transform, perm=[0, 1, 2, 4, 3]))
    mat_diff -= tf.constant(np.eye(Ksize), dtype=tf.float32)
    mat_diff_loss = tf.nn.l2_loss(mat_diff)
    tf.summary.scalar('mat loss', mat_diff_loss)

    return mat_diff_loss


def getLossFunction(name, classWeights):
    name = name.lower()

    # We will check for our custom loss functions first, but failing that we'll fall
    # back on the Keras names.

    if name == "weighted_binary_crossentropy":
        # Code modified from: https://github.com/keras-team/keras/issues/2115

        def weighted_binary_crossentropy(y_true, y_pred, weights):
            # Get number of classes and possible values
            # classCnt = len(weights)
            # valueCnt = len(weights[0])

            print("Y_PRED SHAPE:", y_pred.get_shape().as_list())

            # Create weight mask
            # Dimensions: [batch][classes]
            M = K.round(y_true)
            M = tf.multiply(M, weights)
            invM = tf.multiply(tf.add(K.round(y_true), - 1), -1)
            M = K.cast(tf.add(M, invM), K.floatx())

            sumM = tf.reduce_sum(M)
            normalSum = K.cast(tf.reduce_prod(tf.shape(M)), K.floatx())
            factor = tf.divide(normalSum, sumM)
            M = tf.multiply(M, factor)

            # Multiply mask
            return K.binary_crossentropy(y_pred, y_true) * M

        '''
        def weighted_binary_crossentropy(y_true, y_pred, weights):
            # Get number of classes and possible values
            classCnt = len(weights)
            valueCnt = len(weights[0])

            # DEBUG: Exaggerated amplification of first weight
            #weights[:,0] *= 10.0
            #print("WEIGHTED NOW:")
            #print(weights)
            # END DEBUG

            # Get sum of weights
            sumWeights = np.sum(weights)
            print("SUM OF WEIGHTS:", sumWeights)

            # Prepare final mask
            final_mask = K.zeros_like(y_pred)
            #print("FINAL MASK SIZE:", final_mask.get_shape().as_list())

            # Get array that indicates whether it was incorrectly predicted.
            y_pred_round = K.round(y_pred)
            y_incorrect = K.cast(K.not_equal(y_pred, y_true), K.floatx())

            #print("y_incorrect SIZE:", y_incorrect.get_shape().as_list())

            # Each class is handled separately
            for classIndex in range(classCnt):
                final_mask += (K.cast(weights[classIndex, 0],K.floatx())) * (K.cast(1.0 - y_pred_round[:,:,classIndex], K.floatx())) * y_incorrect[:,:,classIndex] 
                final_mask += (K.cast(weights[classIndex, 1],K.floatx())) * (K.cast(y_pred_round[:,:,classIndex], K.floatx())) * y_incorrect[:,:,classIndex]     
            #final_mask /= sumWeights                
            return K.binary_crossentropy(y_pred, y_true) * final_mask
        '''

        '''
        def weighted_binary_crossentropy(y_true, y_pred, weights):
            # Get number of classes and possible values
            classCnt = len(weights)
            valueCnt = len(weights[0])

            # Get inverted prediction
            y_true_invert = 1.0 - y_true

            # Transpose weights
            tranWeights = np.transpose(weights)

            # Multiply by appropriate weights
            weighted_y_true = y_true * np.array([[tranWeights[1]]])
            weighted_y_true_invert = y_true_invert * np.array([[tranWeights[0]]])

            # Add together
            weightMask = weighted_y_true + weighted_y_true_invert

            # Get regular binary crossentropy result
            origResult = K.binary_crossentropy(y_pred, y_true)

            #print("WEIGHT MASK SHAPE:",weightMask.get_shape().as_list())

            # Compute mask 
            weightMask = K.zeros_like(origResult)
            print(weightMask.get_shape().as_list())
            for classIndex in range(classCnt):
                weightMask[:, classIndex] += (1.0 - y_true[:,classIndex])*weights[classIndex,0] + y_true[:,classIndex]*weights[classIndex,1]

            #print(weightMask)

            # Multiply mask
            result = origResult * weightMask

            return result
        '''

        # Code from: http://tiao.io/posts/adding-__name__-and-__doc__-attributes-to-functoolspartial-objects/
        def wrapped_partial(func, *args, **kwargs):
            partial_func = partial(func, *args, **kwargs)
            update_wrapper(partial_func, func)
            return partial_func

        loss = wrapped_partial(weighted_binary_crossentropy, weights=classWeights)

        return loss
    elif name == "multioutput_categorical_crossentropy":
        return multioutput_categorical_crossentropy
    else:
        return name

    '''
    elif name == "pointnet_categorical_crossentropy":
        def PointNet_categorical_crossentropy(y_true, y_pred, nodes_to_track=nodes_to_track, reg_weight=0.001):
            # Get ordinary categorical_crossentropy
            entropyLoss = keras.losses.categorical_crossentropy(y_true, y_pred)
            tf.summary.scalar('classify loss', entropyLoss)

            # Get loss related to transform layer
            mat_diff_loss = PointNet_Feature_Loss(nodes_to_track)

            # Compute total loss
            totalLoss = entropyLoss + mat_diff_loss * reg_weight

            return totalLoss

        return PointNet_categorical_crossentropy

    elif name == "pointnet_multioutput_categorical_crossentropy":
        def PointNet_multioutput_categorical_crossentropy(y_true, y_pred, nodes_to_track=nodes_to_track, reg_weight=0.001):
            # Get ordinary multi-output categorical_crossentropy
            entropyLoss = multioutput_categorical_crossentropy(y_true, y_pred)
            tf.summary.scalar('classify loss', entropyLoss)

            # Get loss related to transform layer
            mat_diff_loss = PointNet_Feature_Loss(nodes_to_track)

            # Compute total loss
            totalLoss = entropyLoss + mat_diff_loss * reg_weight

            return totalLoss

        return PointNet_multioutput_categorical_crossentropy
    '''


def multioutput_categorical_accuracy(y_true, y_pred):
    oldShape = tf.shape(y_true)
    classCnt = oldShape[-1]
    halfClassCnt = tf.cast(tf.divide(classCnt, 2), tf.int32)

    # oldShape = tf.Print(oldShape, [oldShape], "Old shape")
    # classCnt = tf.Print(classCnt, [classCnt], "classCnt")
    # halfClassCnt = tf.Print(halfClassCnt, [halfClassCnt], "halfClassCnt")

    newShape = tf.concat([oldShape[:-1], [halfClassCnt], [2]], axis=0)
    # newShape = tf.Print(newShape, [newShape], "New shape")

    r_y_true = K.reshape(y_true, newShape)
    r_y_pred = K.reshape(y_pred, newShape)

    result = K.cast(K.equal(K.argmax(r_y_true, axis=-1),
                            K.argmax(r_y_pred, axis=-1)),
                    K.floatx())

    # result = tf.Print(result, [tf.shape(result)], "Result shape:")

    # finalShape = tf.concat([oldShape[:-1], [classCnt]], axis=0)
    # result = K.reshape(result, finalShape)
    # result = tf.Print(result, [tf.shape(result)], "Final result shape:")
    return result