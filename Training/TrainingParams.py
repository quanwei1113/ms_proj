# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

import numpy as np
from General.BaseParams import *

TRAIN_TYPE_TRAIN_TEST = "TRAIN_TEST"
TRAIN_TYPE_CROSS_VALID = "CROSS_VALID"
TRAIN_TYPE_PRE_TRAIN = "PRE_TRAIN"


################################
# TrainingParams
################################

class TrainingParams(BaseParams):

    def __init__(self):
        # Set number of samples per batch
        self.batchSize = 100

        # Set the number of epochs to train on
        # (1 epoch = covered all data in training set)
        self.numEpochs = 200

        # Do data augmentation
        self.doDataAugmentation = True

        # List of visible GPUs
        self.visibleDevices = '0,1'

        # Which optimizer do we want?
        self.optimizerName = 'adagrad'

        # What parameters do we want for the optimizer?
        self.optimizerParams = dict()

        # What are we using for loss?
        self.loss = 'categorical_crossentropy'

        # What are our metrics?
        self.metrics = ['accuracy']

        # Training type: training/testing set or cross-validation?
        self.trainType = TRAIN_TYPE_CROSS_VALID

        # K-fold shuffle subjects seed
        self.kFoldStartSeed = 5280

        # Fold count (used only if doing TRAIN_TYPE_CROSS_VALID or TRAIN_TYPE_PRE_TRAIN)
        self.totalFoldCnt = 3

        # How many trials? (used only if doing TRAIN_TYPE_CROSS_VALID)
        self.totalTrialCnt = 1

        # Validation fraction split (used only if doing TRAIN_TYPE_PRE_TRAIN)
        self.validationSplit = 0.2

        # Do we want sampling on the testing set?
        self.useSamplingOnTest = True

        # Do we want sampling on the validation set?
        self.useSamplingOnValid = True

        # Tensorboard params
        self.TensorboardParams = dict()

        # Learning rate scheduler callback
        self.learningRateFunc = None

        # Do we want validation at all?
        self.useValidationGen = True

