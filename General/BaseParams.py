# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from os.path import join, isfile
from os import listdir
import csv
import numpy as np
import pickle
import hashlib


class BaseParams(object):

    def printParams(self, outputFile=None):
        if outputFile is None:
            print(self.__class__.__name__, ":")
            for attr, value in self.__dict__.items():
                print("\t", attr, value)
        else:
            with open(outputFile, 'w') as myFile:
                myFile.write(self.__class__.__name__ + "\n")
                for attr, value in self.__dict__.items():
                    myFile.write("\t" + str(attr) + " = " + str(value) + "\n")

    def saveParams(self, outputFile):
        with open(outputFile, 'wb') as myFile:
            pickle.dump(self.__dict__, myFile)

    def loadParams(self, inputFile):
        with open(inputFile, 'rb') as myFile:
            tmpDict = pickle.load(myFile)
            self.__dict__.update(tmpDict)

    def getHashKey(self):
        descString = ""
        for attr, value in self.__dict__.items():
            descString += str(attr) + "=" + str(value) + "\n"

        hash_object = hashlib.md5(descString.encode('utf-8'))
        return hash_object.hexdigest()