# MIT LICENSE
#
# Copyright 2018 Micah A. Church
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import absolute_import, print_function

import numpy as np
import h5py
# import cv2

from os import listdir
from keras.models import Sequential
from keras.layers import InputLayer, Reshape, Dense, Dropout, Flatten, Lambda
from keras.layers import Conv2D, MaxPooling2D
import tensorflow as tf
from Data.BaseDataGenerator import *
from Data.DRIVEDataLoadParams import *
import random
import cv2


class DRIVEDataGenerator(BaseDataGenerator):

    def getStructuredPreditionDim(self):
        return self.params.s

    # FUNCTION FOR JUST CONVERTING LABELS
    # ONLY WORKS FOR THINGS WITH CHANNELS, (FULL)
    def convertLabel(self, label):
        assert (len(np.array(label).shape) == 3)
        image = np.zeros((len(label[0]), len(label[1]), 3))
        for h in range(len(label[0])):
            for w in range(len(label[1])):
                if label[h][w][0] == 1:
                    color = np.array((0.0, 0.0, 0.0))
                elif label[h][w][1] == 1:
                    color = np.array((255.0, 0.0, 0.0))
                elif label[h][w][2] == 1:
                    color = np.array((0.0, 0.0, 255.0))
                elif label[h][w][3] == 1:
                    color = np.array((0.0, 255.0, 0.0))
                elif label[h][w][4] == 1:
                    color = np.array((255.0, 255.0, 255.0))
                image[h][w] = color

        return image

    def getSample(self, sampleIndex, frameIndex):
        # use allTasks and allSubjects to determine the proper indeces for sample and patch
        subject = self.allSubjects[sampleIndex]
        patch = self.allTasks[sampleIndex]

        with h5py.File(self.allImagesH5, 'r') as myFile:

            sample = myFile['image']

            if self.params.desiredData.find("PATCH") != -1:
                sample = sample[subject, patch]
            else:
                sample = sample[subject]

            sample = sample.astype(np.float32)

            '''
            # Debug text
            if sampleIndex%1000 == 0:

                print("Getting a sample for output")

                sample = np.flip(sample, axis=-1)

                print(sample.shape)

                #print("Sample Image: " + str(subject) + " Patch: " + str(patch) + " sampleIndex: " + str(sampleIndex) + " frameIndex: " + str(frameIndex))

                cv2.imwrite("./Junk/Sample_Image_" + str(subject) + "_Patch_" + str(patch) + "_sampleIndex_" + str(sampleIndex) + "_frameIndex_" + str(frameIndex) + ".png", sample)

                print("Saved Sample")
                #cv2.imshow("Sample Image" ,np.flip(sample, axis=-1)/255.0)
                #cv2.waitKey(-1)
            '''

        '''

        if self.params.desiredData.find("PATCH"):
                strideType = "S_" + str(self.params.stride)
                fovChoice = self.params.fovChoice

        files = listdir(self.params.baseDir)

        for file in files:
            if self.params.desiredData.find("PATCH") != -1 and file.find(strideType) != -1 and file.find(fovChoice) != -1:
                myFile = h5py.File(join(self.params.baseDir, file), 'r')
            elif self.params.desiredData.find("FULL") != -1 and self.params.desiredData.find("PATCH") == -1 \
            and file.find("images") != -1 and file.find("info") == -1:
                myFile = h5py.File(join(self.params.baseDir, file), 'r')

        if myFile == None:
            raise ValueError("Couldnt find the proper file")

        sample = myFile['image']
        if self.params.desiredData.find("PATCH") != -1:
            sample = sample[subject,patch]			
        else:
            sample = sample[subject]

        sample = sample.astype(np.float32) 

        myFile.close()
        '''

        return sample

    def getLabel(self, sampleIndex, frameIndex):

        # subject = self.allSubjects[sampleIndex]
        '''
        # Debug Text
        if sampleIndex%1000 == 0:
            print("\nGetting a label for output")

            subject = self.allSubjects[sampleIndex]
            patch = self.allTasks[sampleIndex]

            print("getting label from made labels")

            label = np.flip(self.convertLabel(self.allLabels[sampleIndex][frameIndex]),axis=-1)

            print(label.shape)

            cv2.imwrite("./Junk/Ground_Image_" + str(subject) + "_Patch_" + str(patch) + "_sampleIndex_" + str(sampleIndex) + "_frameIndex_" + str(frameIndex) + ".png", label)

            print("Label saved")

            #print("Ground Image: " + str(subject) + " Patch: " + str(patch) + " sampleIndex: " + str(sampleIndex) + " frameIndex: " + str(frameIndex))

            #cv2.imshow("Ground Image",self.convertLabel(self.allLabels[sampleIndex][frameIndex])/255.0)
            #cv2.waitKey(-1)
        '''

        # print("LABEL:", self.allLabels[sampleIndex][frameIndex])
        # print("SIZE:", np.array(self.allLabels[sampleIndex][frameIndex]).shape)
        # print("Shape of label", np.asarray(self.allLabels[sampleIndex][frameIndex]).shape)

        return self.allLabels[sampleIndex][frameIndex]

    def getUniqueValueModes(self):
        return [0, 1]

    def saveSample(self, sampleIndex, frameIndex):
        # Code for getting all samples with after the data has been sampled for every subject
        # Presumes that the samples and labels are full of some variant
        # print(sampleIndex)
        # print(frameIndex)
        # subject = sampleIndex/len(self.params.desiredData)
        subject = self.params.validSubjects[sampleIndex / len(self.params.validSubjects)]
        patch = sampleIndex % len(self.params.validSubjects)
        if subject < 20:
            subfolder = "/Train"
        else:
            subfolder = "/Test"
        # print (subject)
        # print (patch)
        # sampleIndex = patch * ((subject+1) if subject < (len(self.uniqueSubjects)-1) else (subject))
        label = np.flip(self.convertLabel(self.getLabel(sampleIndex, frameIndex)), axis=-1)
        sample = np.flip(self.getSample(sampleIndex, frameIndex), axis=-1)
        cv2.imwrite("./Samples" + subfolder + "/Subject_" + str(subject) + "_Patch_" + str(patch) + ".png", sample)
        cv2.imwrite("./Samples" + subfolder + "/Label_" + str(subject) + "_Patch_" + str(patch) + ".png", label)

    def getIndex(self, groundImg, subject, row, col):
        if self.params.desiredData.find("DRIVE") != -1:
            index = int((groundImg[subject, row, col]) / 255)
        else:
            index = (groundImg[subject, row, col]).astype(np.int32)

            if np.array_equal(index, [0, 0, 0]):
                index = 0
            elif np.array_equal(index, [255, 0, 0]):
                '''
                # Debug print
                print("The pixel in subject",subject, "at (", col,",", row, "):", "is an artery")
                '''
                index = 1
            elif np.array_equal(index, [0, 0, 255]):
                index = 2
            elif np.array_equal(index, [0, 255, 0]):
                index = 3
            elif np.array_equal(index, [255, 255, 255]):
                index = 4
            else:
                print("Bad pixel in subject", subject, "at (", col, ",", row, "):", index)
                # print("Subject: ", subject, "X: ", row, "Y: ", col)
                # print("The color is: ", index)
                # print("Invalid pixel returning 4")
                index = 4
        return index

    def computeIndex(self, value):
        if self.params.desiredData.find("DRIVE") != -1:
            index = int((value) / 255)
        else:
            index = (value).astype(np.int32)

            if np.array_equal(index, [0, 0, 0]):
                index = 0
            elif np.array_equal(index, [255, 0, 0]):
                '''
                # Debug print
                print("The pixel in subject",subject, "at (", col,",", row, "):", "is an artery")
                '''
                index = 1
            elif np.array_equal(index, [0, 0, 255]):
                index = 2
            elif np.array_equal(index, [0, 255, 0]):
                index = 3
            elif np.array_equal(index, [255, 255, 255]):
                index = 4
            else:
                print("Bad pixel:", index)
                # print("Bad pixel in subject", subject, "at (", col,",", row, "):", index)
                # print("Subject: ", subject, "X: ", row, "Y: ", col)
                # print("The color is: ", index)
                # print("Invalid pixel returning 4")
                index = 4
        return index

    def getImageFilenames(self, lostFilelist):

        imagePath = self.params.baseDir

        sequenceLabels = []
        sequenceSubjects = []
        sequenceTask = []
        sequenceFilename = []

        print("Loading filenames and ground truth...")
        # check to ensure data is appropriate
        if self.params.frameCnt != 1:
            raise ValueError("Invalid frame count, must be one")
        if self.params.desiredData.find("DRIVE") != -1 and len(self.params.desiredClasses) != 2 or \
                self.params.desiredData.find("RITE") != -1 and len(self.params.desiredClasses) != 5:
            raise ValueError("Desired data and desired classes do not match")
        if self.params.desiredData.find("PATCH") == -1 and self.params.desiredData.find("FULL") == -1:
            raise ValueError("Invalid data needs to be in PATCHES or FULL")
        if len(self.params.validSubjects) > 40:
            raise ValueError("Invalid set choose either TRAIN or TEST")
        if self.params.desiredData.find("DRIVE") == -1 and self.params.desiredData.find("RITE") == -1:
            raise ValueError("Invalid ground type selection choose either DRIVE or RITE")

        if self.params.desiredData.find("PATCH") != -1:
            strideType = "S_" + str(self.params.stride)
            fovChoice = self.params.fovChoice

        subjects = []
        tasks = []

        # Step 1 get the image file appropriatly
        if self.params.desiredData.find("PATCH") != -1:
            self.allImagesH5 = join(imagePath, "DRIVE_patches_" + str(
                self.params.patchSize) + "_" + strideType + "_" + fovChoice + ".h5")

            with h5py.File(self.allImagesH5, 'r') as imageFile:

                patchCenters = imageFile['center']
                allCenters = patchCenters[:]
                # print allCenters
                patchInfo = imageFile['patch_info']
                patchHeight = patchInfo[0]
                patchWidth = patchInfo[1]
                strideHeight = patchInfo[2]
                strideWidth = patchInfo[3]

                totalImageCnt = allCenters.shape[0]
                maxPatchCnt = allCenters.shape[1]

        else:
            # Full image
            self.allImagesH5 = join(imagePath, "DRIVE_images.h5")
            with h5py.File(self.allImagesH5, 'r') as imageFile:
                totalImageCnt = imageFile['image'].shape[0]

        # Step 2 get the ground file
        if len(self.params.desiredClasses) == 2:
            groundFile = "DRIVE_ground.h5"
        else:
            groundFile = "RITE_ground.h5"

        # Get maximum possible images/subjects
        if not self.params.validSubjects:
            subjects = range(totalImageCnt)
        else:
            subjects = self.params.validSubjects

        print("SUBJECTS TO USE:", subjects)

        # Get maximum number of patches (if that makes sense)
        if self.params.desiredData.find("PATCH") != -1:
            if not self.params.desiredTasks:
                tasks = range(maxPatchCnt)
            else:
                tasks = self.params.desiredTasks

        if self.params.desiredData.find("PATCH") != -1:
            if len(self.params.desiredClasses) == 2:
                groundFile = "DRIVE_ground_labels" + "_patches_" + str(
                    self.params.patchSize) + "_" + strideType + "_" + fovChoice + ".h5"
            else:
                groundFile = "RITE_ground_labels" + "_patches_" + str(
                    self.params.patchSize) + "_" + strideType + "_" + fovChoice + ".h5"
        else:
            if len(self.params.desiredClasses) == 2:
                groundFile = "DRIVE_ground.h5"
            else:
                groundFile = "RITE_ground.h5"

        groundData = join(imagePath, groundFile)
        with h5py.File(groundData, 'r') as groundH5:

            # Get all of the ground image data
            groundImg = groundH5['image']

            groundShape = groundImg.shape

            print("Before all ground get...")

            imgHeight = groundShape[1]
            imgWidth = groundShape[2]

            if self.params.desiredData.find("PATCH") == -1:

                allConvertedGround = np.zeros((len(subjects), imgHeight, imgWidth, len(self.params.desiredClasses)))

                allGround = groundImg[:]

                print("Right before loop")

                for s in range(len(subjects)):
                    for h in range(imgHeight):
                        for w in range(imgWidth):
                            # origIndex = allGround[subjects[s], h, w] #, :]
                            # index = self.convertIndex(origIndex)
                            # allConvertedGround[s][h][w][index]=float(1)

                            index = self.getIndex(allGround, subjects[s], h, w)

                            if index < 0 or index >= len(self.params.desiredClasses):
                                print("Subject: ", subjects[s], "Height: ", h, "Width: ", w)
                                raise ValueError("Index is invalid")
                            allConvertedGround[s][h][w][index] = float(1)

            if self.params.desiredData.find("PATCH") == -1:
                # If we're dealing with the full image data
                print("Is full image")
                data = allConvertedGround[:]
            # Full patches
            elif self.getStructuredPreditionDim() == patchHeight:
                print("is full patch")
                data = groundImg[subjects, ...]

            # Centers
            elif self.getStructuredPreditionDim() == 1:
                print("is center")
                print("shape of groundImg", groundImg.shape)
                print("subjects", subjects, "patchHeight", patchHeight / 2, "patchWidth", patchWidth / 2)
                data = groundImg[subjects, :, patchHeight / 2, patchWidth / 2, :]
                data = np.reshape(data, (-1, 1, self.getStructuredPreditionDim(), self.getStructuredPreditionDim(),
                                         len(self.params.desiredClasses)))
                data = np.array([label for label in data if label.any()])
            # SP
            elif self.getStructuredPreditionDim != 0:
                print("is sp")
                minHeight = patchHeight / 2 - (self.getStructuredPreditionDim() / 2)
                maxHeight = patchHeight / 2 + (self.getStructuredPreditionDim() / 2) + 1
                minWidth = patchWidth / 2 - (self.getStructuredPreditionDim() / 2)
                maxWidth = patchWidth / 2 + (self.getStructuredPreditionDim() / 2) + 1
                data = groundImg[subjects, :, minHeight:maxHeight, minWidth:maxWidth, :]
                data = np.reshape(data, (-1, 1, self.getStructuredPreditionDim(), self.getStructuredPreditionDim(),
                                         len(self.params.desiredClasses)))
                data = np.array([label for label in data if label.any()])
            else:
                raise ValueError("invalid datatype")

            print("The shape of data:", data.shape)

            # print("After all ground get...")

            # allConvertedGround = np.zeros((len(subjects), imgHeight, imgWidth, len(self.params.desiredClasses)))

            '''
            print("Get all relevant ground")
            relevantGround = allGround[subjects,:,:,:]
            print("Convert all indices")
            relevantGround = np.reshape(relevantGround, (-1,relevantGround.shape[-1]))
            print(relevantGround.shape)

            from joblib import Parallel, delayed
            import multiprocessing
            num_cores = multiprocessing.cpu_count()    
            indexGround = Parallel(n_jobs=num_cores)(delayed(self.computeIndex)(relevantGround[i]) for i in range(len(relevantGround)))


            #indexGround = [self.computeIndex(relevantGround[i]) for i in range(len(relevantGround))]

            print("After conversion")
            allConvertedGround = np.reshape(allConvertedGround, (-1, len(self.params.desiredClasses)))
            for i in range(len(allConvertedGround)):				
                allConvertedGround[i][indexGround[i]] = 1 
            allConvertedGround = np.reshape(allConvertedGround, (len(subjects), imgHeight, imgWidth, len(self.params.desiredClasses)))
            print("Done converting")
            #computeIndex     
            '''
            '''
            if self.params.desiredData.find("PATCH") == -1:
                print("Right before loop")

                for s in range(len(subjects)):
                    for h in range(imgHeight):
                        for w in range(imgWidth): 
                            #origIndex = allGround[subjects[s], h, w] #, :]
                            #index = self.convertIndex(origIndex)
                            #allConvertedGround[s][h][w][index]=float(1)

                            index = self.getIndex(allGround, subjects[s], h, w)

                            if index < 0 or index >= len(self.params.desiredClasses):
                                print("Subject: ", subjects[s], "Height: ", h, "Width: ", w)
                                raise ValueError("Index is invalid")
                            allConvertedGround[s][h][w][index]=float(1)
            '''

            '''
            # DEBUG TEXT
            for subIndex in range(allConvertedGround.shape[0]):
                subject = subjects[subIndex]
                image = allConvertedGround[subIndex]
                image = np.flip(self.convertLabel(image), axis=-1)

                print("FullGroundConvertedImage " + str(subject))

                #cv2.imshow("Ground Image", image)
                #cv2.waitKey(-1)
            '''
            print("After conversion loop.")

            # Load up actual ground truth data
            if self.params.desiredData.find("PATCH") != -1:
                # Patches
                for subIndex in range(len(subjects)):
                    subject = subjects[subIndex]
                    if subject not in self.params.lostFileList:

                        # nonpadTime = datetime.timedelta(0)
                        # padTime = datetime.timedelta(0)

                        # subjectGroundImage = allConvertedGround[subIndex]

                        allTaskStartTime = datetime.datetime.now()

                        for task in tasks:
                            center = allCenters[subject, task]

                            if ((fovChoice == "A") or
                                    ((fovChoice == "C" or fovChoice == "F") and center.any())):
                                sequenceFilename.append([subject, task])
                                '''
                                if self.params.desiredData.find("FULL") == -1:
                                    # Centers only
                                    #index = self.getIndex(groundImg, subject, center[1], center[0])
                                    #perFrameLabels = [0.0] * len(self.params.desiredClasses)
                                    #perFrameLabels[index] = float(1)

                                    perFrameLabels = allGround[subIndex, task, center[1], center[0],:]


                                    # Debug Text
                                    if np.array_equal(perFrameLabels, [0,1,0,0,0]):
                                        print("The center in subject",subIndex,"at (", center[0],",", center[1], "):", "is an artery"

                                    #sequenceLabels.append([perFrameLabels]) 
                                else:

                                    if self.getStructuredPreditionDim() != patchHeight:

                                        taskStartTime = datetime.datetime.now()

                                        # Full Patch
                                        #minHeight = center[1] - (patchHeight/2)
                                        #maxHeight = center[1] + (patchHeight/2) + 1
                                        #minWidth = center[0] - (patchWidth/2)
                                        #maxWidth = center[0] + (patchWidth/2) + 1
                                        # Stuctured patch
                                        minHeight = patchHeight/2 - (self.getStructuredPreditionDim()/2)
                                        maxHeight = patchHeight/2 + (self.getStructuredPreditionDim()/2) + 1
                                        minWidth = patchWidth/2 - (self.getStructuredPreditionDim()/2)
                                        maxWidth = patchWidth/2 + (self.getStructuredPreditionDim()/2) + 1

                                        #print("Center", center, "MinHeight:",minHeight, "MaxHeight:",maxHeight,"minWidth:", minWidth,"maxWidth:",maxWidth )

                                        bottom = 0
                                        top = 0
                                        left = 0
                                        right = 0

                                        if minHeight < 0:
                                            bottom = -minHeight
                                            minHeight = 0
                                            #print("PAD!!!")

                                        if minWidth < 0:
                                            left = -minWidth
                                            minWidth = 0
                                            #print("PAD!!!")

                                        if maxHeight > imgHeight:
                                            top = maxHeight - imgHeight
                                            #print("PAD!!!")

                                        if maxWidth > imgWidth:
                                            right = maxWidth - imgWidth
                                            #print("PAD!!!")

                                        #print("(",minHeight,":",maxHeight, minWidth,":", maxWidth,")")

                                        groundPatch = allGround[subIndex, task, minHeight:maxHeight, minWidth:maxWidth, :]
                                        #print("Patch shape:", groundPatch.shape)
                                        #groundPatch = subjectGroundImage[minHeight:maxHeight, minWidth:maxWidth, :]

                                        groundPatch = np.pad(groundPatch, pad_width=((bottom,top),(left,right),(0,0)), mode='constant', constant_values=0)


                                        #print("ground patch shape:", groundPatch.shape)
                                        #if len(image) == 0:
                                        #	print image.shape
                                        #	print "MinHeight: ", minHeight, "MaxHeight: ", maxHeight
                                        #	print "Minwidth: ", minWidth, "Maxwidth: ", maxWidth
                                        #	print "Center: ", center, "X: ", center[0], "Y: ", center[1]
                                        #	print "Image: ", subject, "Patch: ", task
                                        #	print image
                                        #print("Image in loop ", image.shape)
                                        #groundPatch = groundPatch.tolist()
                                        sequenceLabels.append([groundPatch]) 

                                        taskEndTime = datetime.datetime.now()
                                        taskTimeDiff = taskEndTime - taskStartTime

                                        #if bottom != 0 or top != 0 or left != 0 or right != 0:
                                        #	padTime += taskTimeDiff
                                        #else:
                                        #	nonpadTime += taskTimeDiff

                                    #print(np.asarray(Labels).shape)
                                    else:
                                        groundPatch = allGround[subIndex, task, :]
                                '''
                            else:
                                # Found last valid patch
                                break

                    # print("NON PAD TIME:", nonpadTime)
                    # print("PAD TIME:", padTime)
                    # print(np.array(sequenceLabels[0][0]).shape)

                    # allTaskEndTime = datetime.datetime.now()
                    # print("ALL TASK TIME:", (allTaskEndTime - allTaskStartTime))

                # Add subject and task names
                for file in sequenceFilename:
                    sequenceSubjects.append(file[0])
                    sequenceTask.append(file[1])
            # print("Labels after ", np.asarray(Labels).shape)

            else:
                # Full images
                for subIndex in range(len(subjects)):
                    subject = subjects[subIndex]
                    if subject not in self.params.lostFileList:
                        sequenceFilename.append(subject)
                        sequenceSubjects.append(subject)
                        sequenceTask.append(subject)
                    # groundImage = allGround[subIndex]
                    # print(image.shape)
                    # groundImage = groundImage.tolist()
                    # print(image)
                    # sequenceLabels.append([groundImage])

            # QUICK SANITY CHECK
            for label in data:
                if not np.array(label).any():
                    print(label)
                    print(label.shape)
                    raise ValueError("Bad label!  Labels all zeros!")
            sequenceLabels.append(data)

        print("Shape of allSubject", np.array(sequenceSubjects).shape)
        print("Shape of allTasks", np.array(sequenceTask).shape)
        print("Shape of allFilenames", np.array(sequenceFilename).shape)
        print("Shape of allLabels", np.array(data).shape)

        return sequenceFilename, data, sequenceSubjects, sequenceTask

        '''

        infile = self.params.baseDir + '/DRIVE_images_info.h5'
        imgInfo = self.load_hdf5(infile)
        imgHeight = imgInfo.shape[1]
        imgWidth = imgInfo.shape[2]
        imagePath = self.params.baseDir
        sequenceLabels = []
        sequenceSubjects = []
        sequenceTask = []
        sequenceFilename = []
        print("Loading filenames and ground truth...")
        # check to ensure data is appropriate
        if self.params.frameCnt != 1:
            raise ValueError("Invalid frame count, must be one")
        if self.params.desiredData.find("DRIVE") != -1 and len(self.params.desiredClasses) != 2 or \
        self.params.desiredData.find("RITE") != -1 and len(self.params.desiredClasses) != 5:
            raise ValueError("Desired data and desired classes do not match")
        if self.params.desiredData.find("PATCH") == -1 and self.params.desiredData.find("FULL") == -1:
            raise ValueError("Invalid data needs to be in PATCHES or FULL")
        if len(self.params.validSubjects) > 40:
            raise ValueError("Invalid set choose either TRAIN or TEST")
        if self.params.desiredData.find("DRIVE") == -1 and self.params.desiredData.find("RITE") == -1:
            raise ValueError("Invalid ground type selection choose either DRIVE or RITE")

        if self.params.desiredData.find("PATCH") != -1:
            strideType = "S_" + str(self.params.stride)
            fovChoice = self.params.fovChoice

        subjects = []
        tasks = []


        files = listdir(imagePath)

        # Step 1 get the image file appropriatly 
        if self.params.desiredData.find("PATCH") != -1:
            for file in files:
                if file.find(strideType) != -1 and file.find(fovChoice) != -1:
                    myFile = h5py.File(join(imagePath, file), 'r')
                    patchCenters = myFile['center']
                    allCenters = patchCenters[:]
                    #print allCenters
                    patchInfo = myFile['patch_info']
                    patchHeight = patchInfo[0]
                    patchWidth = patchInfo[1]
                    strideHeight = patchInfo[2]
                    strideWidth = patchInfo[3]
                    imgPatches = ((imgHeight-patchHeight)//strideHeight+1)*((imgWidth-patchWidth)//strideWidth+1)
                    imgFile = file

        else:
            for file in files:
                if file.find("images") != -1 and file.find("info") == -1:
                    imgFile = file
        # Step 2 get the ground file
        if len(self.params.desiredClasses) == 2:
            for file in files:
                if file.find("DRIVE_ground") != -1:
                    groundFile = file
        else:
            for file in files:
                if file.find("RITE") != -1:
                    groundFile = file

        if not self.params.validSubjects:
            subjects = range(totImgs)
        else:
            subjects = self.params.validSubjects
            #print(subjects)
        if self.params.desiredData.find("PATCH") != -1:
            if not self.params.desiredTasks:
                tasks = range(imgPatches)
            else:
                tasks = self.params.desiredTasks

        groundData = join(imagePath, groundFile)
        myFile = h5py.File(groundData, 'r')
        groundImg = myFile['image']
        totImgs = groundImg.shape[0]
        if self.params.desiredData.find("FULL") != -1:
            allGround = groundImg[:]
            allConvertedGround = np.zeros((len(subjects),imgHeight,imgWidth, len(self.params.desiredClasses)))
            for s in range(len(subjects)):
                for h in range(imgHeight):
                    for w in range(imgWidth): 
                        index = self.getIndex(allGround, subjects[s], h, w)
                        if index < 0 or index >= len(self.params.desiredClasses):
                            print("Subject: ", s, "Height: ", h, "Width: ", w)
                            raise ValueError("Index is invalid")
                        allConvertedGround[s][h][w][index]=float(1)

        Labels = []
        Filename = []
        # Step 2 get the subjects
        if self.params.desiredData.find("PATCH") != -1:
            for subIndex in range(len(subjects)):
                subject = subjects[subIndex]
                if subject not in self.params.lostFileList:
                    for task in tasks:
                        center = allCenters[subject,task]
                        if (fovChoice == "C" or fovChoice == "F") and center.any():
                            sequenceFilename.append([subject, task])
                            if self.params.desiredData.find("FULL") == -1:
                                index = self.getIndex(groundImg, subject, center[1], center[0])
                                perFrameLabels = [0.0] * len(self.params.desiredClasses)
                                perFrameLabels[index] = float(1)
                                Labels.append(perFrameLabels) 
                            else:
                                minHeight = center[1] - (patchHeight/2)
                                maxHeight = center[1] + (patchHeight/2) + 1
                                minWidth = center[0] - (patchWidth/2)
                                maxWidth = center[0] + (patchWidth/2) + 1
                                image = allConvertedGround[subIndex, minHeight:maxHeight, minWidth:maxWidth, :]
                                #if len(image) == 0:
                                #	print image.shape
                                #	print "MinHeight: ", minHeight, "MaxHeight: ", maxHeight
                                #	print "Minwidth: ", minWidth, "Maxwidth: ", maxWidth
                                #	print "Center: ", center, "X: ", center[0], "Y: ", center[1]
                                #	print "Image: ", subject, "Patch: ", task
                                #	print image
                                #print("Image in loop ", image.shape)
                                image = image.tolist()
                                Labels.append(image) 
                            #print(np.asarray(Labels).shape)
                        else:
                            break
            for file in sequenceFilename:
                sequenceSubjects.append(file[0]) 
                sequenceTask.append(file[1])
            #print("Labels after ", np.asarray(Labels).shape)

        else:
            for subIndex in range(len(subjects)):
                subject = subjects[subIndex]
                if subject not in self.params.lostFileList:
                    sequenceFilename.append(subject)
                    sequenceSubjects.append(subject)
                    sequenceTask.append(subject)
                    image = allConvertedGround[subIndex]
                    #print(image.shape)
                    image = image.tolist()
                    #print(image)
                    Labels.append(image) 

        prevShape = []

        for label in Labels:
            #print("adding around frame")
            labelShape = np.asarray(label).shape
            sequenceLabels.append([label])
            #print(np.asarray(sequenceLabels).shape)
            #if cmp(prevShape,labelShape) != 0:
            #	print prevShape, "!=", labelShape
            prevShape = labelShape
        #print "sequenceLabels:", sequenceLabels
        for label in sequenceLabels:
            if label == [0.0, 0.0]:
                raise ValueError("bad label")

        return sequenceFilename, sequenceLabels, sequenceSubjects, sequenceTask
        '''

