# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

import numpy as np
import os.path
from scipy import spatial
from scipy.spatial import KDTree
from sklearn.decomposition import PCA
from Data.CommonFunctions import saveAsOBJ

DEBUG_alreadySaved = True


class Data3DPreprocessor:

    def __init__(self, preprocessArgs, sourceDataGen=None):
        # For this, we are allowing a few different options for preprocessing
        # - useSequenceGlobalTransform = if true, use same random global transformation for all frames in a sequence
        # - subtractMean = subtract cloud mean
        # - divideStdDev = divide by cloudl standard dev
        # - projectPCA = reproject to PCA
        # - globalRotationX, globalRotationY, globalRotationZ = random rotation range (whole model)
        # - globalShiftX, globalShiftY, globalShiftZ = random shifting range (whole model)
        # - rotationX, rotationY, rotationZ = random rotation range (each cloud)
        # - shiftX, shiftY, shiftZ = random shifting range (each cloud)
        # - noiseRange = noise range (X, Y, and Z) for position noise (individual points)
        # - undersamplingRate = 1 means do nothing, 2 means cut the number of effective points in half by doubling entries in the point list
        # - maxHoleCount = max number of holes to generate
        # - maxHoleSize = max size of each hole

        # - startDimIndex = starting point for point dimensions (default: 0)

        # TODO: Provide list of ALL spots where coordinates are stored, and somehow corrupt the same way.

        self.preprocessArgs = preprocessArgs

        # Clean up dictionary
        self.__checkIfDefaultNeeded('useSequenceGlobalTransform', True)

        self.__checkIfDefaultNeeded('subtractMean', False)
        self.__checkIfDefaultNeeded('divideStdDev', False)
        self.__checkIfDefaultNeeded('projectPCA', False)

        self.__checkIfDefaultNeeded('globalRotationX', 0.0)
        self.__checkIfDefaultNeeded('globalRotationY', 0.0)
        self.__checkIfDefaultNeeded('globalRotationZ', 0.0)

        self.__checkIfDefaultNeeded('globalShiftX', 0.0)
        self.__checkIfDefaultNeeded('globalShiftY', 0.0)
        self.__checkIfDefaultNeeded('globalShiftZ', 0.0)

        self.__checkIfDefaultNeeded('rotationX', 0.0)
        self.__checkIfDefaultNeeded('rotationY', 0.0)
        self.__checkIfDefaultNeeded('rotationZ', 0.0)

        self.__checkIfDefaultNeeded('shiftX', 0.0)
        self.__checkIfDefaultNeeded('shiftY', 0.0)
        self.__checkIfDefaultNeeded('shiftZ', 0.0)

        self.__checkIfDefaultNeeded('noiseRange', 0.0)

        self.__checkIfDefaultNeeded('undersamplingRate', 1)
        self.__checkIfDefaultNeeded('maxHoleCount', 0)
        self.__checkIfDefaultNeeded('maxHoleSize', 0)

        self.__checkIfDefaultNeeded('startDimIndex', 0)

        '''
        # TODO: Add normalization, etc.
        self.mean = np.array((0,0,0))
        self.std = np.array((0,0,0))
        self.principal_components = np.array(((1,0,0), (0,1,0), (0,0,1)))
        '''

    def __checkIfDefaultNeeded(self, key, defaultValue):
        if key not in self.preprocessArgs:
            self.preprocessArgs[key] = defaultValue

    def __rotatePoints(self, pointList, rotX, rotY, rotZ):
        # pointList: [ numPoints, coords ]

        # Convert to radians
        aX = np.radians(rotX)
        aY = np.radians(rotY)
        aZ = np.radians(rotZ)

        # Matrices have to be transposed
        Mx = [[1.0, 0.0, 0.0],
              [0.0, np.cos(aX), -np.sin(aX)],
              [0.0, np.sin(aX), np.cos(aX)]]

        My = [[np.cos(aY), 0.0, np.sin(aY)],
              [0.0, 1.0, 0.0],
              [-np.sin(aY), 0.0, np.cos(aY)]]

        Mz = [[np.cos(aZ), -np.sin(aZ), 0.0],
              [np.sin(aZ), np.cos(aZ), 0.0],
              [0.0, 0.0, 1.0]]

        M = np.matmul(Mx, np.matmul(My, Mz))
        M = np.transpose(M)

        return np.matmul(pointList, M)

    def __shiftPoints(self, pointList, shiftX, shiftY, shiftZ):
        offset = np.array([shiftX, shiftY, shiftZ])

        return pointList + offset

    def __undersamplePoints(self, pointList, undersamplingRate):
        if undersamplingRate > 1:
            # Get total number of points
            pointCnt = pointList.shape[0]

            # How many points to remove?
            pointsToRemove = int(pointCnt / undersamplingRate)

            # How many points to keep?
            pointsToKeep = pointCnt - pointsToRemove

            # Select points randomly
            chosenOnes = np.random.choice(pointCnt, pointsToKeep, replace=False)

            newPoints = np.zeros_like(pointList)
            for i in range(pointCnt):
                index = chosenOnes[(i % pointsToKeep)]
                newPoints[i] = pointList[index]

            return newPoints
        else:
            return pointList

    def __makeHoles(self, pointList, maxHoleCount, maxHoleSize):

        if maxHoleCount > 0:
            # Get total number of points
            pointCnt = pointList.shape[0]

            # Randomly pick some starting points
            startIndices = np.random.choice(pointCnt, maxHoleCount, replace=False)
            startPoints = pointList[startIndices]

            # Make KDTree
            tree = spatial.KDTree(pointList)

            # For each starting point...
            allPointsToRemove = []
            for start in startPoints:
                # Get size of hole
                holeSize = np.random.uniform(low=0, high=maxHoleSize)
                # Get indices of nearest neighbors
                holeNeighbors = tree.query_ball_point(start, holeSize)
                allPointsToRemove += holeNeighbors

            allPointsToRemove = list(set(allPointsToRemove))

            # Get points that are NOT in the removal set
            newPoints = np.zeros_like(pointList)
            index = 0
            while index < pointCnt:
                for i in range(pointCnt):
                    if i not in allPointsToRemove:
                        newPoints[index] = pointList[i]
                        index += 1

            return newPoints
        else:
            return pointList

    def __getRandomValues(self, listArgs):
        values = []
        for arg in listArgs:
            rangeVal = float(self.preprocessArgs[arg])
            values.append(np.random.uniform(low=-rangeVal, high=rangeVal))
        return tuple(values)

    def __getRandomGlobalTransformation(self):
        globalRotationX, globalRotationY, globalRotationZ = self.__getRandomValues(
            ['globalRotationX', 'globalRotationY', 'globalRotationZ'])
        globalShiftX, globalShiftY, globalShiftZ = self.__getRandomValues(
            ['globalShiftX', 'globalShiftY', 'globalShiftZ'])
        return globalRotationX, globalRotationY, globalRotationZ, globalShiftX, globalShiftY, globalShiftZ

    def __augmentOne(self, frame, sequenceGlobalTransform):
        # Dimensions: [ cloudCnt, pointCnt, coords ]
        # OR
        # Dimensions: [ cloudCnt, pointCnt, (other stuff?) + coords + (other stuff?) + nearest neighbor indices ]

        # Get shape
        cloudCnt = frame.shape[0]
        pointCnt = frame.shape[1]
        channelCnt = frame.shape[2]
        startDimIndex = self.preprocessArgs['startDimIndex']

        if channelCnt > 3:
            # We'll need to cut out first set
            subFrame = frame[:, :, startDimIndex:(startDimIndex + 3)]
        else:
            subFrame = frame

        # Get random global rotation/shifting
        if self.preprocessArgs['useSequenceGlobalTransform']:
            # Use sequence-wide transform
            (globalRotationX, globalRotationY, globalRotationZ, globalShiftX, globalShiftY,
             globalShiftZ) = sequenceGlobalTransform
        else:
            globalRotationX, globalRotationY, globalRotationZ, globalShiftX, globalShiftY, globalShiftZ = self.__getRandomGlobalTransformation()

        global DEBUG_alreadySaved

        # Do data augmentation
        for c in range(cloudCnt):

            if not DEBUG_alreadySaved:
                # Save clouds before
                saveAsOBJ(subFrame[c], "JUNK/beforeAugPointClouds" + str(c) + ".obj")

            # Global rotation/shifting
            subFrame[c] = self.__rotatePoints(subFrame[c], globalRotationX, globalRotationY, globalRotationZ)
            subFrame[c] = self.__shiftPoints(subFrame[c], globalShiftX, globalShiftY, globalShiftZ)

            # Local rotation/shifting
            rotationX, rotationY, rotationZ = self.__getRandomValues(['rotationX', 'rotationY', 'rotationZ'])
            shiftX, shiftY, shiftZ = self.__getRandomValues(['shiftX', 'shiftY', 'shiftZ'])
            subFrame[c] = self.__rotatePoints(subFrame[c], rotationX, rotationY, rotationZ)
            subFrame[c] = self.__shiftPoints(subFrame[c], shiftX, shiftY, shiftZ)

            # Add noise
            for p in range(pointCnt):
                pointShiftX, pointShiftY, pointShiftZ = self.__getRandomValues(
                    ['noiseRange', 'noiseRange', 'noiseRange'])
                subFrame[c][p] = self.__shiftPoints(np.array([subFrame[c][p]]), pointShiftX, pointShiftY, pointShiftZ)[
                    0]

            # Undersample
            subFrame[c] = self.__undersamplePoints(subFrame[c], int(self.preprocessArgs['undersamplingRate']))

            # Fill with holes
            subFrame[c] = self.__makeHoles(subFrame[c], int(self.preprocessArgs['maxHoleCount']),
                                           float(self.preprocessArgs['maxHoleSize']))

            if not DEBUG_alreadySaved:
                # Save clouds before
                saveAsOBJ(subFrame[c], "JUNK/afterAugPointClouds" + str(c) + ".obj")

        DEBUG_alreadySaved = True

        # Put that thing back where it came from, or so help me...
        if channelCnt > 3:
            frame[:, :, startDimIndex:(startDimIndex + 3)] = subFrame
        else:
            frame = subFrame

        return frame

    def __augmentAndStandardize(self, video):

        # Standardize first
        video = self.__standardizeOnly(video)

        # Get shape
        videoShape = video.shape

        # Apply the transformation to each frame of the video
        for b in range(len(video)):
            sequenceTransform = self.__getRandomGlobalTransformation()
            for i in range(len(video[b])):
                # Augment
                video[b][i] = self.__augmentOne(video[b][i], sequenceTransform)

        return video

    def __standardizeCloud(self, pointList):

        # Subtract mean?
        if self.preprocessArgs['subtractMean']:
            mean = np.mean(pointList, axis=0)
            pointList -= mean

        # Divide by std. dev?
        if self.preprocessArgs['divideStdDev']:
            std = np.std(pointList, axis=0)
            pointList /= std

        # Reproject to PCA?
        if self.preprocessArgs['projectPCA']:
            pca = PCA(n_components=3)
            pca.fit(pointList)
            pointList = pca.transform(pointList)

        return pointList

    def __standardizeOne(self, frame):
        # Dimensions: [ cloudCnt, pointCnt, coords ]
        # OR
        # Dimensions: [ cloudCnt, pointCnt, (other stuff?) + coords + (other stuff?) + nearest neighbor indices ]

        # Get shape
        cloudCnt = frame.shape[0]
        pointCnt = frame.shape[1]
        channelCnt = frame.shape[2]
        startDimIndex = self.preprocessArgs['startDimIndex']

        if channelCnt > 3:
            # We'll need to cut out first set
            subFrame = frame[:, :, startDimIndex:(startDimIndex + 3)]
        else:
            subFrame = frame

        for c in range(cloudCnt):
            subFrame[c] = self.__standardizeCloud(subFrame[c])

            # Put that thing back where it came from, or so help me...
        if channelCnt > 3:
            frame[:, :, startDimIndex:(startDimIndex + 3)] = subFrame
        else:
            frame = subFrame

        return frame

    def __standardizeOnly(self, video):
        for b in range(len(video)):
            for i in range(len(video[b])):
                video[b][i] = self.__standardizeOne(video[b][i])

        return video

    def preprocessSequence(self, video, doDataAugmentation):

        # Do not have enough channels?
        startDimIndex = self.preprocessArgs['startDimIndex']
        channelCnt = video.shape[-1]
        if (startDimIndex + 3) > channelCnt:
            raise ValueError("Start index is", startDimIndex, "but channel count is", channelCnt)

        if doDataAugmentation:
            # Do data augmentation
            video = self.__augmentAndStandardize(video)
        else:
            # Just normalize
            video = self.__standardizeOnly(video)

            # MOVED TO MAIN GENERATOR
        # Are we using the DPCC format?
        # if self.preprocessArgs['useDPCCFormat']:
        #    video = self.__changeToDPCCFormat(video)

        return video

    '''
    def __changeToDPCCFormat(self, video):
        newVideo = []
        for b in range(len(video)):
            newVideo.append(self.__changeSequenceToDPCCFormat(video[b]))            
        return np.array(newVideo)

    def __changeSequenceToDPCCFormat(self, data):

        #print("Data before DPCC:", data.shape)

        NNCount = int(self.preprocessArgs['DPCC_NNCount'])
        TimeRad = int(self.preprocessArgs['DPCC_TimeRad'])

        # Format now:
        # [frame, cloud, point, 3]
        # Format after:
        # [frame, cloud, point, 4 + 4 + 2*(NNCount)*(2*TimeRad + 1)]

        outChannelCnt = 4 + 4 + 2*(NNCount)*(2*TimeRad + 1)

        data = np.array(data)
        newShape = list(data.shape)
        newShape[-1] = outChannelCnt
        newData = np.zeros(shape=newShape)

        #print("Data about to DPCC:", newData.shape)

        cloudCnt = newShape[1]

        # For each cloud...
        for c in range(cloudCnt):


            # Make search trees for each frame
            searchTrees = []
            for i in range(len(data)):
                #print("TREE", i)
                tree = KDTree(data[i][c], leafsize=256)
                searchTrees.append(tree)

            #print("After trees")

            # For each frame...            
            for i in range(len(data)):
                # Get neighbor indices for surrounding frames
                pointNeighbors = np.array([])
                pCnt = len(data[i][c])

                #print("UP TO HERE FRAME", i)

                for t in range(-TimeRad+i, TimeRad+i+1):
                    tindex = min(max(t, 0), len(data))
                    # Query the tree


                    distances, nearestNeighbors = tree.query(data[i][c], k=NNCount) 
                    # DEBUG                   
                    #nearestNeighbors = (np.zeros(pCnt*NNCount))
                    #nearestNeighbors = np.reshape(nearestNeighbors, (pCnt, NNCount))                    
                    # END DEBUG

                    #print("UP TO HERE", t)

                    # Create time indices
                    timeList = np.full([pCnt, NNCount, 1], tindex)
                    nearestNeighbors = np.reshape(nearestNeighbors, [pCnt, NNCount, 1])
                    nearestNeighbors = np.concatenate([timeList, nearestNeighbors], axis=-1)
                    nearestNeighbors = np.reshape(nearestNeighbors, [pCnt, 2*NNCount])

                    # Append to master list of neighbors
                    if len(pointNeighbors) == 0:
                        pointNeighbors = nearestNeighbors
                    else:
                        pointNeighbors = np.concatenate((pointNeighbors, nearestNeighbors), axis=-1)

                # For each point...
                for j in range(len(data[i][c])):
                    # Set time coordinate
                    newData[i][c][j][0] = i
                    # Set point coordinates
                    newData[i][c][j][1:4] = data[i][c][j]
                    # Repeat coordinates
                    newData[i][c][j][4:8] = newData[i][c][j][0:4]
                    # Set neighbors
                    newData[i][c][j][8:] = pointNeighbors[j]

        #print("Data AFTER DPCC:", newData.shape)

        return newData
    '''

    def getPrecomputed(self):
        print("Warning: Data3DPreprocessor does not precompute anything.")
        return 0, 0, 0
        # return self.mean, self.std, self.principal_components

    def save(self, filename):
        print("Warning: Data3DPreprocessor does not save anything.")
        # np.savez(filename, mean=self.mean, std=self.std, pca=self.principal_components)

    def load(self, filename):
        print("Warning: Data3DPreprocessor does not load anything.")
        '''
        if os.path.exists(filename):
            # Open file and grab stuff
            npzfile = np.load(filename)
            self.mean = npzfile['mean']
            self.std = npzfile['std']
            self.principal_components = npzfile['pca']
            return True
        else:
            return False
        '''
