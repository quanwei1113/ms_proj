# MIT LICENSE
#
# Copyright 2018 Ben F. Klinghoffer, Hannah M. Szmurlo, Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import absolute_import, print_function

# from scipy.spatial import KDTree
from sklearn.neighbors import KDTree
from sklearn.neighbors import BallTree
# from scipy.spatial import cKDTree as KDTree
from scipy.spatial import distance_matrix, distance
import random
import h5py
import numpy as np
from scipy import ndimage
import datetime
from Data.ObjLoader import ObjLoader
from Data.BP4DFeaturePoints3D import *
from Data.Cached3DSequenceData import *


def getDigitCnt(cnt):
    digitCnt = 1
    while cnt >= 10:
        cnt /= 10
        digitCnt += 1
    return digitCnt


def padZero(index, digitCnt):
    curDigitCnt = getDigitCnt(int(index))
    diffDigit = digitCnt - curDigitCnt
    if diffDigit > 0:
        for i in range(diffDigit):
            index = "0" + index
    return index


def getImage(fileName):
    image = ndimage.imread(fileName)
    if len(image.shape) < 3:
        oldShape = image.shape
        image = np.reshape(image, (oldShape[0], oldShape[1], 1))
    return image


def extractPointsFromObj(objFile):
    allPoints = []

    f = open(objFile)
    for line in f:
        line = line.split()
        if len(line) <= 0:
            continue
        elif line[0] == 'v':
            temp = [float(point) for point in line[1:4]]
            allPoints.append(temp)

    random.shuffle(allPoints)

    return allPoints


def extractAllFromObj(objFile):
    allPoints = []
    allIndices = []
    allNormals = []

    f = open(objFile)
    for origline in f:
        line = origline.split()
        if len(line) <= 0:
            continue
        elif line[0] == 'v':
            temp = [float(point) for point in line[1:4]]
            allPoints.append(temp)
        elif line[0] == 'vn':
            temp = [float(point) for point in line[1:4]]
            allNormals.append(temp)
        elif line[0] == 'f':
            if len(line[1].split("/")) > 2:
                temp = [int(point.split("/")[0]) - 1 for point in line[1:4]]
            else:
                temp = [int(point) - 1 for point in line[1:4]]
            allIndices.append(temp)

    return allPoints, allIndices, allNormals


def extractPointsFromWrl(wrlFile):
    allPoints = []

    f = open(wrlFile)
    foundPoints = False

    for line in f:
        if foundPoints:
            if "0.000000 0.000000 0.000000," in line:
                continue
            elif "]" in line:
                break
            line = line.replace(',', '')
            sLine = line.split()
            temp = [float(point) for point in sLine]
            allPoints.append(temp)
        elif "point [" in line:
            foundPoints = True
    return allPoints


def extractFeaturePoints(tbndFile, startRow=1):
    featurePoints = []
    f = open(tbndFile)
    for line in f:
        line = line.split()
        featurePoints.append([float(x) for x in line])

    return featurePoints[startRow:]


def extractFeaturePointsBnd(bndFile):
    featurePoints = []
    neigborIndex = []
    f = open(bndFile)
    for line in f:
        line = line.split()
        neigborIndex.append(int(line[0]))
        featurePoints.append([float(num) for num in line[1:]])
    if np.max(featurePoints) < 20:
        return neigborIndex[:]
    return featurePoints[:]


def cropFace(allPoints, featurePoints, avgPointsIndex, chinIndex):
    nosePoints = [featurePoints[i] for i in avgPointsIndex]
    facePoint = featurePoints[chinIndex]
    noseAvg = np.average(nosePoints, axis=0)
    cropRange = distance.euclidean(noseAvg, facePoint)

    cropedPoints = []

    for point in allPoints:
        if distance.euclidean(noseAvg, point) < cropRange:
            cropedPoints.append(point)

    return cropedPoints[:]


def desiredFeatureIndexToPoints(desiredFeaturePoints, featurePoints):
    desiredPoints = []
    for point in desiredFeaturePoints:
        if isinstance(point, list):
            point = np.average([featurePoints[i] for i in point], axis=0)
        else:
            point = featurePoints[point]
        desiredPoints.append(point)
    return desiredPoints


def getRandomPointCloud(allPoints, featurePoints, params):
    if params.doFaceCrop:
        allPoints = cropFace(allPoints, featurePoints, params.cropFaceCenterPoints, params.cropFaceChinPoint)

    somePoints = allPoints[:params.maxPointCount]

    while len(somePoints) < params.maxPointCount:
        # print("Cloud too small (", len(somePoints), "points ), reusing data points...")
        correctionNumber = params.maxPointCount - len(somePoints)
        somePoints += somePoints[:correctionNumber]

    if params.isRelativeToPoints:
        # featurePoints = extractFeaturePoints(tbndFile)
        queryPoints = desiredFeatureIndexToPoints(params.desiredFeaturePoints, featurePoints)
        relativePoints = []

        for featurePoint in queryPoints:
            relativePoints.append(np.subtract(somePoints, featurePoint))

        return relativePoints
    else:
        somePoints = [somePoints]
        return somePoints


def getFeaturePointCloud(allPoints, featurePoints, params):
    if params.doFaceCrop:
        allPoints = cropFace(allPoints, featurePoints, params.cropFaceCenterPoints, params.cropFaceChinPoint)
    shuffledClouds = []

    # allPoints = extractPointsFromObj(objFile)
    # featurePoints = extractFeaturePoints(tbndFile)

    # print("Feature points:")
    # print(featurePoints)
    # print(params.desiredFeaturePoints)
    # print("End feature points")

    queryPoints = desiredFeatureIndexToPoints(params.desiredFeaturePoints, featurePoints)
    tree = BallTree(allPoints, leaf_size=16)  # 256)
    allClouds = tree.query_radius(queryPoints, r=params.cloudRadius)

    # cloudA = [x for x in allClouds[0] if x != len(allPoints)]
    # cloudA = [allPoints[i] for i in cloudA]
    # cloudB = [x for x in allClouds[1] if x != len(allPoints)]
    # cloudB = [allPoints[i] for i in cloudB]
    # open("TESTLEFTEYE.obj", "w").write(str(cloudA))
    # open("TESTRIGHTEYE.obj", "w").write(str(cloudB))
    # raise ValueError("!!!!!!!!!!!!!!")

    featureCount = 0

    for cloud in allClouds:
        # Remove all points that are actually out of bounds
        cloud = [x for x in cloud if x != len(allPoints)]
        # Shuffle the clouds
        random.shuffle(cloud)
        # ONLY grab the points we want
        if len(cloud) > params.maxPointCount:
            cloud = cloud[:params.maxPointCount]

        # If we don't have ENOUGH points...
        while (len(cloud) < params.maxPointCount):
            # print("Cloud too small (", len(cloud), "points ), reusing data points...")
            correctionNumber = params.maxPointCount - len(cloud)
            cloud += cloud[:correctionNumber]

        # Get actual points
        cloud = [allPoints[i] for i in cloud]

        # Get relative points if requested
        if params.isRelativeToPoints:
            relativeCloud = np.subtract(cloud, queryPoints[featureCount])
            shuffledClouds.append(relativeCloud)
            featureCount += 1
        else:
            shuffledClouds.append(cloud)

    '''
    # OLD KDTREE CODE
    tree = KDTree(allPoints, leafsize=256)

    closestClouds = []
    for queryPoint in queryPoints:
        # Get points within radius
        clouds = tree.query_ball_point(queryPoint, r=params.cloudRadius)        

        # Remove all points that are actually out of bounds
        clouds = [x for x in clouds if x != len(allPoints)]
        # Shuffle the clouds        
        random.shuffle(clouds)
        # ONLY grab the points we want
        if len(clouds) > params.maxPointCount:
            clouds = clouds[:params.maxPointCount]
        closestClouds.append(clouds)
        #print("CLOUD LEN:", len(clouds))
    '''
    '''
    # OLD VERSION
    featureCount = 0
    for cloud in closestClouds:
        if not cloud:
            raise ValueError("No values in range of feature point")
        cloud = [allPoints[i] for i in cloud]
        random.shuffle(cloud)
        while (len(cloud) < params.maxPointCount):
            print("Cloud too small (", len(cloud), "points ), reusing data points...")
            correctionNumber = params.maxPointCount - len(cloud)
            cloud += cloud[:correctionNumber]


        if params.isRelativeToPoints:
            relativeCloud = np.subtract(cloud[:params.maxPointCount], queryPoints[featureCount])
            shuffledClouds.append(relativeCloud)
            featureCount += 1
        else:
            shuffledClouds.append(cloud[:params.maxPointCount])
    '''

    return shuffledClouds


def poseNormalize(points, featureData):
    # Append 1 to each point
    ones = np.array([[1.0]] * len(points))
    hPoints = np.concatenate([points, ones], axis=1)

    # Get matrices
    modelCenter = featureData.getBackwardCenterTranslateMatrix()
    modelPose = featureData.getBackwardPoseMatrix()
    model = np.matmul(modelPose, modelCenter)

    # Pose correct
    normPoints = np.matmul(hPoints, model)

    # Remove the homogeneous coordinates
    normPoints = normPoints[:, 0:3]

    return normPoints


# DEBUG_alreadySaved = False

def saveAsOBJ(points, filename):
    with open(filename, 'w') as f:
        for i in range(len(points)):
            s = "v "
            for j in range(3):
                s += str(points[i][j]) + " "
            s += "\n"
            f.write(s)


def getPointCloud(objFile, tbndFile, params):
    if params.imageDataType == "OBJ":
        allPoints = extractPointsFromObj(objFile)
        featurePoints = extractFeaturePoints(tbndFile)
    elif params.imageDataType == "WRL":
        allPoints = extractPointsFromWrl(objFile)
        featurePoints = extractFeaturePointsBnd(tbndFile)
        if isinstance(featurePoints[0], int):
            featurePoints = [allPoints[index] for index in featurePoints]

    # global DEBUG_alreadySaved
    # if not DEBUG_alreadySaved:
    #    saveAsOBJ(allPoints, "JUNK/beforeModel.obj")
    #    saveAsOBJ(featurePoints, "JUNK/beforePoints.obj")

    if params.doPoseNormalize:
        featureData = BP4DFeaturePoints3D(tbndFile)
        allPoints = poseNormalize(allPoints, featureData)
        featurePoints = poseNormalize(featurePoints, featureData)

    # if not DEBUG_alreadySaved:
    #    saveAsOBJ(allPoints, "JUNK/afterModel.obj")
    #    saveAsOBJ(featurePoints, "JUNK/afterPoints.obj")

    if params.useFullModel:
        pointCloud = getRandomPointCloud(allPoints, featurePoints, params)
    else:
        pointCloud = getFeaturePointCloud(allPoints, featurePoints, params)

    # if not DEBUG_alreadySaved:
    #    for i in range(len(pointCloud)):
    #        saveAsOBJ(pointCloud[i], "JUNK/pointClouds" + str(i) + ".obj")
    #
    # DEBUG_alreadySaved = True

    return pointCloud


def convertToDPCC(data, params, sampleIndex, frameIndex, localSequenceCache,
                  allImageFiles, ext3D, extBnd):
    # startTime = datetime.datetime.now()

    # Format now:
    # [cloud, point, 3]
    # Format after:
    # [cloud, point, 4 + 2*(NNCount)*(2*TimeRad + 1)]

    NNCount = params.DPCC_NNCount
    TimeRad = params.DPCC_TimeRad
    SpaceRad = params.DPCC_SpaceRad

    outChannelCnt = 4 + 2 * (NNCount) * (2 * TimeRad + 1)

    data = np.array(data)
    # print("Data shape =", data.shape)
    newShape = list(data.shape)
    newShape[-1] = outChannelCnt
    newData = np.zeros(shape=newShape)
    cloudCnt = newShape[0]

    leaf_size = 16  # 256

    '''
    if TimeRad > 0:
        raise ValueError("NYI: Need efficient neighbor search for adjacent frames (and KD doesn't help).")

    # Compute distance matrix
    allDistMat = []
    for c in range(cloudCnt):
        distMat = distance_matrix(data[c], data[c])
        allDistMat.append(distMat)
    '''

    # print("Data about to DPCC:", newData.shape)

    # OK, here's the strategy.
    # We're going to cache information as necessary to get what we need.
    # If our cache sampleIndex != sampleIndex, need to start over.
    # Given our cache:
    #   - Look for frame (use bounds checking to clamp)
    #   - If we have the frame, use existing tree for search.
    #   - Otherwise, create tree and store for next time.

    # print("Sample index =", sampleIndex, ", Frame index =", frameIndex)

    # Check sample index on cache
    if not localSequenceCache.isSameSample(sampleIndex):
        # Reset cache
        localSequenceCache.reset(sampleIndex)
        # print("Reset cache", sampleIndex)

    # Add this frame (since we already have it)
    for c in range(cloudCnt):
        if not localSequenceCache.hasTree(frame=frameIndex, cloud=c):
            # Have to create new tree
            # tree = KDTree(data[c], leafsize=leaf_size)
            tree = BallTree(data[c], leaf_size=leaf_size)
            localSequenceCache.setTree(frame=frameIndex, cloud=c, tree=tree)
            # localSequenceCache.setData(frame=frameIndex, cloud=c, data=data[c])

    # endTime = datetime.datetime.now()
    # print("Tree build Time:", (endTime-startTime))
    # startTime = datetime.datetime.now()

    # For each cloud...
    for c in range(cloudCnt):

        # Get neighbor indices for surrounding frames
        pointNeighbors = np.array([])
        pCnt = len(data[c])

        for t in range(-TimeRad + frameIndex, TimeRad + frameIndex + 1):
            # for t in range(-2*TimeRad+frameIndex, frameIndex+1):
            # Make sure we don't go out of bounds
            tindex = min(max(t, 0), localSequenceCache.getFrameCnt() - 1)

            # KDTREE QUERY
            # Do we have a tree for this frame?
            if not localSequenceCache.hasTree(frame=tindex, cloud=c):
                # print("Have to load frame", tindex)
                # We have to load the entire FRAME
                otherFrame = np.array(getPointCloud(
                    params.baseDir + '/' + str(allImageFiles[sampleIndex][tindex]) + ext3D,
                    params.tbndBaseDir + '/' + str(allImageFiles[sampleIndex][tindex]) + extBnd,
                    params))
                # Have to create new trees
                for otherCloud in range(len(otherFrame)):
                    # tree = KDTree(otherFrame[otherCloud], leafsize=leaf_size)
                    tree = BallTree(otherFrame[otherCloud], leaf_size=leaf_size)
                    localSequenceCache.setTree(frame=tindex, cloud=otherCloud, tree=tree)
                    # localSequenceCache.setData(frame=tindex, cloud=otherCloud, data=otherFrame[otherCloud])

            # Get the tree
            tree = localSequenceCache.getTree(frame=tindex, cloud=c)

            # Query the tree
            # nearestNeighbors = tree.query_ball_point(data[c], r=SpaceRad)
            nearestNeighbors = tree.query_radius(data[c], r=SpaceRad)
            nearestNeighbors = nearestNeighbors.tolist()

            '''
            # BRUTE FORCE SEARCH
            nearestNeighbors = []
            for centerP in range(pCnt):
                currentPointNeighbors = []
                for p in range(pCnt):
                    if allDistMat[c][centerP][p] <= SpaceRad:
                        currentPointNeighbors.append(p)                
                nearestNeighbors.append(currentPointNeighbors)
            '''

            # print("NN SHAPE:", len(nearestNeighbors))

            # For each neighbor cloud
            newNeighborList = []
            for p in range(pCnt):
                nearestNeighbors[p] = nearestNeighbors[p].tolist()

                if len(nearestNeighbors[p]) == 0:
                    np.set_printoptions(suppress=True)
                    print("NO POINTS????")
                    print(nearestNeighbors[p])
                    print(p)
                    print(len(nearestNeighbors))
                    print("POINT:", repr(data[c][p]))
                    print("ALL POINTS:", repr(data[c]))
                    print("TREE DATA:", repr(localSequenceCache.getData(frame=tindex, cloud=c)))
                    # print("TREE POINTS:", repr(tree.data))
                    raise ValueError("STOP!!!")

                # Shuffle the clouds
                random.shuffle(nearestNeighbors[p])
                # ONLY grab the points we want
                # print(p,"NN size:", len(nearestNeighbors[p]))
                if len(nearestNeighbors[p]) > NNCount:
                    nearestNeighbors[p] = nearestNeighbors[p][:NNCount]

                # What if we don't have enough?
                if len(nearestNeighbors[p]) == 0:
                    print("NO POINTS!!! Adding self...")
                    nearestNeighbors[p] = [p]

                if (len(nearestNeighbors[p]) < NNCount):
                    # Too small, add repeats
                    while (len(nearestNeighbors[p]) < NNCount):
                        # print("Cloud too small (", len(nearestNeighbors[p]), "points ), reusing data points...")
                        correctionNumber = NNCount - len(nearestNeighbors[p])
                        # print("Adding", correctionNumber)
                        # print(nearestNeighbors[p].shape)
                        nearestNeighbors[p] += nearestNeighbors[p][:correctionNumber]

                # print(p,"NN size now:", len(nearestNeighbors[p]))
                nearestNeighbors[p] = np.asarray(nearestNeighbors[p])
                # print(p,"NN shape:", nearestNeighbors[p].shape)
                newNeighborList.append(nearestNeighbors[p])

            # At this point, everything should be even
            nearestNeighbors = np.asarray(newNeighborList)
            # print("ALL NN shape:", nearestNeighbors.shape)
            # for p in range(pCnt):
            #    print(p,"NN shape:", nearestNeighbors[p].shape)

            # Create time indices
            timeList = np.full([pCnt, NNCount, 1], tindex)
            nearestNeighbors = np.reshape(nearestNeighbors, [pCnt, NNCount, 1])
            nearestNeighbors = np.concatenate([timeList, nearestNeighbors], axis=-1)
            nearestNeighbors = np.reshape(nearestNeighbors, [pCnt, 2 * NNCount])

            # Append to master list of neighbors
            if len(pointNeighbors) == 0:
                pointNeighbors = nearestNeighbors
            else:
                pointNeighbors = np.concatenate((pointNeighbors, nearestNeighbors), axis=-1)

        # For each point...
        for j in range(len(data[c])):
            # Set time coordinate
            newData[c][j][0] = frameIndex
            # Set point coordinates
            newData[c][j][1:4] = data[c][j]
            # Set neighbors
            newData[c][j][4:] = pointNeighbors[j]

    # print("Data AFTER DPCC:", newData.shape)

    # endTime = datetime.datetime.now()
    # print("DPCC Time:", (endTime-startTime))

    return newData


def getH5Data(fileName, datasetName, imageNumber, sliceStart, sliceEnd):
    f = h5py.File(fileName, 'r')
    # List all groups
    # Get the data
    data = np.array(list(f[datasetName])[sliceStart:sliceEnd])
    data = data.reshape(len(data), -1, 3)
    np.random.shuffle(data)

    if imageNumber < len(data):
        return data[:imageNumber]
    elif imageNumber == 0:
        return data
    else:
        print("Number of images requested too large, returning all images...")
        return data


def getH5Slice(fileName, datasetName, sliceStart, sliceEnd):
    f = h5py.File(fileName, 'r')
    data = np.array(list(f[datasetName])[sliceStart:sliceEnd])
    return data


def appendToData(data, params):
    outChannelCnt = 3
    # if params.outputExtraPointCoords:
    #    outChannelCnt += 3
    # if params.outputNeighborIndices:
    #    outChannelCnt += params.nearestNeighborCnt

    data = np.array(data)
    newShape = list(data.shape)
    newShape[-1] = outChannelCnt
    newData = np.zeros(shape=newShape)

    for i in range(len(data)):
        # if params.outputNeighborIndices:
        #    tree = KDTree(data[i], leafsize=256)
        #    dist, nearestNeighbors = tree.query(data[i], k=params.nearestNeighborCnt)

        for j in range(len(data[i])):
            newData[i][j][0:3] = data[i][j][0:3]
            start = 3
            # if params.outputExtraPointCoords:
            #    newData[i][j][start:(start+3)] = data[i][j][0:3]
            #    start += 3
            # if params.outputNeighborIndices:
            #    newData[i][j][start:(start+params.nearestNeighborCnt)] = nearestNeighbors[j]

    # print("DATA SHAPE:", data.shape)

    return newData


'''
def setupH5Data(frameCnt, fileEnding, params, args):

    fullData = []

    for i in range(frameCnt):

        random.seed(i)

        channelCnt = 0
        H5Color = None 
        H5Depth = None 
        H53D = None
        H5Normal = None
        H5NeighborTime = None
        timeTree = None
        tree = None

        # initialize data and count ending channel size
        for arg in args:
            if arg == "FT_COLOR":
                if H5Color is None:
                    path = params.baseDir + "/ColorData" + fileEnding
                    H5Color = getH5Slice(path, "colorData", i, i+1)
                    H5Color = np.reshape(H5Color, (len(H5Color), -1, 3))
                    random.shuffle(H5Color[0])
                channelCnt += 3
            elif arg == "FT_DEPTH":
                if H5Depth is None:
                    path = params.baseDir + "/DepthData" + fileEnding
                    H5Depth = getH5Slice(path, "depthData", i, i+1)
                    H5Depth = np.reshape(H5Depth, (len(H5Depth), -1, 3))
                    random.shuffle(H5Depth[0])
                channelCnt += 1
            elif arg == "FT_3D":
                if H53D is None:
                    path = params.baseDir + "/PreciseDepthData" + fileEnding
                    H53D = getH5Slice(path, "preciseData", i, i+1)
                    H53D = np.reshape(H53D, (len(H53D), -1, 3))
                    random.shuffle(H53D[0])
                channelCnt += 3
            elif arg == "FT_TIME":
                channelCnt += 1
            elif arg == "FT_NORMAL":
                if H5Normal is None:
                    path = params.baseDir + "/NormalData" + fileEnding
                    H5Normal = getH5Slice(path, "normalData", i, i+1)
                    H5Normal = np.reshape(H5Normal, (len(H5Normal), -1, 3))
                    random.shuffle(H5Normal[0])
                channelCnt += 3
            elif arg == "FT_2D":
                size = len(getH5Slice(params.baseDir + "/ColorData" + fileEnding, "colorData", 0, 1)[0])
                channelCnt += 2
            elif arg == "FT_DIST":
                #featurePoints = extractFeaturePoints(tbndFile)
                #queryPoints = [featurePoints[i] for i in params.desiredPoints]
                #channelCnt += params.desiredPoints
                print("FT_DIST not yet implemented")
            elif arg == "FT_NEIGHBORS":
                if H53D is None:
                    path = params.baseDir + "/PreciseDepthData" + fileEnding
                    H53D = getH5Slice(path, "preciseData", i, i + 1)
                    H53D = np.reshape(H53D, (len(H53D), -1, 3))
                    random.shuffle(H53D[0])
                if tree is None:
                    tree = KDTree(H53D[0][0:params.maxPointCount], leafsize=512)
                channelCnt += params.nearestNeighborCnt
            elif arg == "FT_NEIGHBORS_TIME":
                if H5NeighborTime is None:
                    if i - params.timeRadius < 0:
                        timeMin = 0
                        timeMax = i + params.timeRadius
                    elif i + params.timeRadius >= frameCnt:
                        timeMin = i - params.timeRadius
                        timeMax = frameCnt - 1
                    else:
                        timeMin = i - params.timeRadius
                        timeMax = i + params.timeRadius
                    path = params.baseDir + "/PreciseDepthData" + fileEnding
                    H5NeighborTime = getH5Slice(path, "preciseData", timeMin, timeMax)
                    H5NeighborTime = np.reshape(H5NeighborTime, (len(H5NeighborTime), -1, 3))
                    smallH5NeighborTime = []
                    for seed in range(timeMin, timeMax):
                        random.seed(seed)
                        random.shuffle(H5NeighborTime[seed-timeMin])
                        smallH5NeighborTime.append(H5NeighborTime[seed - timeMin][0:params.maxPointCount])
                    random.seed(i)
                    dataForTree = np.reshape(smallH5NeighborTime, (-1, 3))
                    timeTree = KDTree(dataForTree, leafsize=512)
                channelCnt += (params.nearestNeighborCnt*2)    

            data = np.zeros(shape=(params.maxPointCount, channelCnt))

        #
        for j in range(len(data)): 
            start = 0
            for arg in args:
                if arg == "FT_COLOR":
                    data[j][start:(start+3)] = H5Color[0][j]
                    start += 3
                elif arg == "FT_DEPTH":
                    data[j][start:(start+1)] = H5Depth[0][j][0]
                    start += 1
                elif arg == "FT_3D":
                    data[j][start:(start+3)] = H53D[0][j]
                    start += 3
                elif arg == "FT_TIME":
                    data[j][start:(start+1)] = i
                    start += 1
                elif arg == "FT_NORMAL":
                    data[j][start:(start+3)] = H5Normal[0][j]
                    start += 3
                elif arg == "FT_2D":
                    data[j][start:(start+2)] = [(j % size), int(j/size)]
                    start += 2 
                elif arg == "FT_DIST":
                    #for featurePoint in queryPoints:
                     #   data[j][start:(start + 1)] = spatial.distance.euclidean(featurePoint,data[j])
                      #  start += 1
                    pass
                elif arg == "FT_NEIGHBORS":
                    _, nearestNeighbors = tree.query(H53D[0][j], k=params.nearestNeighborCnt)
                    print(nearestNeighbors)
                    data[j][start:(start+params.nearestNeighborCnt)] = nearestNeighbors
                    start += params.nearestNeighborCnt
                elif arg == "FT_NEIGHBORS_TIME":
                    _, nearestNeighbors = timeTree.query(smallH5NeighborTime[0][j-timeMin], k=params.nearestNeighborCnt)
                    timeNeighborList = []
                    for nearestNeighbor in nearestNeighbors:
                        DataIndex = nearestNeighbor % len(smallH5NeighborTime[0])
                        timeIndex = int(nearestNeighbor / len(smallH5NeighborTime[0]))
                        timeIndex = timeIndex + timeMin
                        timeNeighborList.append(timeIndex)
                        timeNeighborList.append(DataIndex)
                    data[j][start:start+(params.nearestNeighborCnt*2)] = timeNeighborList
                    start += (params.nearestNeighborCnt*2)
        fullData.append(data)
    return fullData


def setupOBJData(fileList, params, args):

    fullData = []
    loaders = []
    fullTbnd = []

    for file in fileList:
        loaders.append(ObjLoader(params.baseDir + "/" + file))
        fullTbnd.append(params.tbndDir + "/" + file)

    for i in range(len(fileList)):

        channelCnt = 0
        tree = None
        timeTree = None

        for arg in args:
            if arg == "FT_COLOR":
                colorClouds = getPointCloud(loaders[i].vertices, fullTbnd[i], params)
                channelCnt += 3
            elif arg == "FT_DEPTH":
                print("FT_DEPTH is not supported for OBJ data")
            elif arg == "FT_3D":
                DS3Clouds = getPointCloud(loaders[i].vertices, fullTbnd[i], params)
                channelCnt += 3
            elif arg == "FT_TIME":
                channelCnt += 1
            elif arg == "FT_NORMAL":
                normalClouds = getPointCloud(loaders[i].vertices, fullTbnd[i], params)
                channelCnt += 3
            elif arg == "FT_2D":
                print("FT_2D is not supported for OBJ data")
            elif arg == "FT_DIST":
                featurePoints = extractFeaturePoints(fullTbnd[i])
                queryPoints = [featurePoints[i] for i in params.desiredPoints]
                channelCnt += params.desiredPoints
            elif arg == "FT_NEIGHBORS":
                if tree is None:
                    tree = KDTree(loaders[i].vertices, leafsize=256)
                channelCnt += params.nearestNeighborCnt
            elif arg == "FT_NEIGHBORS_TIME":
                if beenHereBefore is None:
                    if i - params.timeRadius < 0:
                        timeMin = 0
                        timeMax = i + params.timeRadius
                    elif i + params.timeRadius >= len(fileList):
                        timeMin = i - params.timeRadius
                        timeMax = len(fileList) - 1
                    else:
                        timeMin = i - params.timeRadius
                        timeMax = i + params.timeRadius
                channelCnt += (params.nearestNeighborCnt*2)

        data = np.zeros(shape = (len(params.desiredPoints),params.maxPointCount, channelCnt))


        for j in range(len(data)):

            if "FT_NEIGHBORS_TIME" in args:
                timePoints = []
                for timeCnt in range(timeMax):
                    timePoints.append(getPointCloud(loaders[timeCnt + timeMin],fullTbnd[timeCnt + timeMin], params))
                dataForTree = np.reshape(timePoints, (-1, channelCnt))
                timeTree = KDTree(dataForTree, leafsize=256)

            for k in range(len(data[j])):
                start = 0
                for arg in args:
                    if arg == "FT_COLOR":
                        data[j][k][start:(start+3)] = colorClouds[j][k]
                        start += 3
                    elif arg == "FT_DEPTH":
                        pass
                    elif arg == "FT_3D":
                        data[j][k][start:(start+3)] = DS3Clouds[j][k]
                        start += 3
                    elif arg == "FT_TIME":
                        data[j][k][start:(start+1)] = i
                        start += 1
                    elif arg == "FT_NORMAL":
                        data[j][k][start:(start+3)] = normalClouds[j][k]
                        start += 3
                    elif arg == "FT_2D":
                        pass
                    elif arg == "FT_DIST":
                        for featurePoint in queryPoints:
                            data[j][k][start:(start + 1)] = spatial.distance.euclidean(featurePoint,data[j])
                            start += 1
                    elif arg == "FT_NEIGHBORS":
                        #Query can be done here or in the setup above, unsure of what is better...
                        dist, nearestNeighbors = tree.query(loaders[i].vertices[j], k=params.nearestNeighborCnt)
                        data[j][k][start:(start+params.nearestNeighborCnt)] = nearestNeighbors[j]
                        start += params.nearestNeighborCnt
                    elif arg == "FT_NEIGHBORS_TIME":
                        _, nearestNeighbors = timeTree.query(timePoints[k][j - timeMin],
                                                             k=params.nearestNeighborCnt)
                        timeNeighborList = []
                        for nearestNeighbor in nearestNeighbors:
                            DataIndex = nearestNeighbor % len(timePoints[k])
                            timeIndex = int(nearestNeighbor / len(timePoints[k]))
                            timeIndex = timeIndex + timeMin
                            timeNeighborList.append(timeIndex)
                            timeNeighborList.append(DataIndex)
                        data[j][start:start + (params.nearestNeighborCnt * 2)] = timeNeighborList
                        start += (params.nearestNeighborCnt * 2)
        fullData.append(data)
    return fullData

def setupImageData(fileList, tbndList, params, args):

    #file here are weird look at thrm

    fullData = []

    for i in range(len(fileList)):
        channelCnt = 0
        colorImage = None
        depthImage = None
        normalImage = None

        for arg in args:
            if arg == "FT_COLOR":
                if colorImage is None:
                    path = params.baseDir + "/COLOR" + str(fileEnding[i])
                    colorImage = getImage(path)
                channelCnt += 3
            elif arg == "FT_DEPTH":
                if depthImage is None:
                    path = params.baseDir + "/DEPTH" + str(fileEnding[i])
                    depthImage = getImage(path)
                channelCnt += 1
            elif arg == "FT_3D":
                print("FT_3D is not supported for image data")
            elif arg == "FT_TIME":
                channelCnt += 1
            elif arg == "FT_NORMAL":
                if normalImage is None:
                    path = params.baseDir + "/NORMAL" + str(fileEnding[i])
                    normalImage = getImage(path)
                channelCnt += 3
            elif arg == "FT_2D":
                channelCnt += 2 
            elif arg == "FT_DIST":
                print("FT_DIST is not supported for image data")
                #distance is odd for images, we'll get here eventually
            elif arg == "FT_NEIGHBORS":
                print("FT_NEIGHBORS is not supported for image data")
            elif arg == "FT_NEIGHBORS_TIME":
                print("FT_NEIGHBORS_TIME is not supported for image data")

        data = np.zeros(shape = (len(image)*(len(image[0])), channelCnt))

        for j in range(len(image)*(len(image[0]))): 
            start = 0
            for arg in args:
                if arg == "FT_COLOR":
                    data[j][start:(start+3)] =
                    start += 3
                elif arg == "FT_DEPTH":
                    pass
                elif arg == "FT_3D":
                    pass
                elif arg == "FT_TIME":
                    data[j][start:(start+1)] = i
                    start += 1
                elif arg == "FT_NORMAL":
                    data[j][start:(start+3)] = objLoader.normal_index[j]
                    start += 3
                elif arg == "FT_2D":
                    data[j][start:(start+2)] = [(j % np.sqrt(len(data))), int(j / np.sqrt(len(data)))]
                elif arg == "FT_DIST":
                    pass
                elif arg == "FT_NEIGHBORS":
                    pass
                elif arg == "FT_NEIGHBORS_TIME":
                    pass
'''
