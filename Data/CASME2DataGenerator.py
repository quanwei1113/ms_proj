from os.path import join, isfile
from os import listdir
from os.path import splitext, isdir, basename
from os import rename
import csv
import glob
import pandas as pd
from Data.BaseDataGenerator import *
from Data.DataLoadParams import *
from Data.CommonFunctions import *
import cv2
import sys
import numpy as np

# class for index to be used in lists, as well as
# parsed list

# list of valid/desired subjects
allSubjects_C2 = ["sub01", "sub02", "sub03", "sub04",
                  "sub05", "sub06", "sub07", "sub08",
                  "sub09", "sub10", "sub11", "sub12",
                  "sub13", "sub14", "sub15", "sub16",
                  "sub17", "sub18", "sub19", "sub20",
                  "sub21", "sub22", "sub23", "sub24",
                  "sub25", "sub26"]

trainSubjects_C2 = ['sub01', 'sub03', 'sub05', 'sub07', 'sub09', 'sub11', 'sub13', 'sub15', 'sub17', 'sub19', 'sub21',
                    'sub23', 'sub25']
testSubjects_C2 = ['sub02', 'sub04', 'sub06', 'sub08', 'sub10', 'sub12', 'sub14', 'sub16', 'sub18', 'sub20', 'sub22',
                   'sub24', 'sub26']

allAU_C2 = ['AU01',
            'AU02',
            'AU04',
            'AU05',
            'AU06',
            'AU07',
            'AU09',
            'AU10',
            'AU12',
            'AU14',
            'AU15',
            'AU16',
            'AU17',
            'AU18',
            'AU20',
            'AU24',
            'AU25',
            'AU26',
            'AU38']
allTasks_C2 = []

allExp_C2 = ["neutral", "happiness", "others", "disgust",
             "repression", "surprise", "fear",
             "sadness"]

frameCnts_C2 = {'sub05/EP09_05f': 424, 'sub12/EP04_16': 288, 'sub13/EP09_10': 222, 'sub18/EP18_01': 433,
                'sub16/EP04_02f': 649, 'sub26/EP07_37': 545, 'sub19/EP02_01': 168, 'sub17/EP03_02': 380,
                'sub08/EP13_01f': 461, 'sub17/EP03_09': 687, 'sub01/EP19_05f': 579, 'sub23/EP12_02f': 700,
                'sub12/EP02_05': 267, 'sub20/EP06_03': 173, 'sub19/EP01_02f': 285, 'sub10/EP08_01f': 203,
                'sub23/EP12_03': 291, 'sub05/EP02_07': 140, 'sub17/EP02_03': 356, 'sub10/EP12_03f': 120,
                'sub16/EP01_05': 156, 'sub23/EP04_03f': 133, 'sub11/EP08_01f': 160, 'sub09/EP13_01': 161,
                'sub11/EP12_03f': 275, 'sub09/EP13_02': 225, 'sub11/EP13_03f': 170, 'sub13/EP01_01': 224,
                'sub14/EP09_03': 51, 'sub17/EP05_03': 108, 'sub17/EP05_02': 153, 'sub23/EP05_24f': 694,
                'sub10/EP13_03f': 152, 'sub17/EP11_02': 258, 'sub07/EP18_03': 182, 'sub17/EP05_09': 317,
                'sub07/EP18_01': 140, 'sub10/EP16_02f': 132, 'sub17/EP05_03f': 115, 'sub20/EP03_02': 224,
                'sub25/EP12_01': 364, 'sub04/EP13_06f': 236, 'sub17/EP08_02': 133, 'sub17/EP08_03': 152,
                'sub17/EP18_07': 135, 'sub26/EP13_01': 74, 'sub17/EP02_18f': 303, 'sub16/EP01_09f': 196,
                'sub07/EP06_02_02': 133, 'sub14/EP09_06': 94, 'sub09/EP15_05': 200, 'sub24/EP10_03': 876,
                'sub17/EP01_06': 199, 'sub11/EP18_03f': 354, 'sub05/EP05_03': 248, 'sub10/EP18_01f': 121,
                'sub17/EP15_05': 106, 'sub17/EP15_03': 658, 'sub11/EP13_02f': 154, 'sub23/EP17_01': 221,
                'sub05/EP05_09': 234, 'sub03/EP19_08': 291, 'sub13/EP02_02': 92, 'sub19/EP13_01': 297,
                'sub01/EP02_01f': 290, 'sub09/EP09_04': 220, 'sub09/EP09_05': 169, 'sub02/EP15_04': 167,
                'sub17/EP16_01f': 87, 'sub01/EP19_03f': 406, 'sub05/EP16_04f': 409, 'sub09/EP06_02f': 201,
                'sub20/EP10_02': 309, 'sub10/EP12_02f': 118, 'sub09/EP05_05': 169, 'sub09/EP05_02': 271,
                'sub09/EP05_03': 279, 'sub05/EP08_05': 137, 'sub01/EP19_06f': 300, 'sub17/EP13_06': 332,
                'sub26/EP13_11': 221, 'sub17/EP13_01': 286, 'sub17/EP13_03': 254, 'sub25/EP10_10': 207,
                'sub07/EP03_04': 106, 'sub12/EP09_06': 233, 'sub18/EP19_01': 433, 'sub17/EP01_13': 354,
                'sub12/EP09_02': 209, 'sub17/EP01_15': 223, 'sub25/EP09_02': 232, 'sub21/EP01_07': 91,
                'sub26/EP08_04': 154, 'sub17/EP06_07': 360, 'sub17/EP06_04': 187, 'sub02/EP11_01': 373,
                'sub17/EP06_08': 360, 'sub10/EP06_01f': 157, 'sub05/EP06_10': 300, 'sub10/EP11_01f': 97,
                'sub01/EP03_02': 231, 'sub17/EP15_01': 403, 'sub24/EP18_03': 321, 'sub16/EP01_08': 219,
                'sub26/EP16_01': 88, 'sub11/EP15_01f': 210, 'sub01/EP04_04': 255, 'sub02/EP09_06f': 367,
                'sub12/EP16_02': 411, 'sub01/EP04_03': 82, 'sub05/EP03_07': 158, 'sub20/EP18_03': 342,
                'sub25/EP03_02': 164, 'sub26/EP18_50': 239, 'sub26/EP18_51': 283, 'sub04/EP12_01f': 773,
                'sub02/EP13_04': 143, 'sub20/EP07_04': 388, 'sub20/EP16_01': 475, 'sub05/EP03_01': 218,
                'sub25/EP03_01': 181, 'sub05/EP04_05': 69, 'sub01/EP19_01': 164, 'sub05/EP04_06': 105,
                'sub17/EP07_01': 170, 'sub07/EP01_01': 234, 'sub03/EP18_06': 140, 'sub18/EP08_01': 300,
                'sub02/EP02_04f': 160, 'sub17/EP05_10': 239, 'sub24/EP07_01': 119, 'sub05/EP03_06': 173,
                'sub26/EP18_47': 181, 'sub19/EP06_01f': 376, 'sub10/EP10_01f': 194, 'sub01/EP04_02': 219,
                'sub17/EP15_04': 201, 'sub12/EP01_02': 316, 'sub11/EP19_03f': 194, 'sub10/EP13_02f': 156,
                'sub06/EP10_08': 321, 'sub15/EP08_02': 109, 'sub09/EP09f': 90, 'sub19/EP16_01': 264,
                'sub13/EP12_01': 97, 'sub19/EP16_02': 266, 'sub19/EP08_02': 305, 'sub23/EP13_03': 99,
                'sub23/EP13_04': 123, 'sub26/EP09_09': 193, 'sub05/EP13_04f': 151, 'sub03/EP09_03': 150,
                'sub19/EP19_04': 163, 'sub07/EP08_02': 269, 'sub02/EP08_04': 163, 'sub20/EP16_04': 251,
                'sub19/EP19_01': 270, 'sub11/EP13_05f': 176, 'sub19/EP19_03': 200, 'sub20/EP13_02': 409,
                'sub26/EP09_04': 199, 'sub23/EP07_01': 167, 'sub02/EP06_02f': 120, 'sub10/EP12_04': 155,
                'sub24/EP01_08': 191, 'sub19/EP15_03f': 144, 'sub20/EP12_01': 239, 'sub08/EP12_08f': 285,
                'sub04/EP13_02f': 183, 'sub04/EP19_01f': 370, 'sub13/EP01_02': 132, 'sub05/EP16_03f': 183,
                'sub17/EP02_11': 468, 'sub06/EP16_05': 228, 'sub10/EP19_04f': 152, 'sub19/EP06_02f': 482,
                'sub26/EP13_02': 180, 'sub25/EP10_01': 242, 'sub09/EP02_01f': 95, 'sub19/EP19_02': 385,
                'sub06/EP01_01': 236, 'sub26/EP18_49': 279, 'sub19/EP11_04f': 464, 'sub02/EP01_11f': 145,
                'sub17/EP13_09': 254, 'sub02/EP03_02f': 190, 'sub24/EP08_02': 614, 'sub07/EP15_01': 200,
                'sub23/EP13_07f': 155, 'sub05/EP19_03': 151, 'sub13/EP02_03': 111, 'sub24/EP07_04f': 442,
                'sub17/EP13_04': 174, 'sub26/EP18_44': 118, 'sub24/EP10_01f': 800, 'sub06/EP02_31': 276,
                'sub12/EP06_06': 191, 'sub22/EP01_12': 337, 'sub17/EP02_01': 296, 'sub12/EP08_07': 339,
                'sub26/EP18_46': 134, 'sub12/EP08_01': 252, 'sub12/EP08_03': 327, 'sub17/EP10_06': 244,
                'sub02/EP06_01f': 182, 'sub09/EP06_01f': 388, 'sub10/EP13_01': 124, 'sub17/EP05_04': 108,
                'sub12/EP03_04': 228, 'sub15/EP03_02': 89, 'sub23/EP02_01': 439, 'sub05/EP13_06': 121,
                'sub23/EP05_25f': 700, 'sub23/EP03_14f': 546, 'sub02/EP09_01': 243, 'sub24/EP10_02': 514,
                'sub19/EP01_01f': 385, 'sub07/EP06_02_01': 113, 'sub08/EP12_07f': 443, 'sub24/EP12_01': 373,
                'sub11/EP02_06f': 136, 'sub26/EP03_10': 343, 'sub05/EP12_06': 151, 'sub09/EP17_08': 129,
                'sub21/EP05_02': 291, 'sub17/EP11_01': 86, 'sub07/EP06_01': 91, 'sub05/EP07_01': 300,
                'sub26/EP07_28': 158, 'sub20/EP15_03f': 1024, 'sub25/EP18_04f': 448, 'sub10/EP11_01': 139,
                'sub22/EP13_08': 492, 'sub24/EP02_02f': 488, 'sub14/EP04_04f': 89, 'sub03/EP01_2': 107,
                'sub05/EP12_03f': 93, 'sub20/EP01_03': 306, 'sub13/EP03_01': 132, 'sub19/EP11_01f': 196,
                'sub06/EP15_02': 137, 'sub11/EP15_04f': 185, 'sub02/EP14_01': 200, 'sub03/EP08_1': 105,
                'sub15/EP04_02': 182, 'sub04/EP12_02f': 743, 'sub13/EP08_01': 163, 'sub02/EP09_10': 73,
                'sub17/EP12_03': 409, 'sub03/EP07_03': 88, 'sub09/EP18_03': 118, 'sub14/EP09_04': 146,
                'sub12/EP03_02': 143, 'sub03/EP07_04': 136, 'sub26/EP15_01': 127}

INPUT_DATATYPE_IMAGE = "IMAGE"
INPUT_DATATYPE_OBJ = "OBJ"
INPUT_DATATYPE_H5_2D = "H5_2D"
INPUT_DATATYPE_H5_DEPTH = "H5_DEPTH"
INPUT_DATATYPE_H5_NORMAL = "H5_NORMAL"


def CASMEmain():
    batchSize = 32
    baseDataParams = DataLoadParams()
    baseDataParams.baseGroundPath = "/media/Data/CASME2/"
    baseDataParams.baseDir = "/media/Data/CASME2/CASME2-RAW"
    CASME2Generator = CASME2DataGenerator(baseDataParams)
    CASME2Generator.initialize(batchSize)


class imageIndex():
    def __init__(self, nameNumber, index):
        self.index = index
        self.nameNumber = nameNumber


class CASME2DataGenerator(BaseDataGenerator):
    # TODO:make function for expIndices, both AU and expressions

    # def __init__(self, dataParams):
    #	super(CASME2DataGenerator, self).__init__(dataParams)

    def getAUs(self, AUstring):
        AUstring = AUstring.replace('?', '')
        AUstring = AUstring.replace('+', ' ')
        AUstring = AUstring.replace('L', '')
        AUstring = AUstring.replace('R', '')
        AUStringList = AUstring.split(' ')
        # AUStringList = [item for sublist in AUStringList for item in sublist]

        AUMod = []
        for x in AUStringList:
            if x != '' and int(x) < 10:
                x = "AU0" + x
                AUMod.append(x)
                return AUMod
            elif x != '' and int(x) >= 10:
                x = "AU" + x
                AUMod.append(x)
                return AUMod

    def getExpIndices(self, csvData, row, expType, expList=None):

        if expType == DATA_AU_OCC:
            AUList = self.getAUs(csvData['Action Units'].values[row])
            if AUList == None:
                AUList = []
            # Make new output list
            # Loop through AUList
            # Does this AU exist in desiredClasses?  (use desiredClasses.index(AU))
            # If -1, not there
            # Otherwise, append index to output list
            # Return output list
            AUOutputList = []
            for AU in AUList:
                if AU in self.params.desiredClasses:
                    index = self.params.desiredClasses.index(AU)
                    AUOutputList.append(index)
            return AUOutputList
        elif expType == DATA_MICRO_EXP:
            exp = csvData['Estimated Emotion'].values[row]
            return [expList.index(exp)]  ###

    def getImageFilenames(self, lostFilelist):
        # list of valid/desired tasks
        validTasks = []
        imageNumbers = []
        # set a starting point for each image path
        imagePath = self.params.baseDir
        startGroundFolder = join(self.params.baseGroundPath, self.params.desiredData)
        # look for subjects,make path, and verify
        # load CSV
        CASME_CSV = []
        CASME_CSV = pd.read_csv(self.params.baseGroundPath + '/CASME2-coding.csv')
        CASME_CSV.head()
        # Create list of all AUs
        AUList = CASME_CSV['Action Units'].values

        # strip "+" from AUs
        AUList = [s.replace('?', '') for s in AUList]
        AUList = [s.replace('+', ' ') for s in AUList]
        AUList = [s.replace('L', '') for s in AUList]
        AUList = [s.replace('R', '') for s in AUList]
        AUList = [s.split(' ') for s in AUList]
        AUList = [item for sublist in AUList for item in sublist]

        # find unique AUs
        uniqueAUs = []
        for x in AUList:
            if x != '' and int(x) < 10:
                x = "AU0" + x
            elif x != '' and int(x) >= 10:
                x = "AU" + x
            if x not in uniqueAUs:
                uniqueAUs.append(x)
        uniqueAUs.sort()

        if self.params.desiredData == DATA_MICRO_EXP:
            # expCnt = 8
            expCnt = len(self.params.desiredClasses)

        elif self.params.desiredData == DATA_AU_OCC:
            if len(self.params.desiredClasses) == 0:
                expCnt = len(uniqueAUs)
            else:
                expCnt = len(self.params.desiredClasses)
        # determine chosen classes, chosenClasses

        sequenceFilename = []
        sequenceLabels = []
        sequenceSubjects = []
        sequenceTasks = []
        taskList = []

        Labels = []

        fileSubjects = CASME_CSV['Subject'].values
        fileTasks = CASME_CSV['Filename'].values

        for sequenceIndex in range(len(fileTasks)):
            task = fileTasks[sequenceIndex]
            subjectIndex = fileSubjects[sequenceIndex]
            subject = str(subjectIndex)
            if len(subject) == 1:
                subject = "0" + subject
            subject = "sub" + subject
            taskPath = (self.params.baseDir + "/" + subject + "/" + task)
            if (subject in self.params.validSubjects):
                if (task in self.params.desiredTasks) or (len(self.params.desiredTasks) == 0):

                    if self.params.desiredData == DATA_MICRO_EXP:
                        exp = CASME_CSV['Estimated Emotion'].values[sequenceIndex]
                        if exp not in self.params.desiredClasses:
                            continue

                    offsetFrame = CASME_CSV['OffsetFrame'].values[sequenceIndex]
                    onsetFrame = CASME_CSV['OnsetFrame'].values[sequenceIndex]
                    frameCnt = frameCnts_C2[subject + "/" + task]
                    Labels = np.zeros((frameCnt, expCnt))
                    expToSet = self.getExpIndices(CASME_CSV, sequenceIndex, self.params.desiredData,
                                                  self.params.desiredClasses)
                    for f in range(int(onsetFrame) - 1, int(offsetFrame) - 1):
                        for e in expToSet:
                            Labels[f][e] = 1

                    if not self.params.expRegionOnly:
                        if self.params.desiredData == DATA_MICRO_EXP:
                            neutralIndex = self.params.desiredClasses.index("neutral")
                            # Otherwise, use instead of zero below.
                            for f in range(0, int(onsetFrame) - 1):
                                Labels[f][neutralIndex] = 1
                            for f in range(int(offsetFrame) - 1, frameCnt):
                                Labels[f][neutralIndex] = 1

                    Filename = []
                    if self.params.expRegionOnly:
                        Labels = np.array(Labels[(onsetFrame - 1):(offsetFrame - 1), :])
                        for i in range(int(onsetFrame) - 1, int(offsetFrame) - 1):
                            Filename.append(subject + "/" + task + "/" + "reg_img" + str(i + 1))
                    else:
                        for i in range(frameCnt):
                            Filename.append(subject + "/" + task + "/" + "reg_img" + str(i + 1))

                    for i in range(0, len(Filename), self.params.stride):
                        # if (i + self.params.frameCnt - 1) < len(Filename):
                        rowkeeper = []
                        rowlabel = []
                        goodSub = True
                        for j in range(0, self.params.frameCnt):
                            # Check the file in the lost file list
                            if (int(i) + int(j)) < len(Filename):
                                if Filename[int(i) + int(j)] in lostFilelist:
                                    goodSub = False
                                    break
                                # padding subsequence
                                # If we're here, add the frame for the sequence
                                fileToAppend = Filename[int(i) + int(j)]
                                # fileToAppend = fileToAppend[:fileToAppend.find('.')]
                                rowkeeper.append(fileToAppend)
                                rowlabel.append(Labels[int(i) + int(j)])
                            else:
                                rowkeeper.append(Filename[-1])
                                rowlabel.append(Labels[-1])

                        # If this is a good sequence, cut off labels on ends if using time filter
                        if goodSub:
                            if self.params.timeFilter == 1:
                                timeRowlabel = rowlabel
                            else:
                                delist = int(self.params.timeFilter / 2)
                                timeRowlabel = rowlabel[delist:-delist]

                            sequenceLabels.append(timeRowlabel)
                            sequenceFilename.append(rowkeeper)
                            sequenceSubjects.append(subject)
                            sequenceTasks.append(task)

        # print(sequenceLabels, sequenceFilename, sequenceSubjects, sequenceTasks)

        return sequenceFilename, sequenceLabels, sequenceSubjects, sequenceTasks

    def getSample(self, sampleIndex, frameIndex):
        if self.params.imageDataType == INPUT_DATATYPE_IMAGE:
            image = getImage(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]) + ".jpg")
            # print(image.shape)

            return image
        # TODO: PUT THIS BACK!!!!!
        # return getImage(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]) + "_depth.jpg")
        elif self.params.imageDataType == INPUT_DATATYPE_OBJ:
            data = np.array(
                getPointCloud(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]) + ".obj",
                              self.params.tbndBaseDir + '/' + str(
                                  self.allImageFiles[sampleIndex][frameIndex]) + "_bnd.bnd", self.params))
            return data
        elif "H5" in self.params.imageDataType:
            return np.array(getH5Data(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]),
                                      self.params.H5DatasetName, 0))

    def getLabel(self, sampleIndex, frameIndex):
        return self.allLabels[sampleIndex][frameIndex]

    '''
    def getZeroPadNumber(self, subjectPath, task):
        if not "H5" in self.params.imageDataType:
            taskPath = join(subjectPath, task)				
            # Get iterator for files
            imageFiles = glob.iglob(taskPath + "/" + "*" + self.params.IMAGE_FILE_EXT)
            print(taskPath + "/" + "*" + self.params.IMAGE_FILE_EXT)
            # Grab first filename
            firstItem = next(imageFiles)
            firstItem = os.path.basename(firstItem)
            itemLen = len(firstItem) - 4
            maxItemCnt = pow(10, itemLen) - 1
            digitCnt = getDigitCnt(maxItemCnt)

        else:
            digitCnt = 0
        return digitCnt
        '''

    def getZeroPadNumber(self, currentSubjectPath, task):
        if not "H5" in self.params.imageDataType:

            currentTaskPath = join(currentSubjectPath, task)
            # Get iterator for files
            imageFiles = glob.iglob(currentTaskPath + "/" + "*" + self.params.IMAGE_FILE_EXT)
            # Grab first filename
            firstItem = next(imageFiles)
            firstItem = os.path.basename(firstItem)
            itemLen = len(firstItem) - 4
            maxItemCnt = pow(10, itemLen) - 1
            digitCnt = getDigitCnt(maxItemCnt)

        else:
            digitCnt = 0
        return digitCnt


if __name__ == "__main__": CASMEmain()










