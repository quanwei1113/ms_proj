# MIT LICENSE
#
# Copyright 2018 Michael J. Reale, Micah A. Church
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from os.path import join, isfile
from os import listdir
import csv
import numpy as np
import pickle
from General.BaseParams import *

DATA_AU_INT = "AU_INT"
DATA_AU_OCC = "AU_OCC"
DATA_AU_SM_OCC = "AU_SM_OCC"  # Two output values per class
DATA_PAIN = "PAIN"
DATA_MICRO_EXP = "MICRO_EXP"
DATA_PROTO_EXP = "PROTO_EXP"

DRIVE_DATA_FULL = "DRIVE_FULL"
DRIVE_DATA_PATCH = "DRIVE_PATCH"
DRIVE_DATA_PATCH_FULL = "DRIVE_PATCH_FULL"
RITE_DATA_FULL = "RITE_FULL"
RITE_DATA_PATCH = "RITE_PATCH"
RITE_DATA_PATCH_FULL = "RITE_PATCH_FULL"

INPUT_DATATYPE_IMAGE = "IMAGE"
INPUT_DATATYPE_OBJ = "OBJ"
INPUT_DATATYPE_WRL = "WRL"
INPUT_DATATYPE_H5 = "H5"

# Max AU intensity (barring AU43)
MAX_AU_INTENSITY = 5.0


class DataLoadParams(BaseParams):

    def __init__(self):
        # Desired subjects
        self.validSubjects = ["F001", "F002", "F003", "F004", "F005", "F006",
                              "F007", "F008", "F009", "F010", "F011", "F012", "F013",
                              "F014", "F015", "F016", "F017", "F018", "F019", "F020",
                              "F021", "F022", "F023"
                              ]

        self.desiredData = DATA_AU_OCC

        # Class list
        # (If using PAIN, this is overridden
        self.desiredClasses = ["AU06", "AU04", "AU10"]

        # Desired tasks
        # If empty, use all tasks
        self.desiredTasks = []

        # Base ground truth path
        # augment to the proper directory for the dataset
        self.baseGroundPath = "/media/Data1/BP4D/AUCoding"

        # Data directory
        # augment to the proper base directory for the dataset
        self.baseDir = "/media/Data2/BP4D/75-wide/COLOR"

        # List of files that SHOULD NOT be loaded (see lostFiles.txt)
        self.lostFileList = []

        # Data type of image to load
        self.imageDataType = INPUT_DATATYPE_IMAGE

        # How many frames in one sequence
        self.frameCnt = 1

        # Stride for cutting out sequences
        self.stride = 4

        # Time filter size
        self.timeFilter = 1

        # Do we want data balancing?
        self.doDataBalancing = False

        # If data balancing, what percentage of the "zero" samples do we want to remove?
        self.balanceRemoveRatio = 1.0

        # Do we want data sampling?  (0 or less = False, otherwise, True, and this represents the sample count)
        self.samplingCount = 0

        # Do we want to normalize the labels?
        self.normalizeLabels = False

        # Shuffle indices for batches?
        self.doShuffleIndices = False

        # Do data augmentation at all?
        self.doDataAugmentation = False

        # How much of image to keep when cropping.
        # Performed AFTER data augmentation, and always centered on image.
        # Performed whether data augmentation is on or not.
        # 1.0 = no cropping
        self.cropPercentage = 1.0

        # Do we want to load all of the data ahead of time?
        self.preloadAllData = True

        # Do we want to save/use cached data?
        self.useCachedData = True

        # Do we treat the data as unordered list of points?
        self.treatAsPointList = False

        # If we're doing "structured prediction", how large is our subimage?
        self.subimageHeight = 1
        self.subimageWidth = 1

        # DPCC-related parameters #################################

        # Is the data layout for the DPCC network?
        # (Assumes using one of the 3D variants)
        #       If True: (features (t,x,y,z)) + (t,x,y,z point coordinates) + 2*(K nearest neighbor indices)*(2*timeRadius+1)
        self.useDPCCFormat = False

        # DPCC Nearest Neighbor count
        self.DPCC_NNCount = 50

        # DPCC Nearest Neighbor radius (spatial)
        self.DPCC_SpaceRad = 5

        # DPCC Time Radius (default: 0)
        self.DPCC_TimeRad = 0





