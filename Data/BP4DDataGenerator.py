# MIT LICENSE
#
# Copyright 2018 Micah A. Church, Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

import numpy as np
import glob
import os
from keras.models import Sequential
from keras.layers import InputLayer, Reshape, Dense, Dropout, Flatten, Lambda
from keras.layers import Conv2D, MaxPooling2D
from Data.BaseDataGenerator import *
from Data.DataLoadParams import *
from Data.CommonFunctions import *
from Data.Cached3DSequenceData import *
import pandas as pd

allSubjects_BP = ['F001',
                  'F002',
                  'F003',
                  'F004',
                  'F005',
                  'F006',
                  'F007',
                  'F008',
                  'F009',
                  'F010',
                  'F011',
                  'F012',
                  'F013',
                  'F014',
                  'F015',
                  'F016',
                  'F017',
                  'F018',
                  'F019',
                  'F020',
                  'F021',
                  'F022',
                  'F023',
                  'M001',
                  'M002',
                  'M003',
                  'M004',
                  'M005',
                  'M006',
                  'M007',
                  'M008',
                  'M009',
                  'M010',
                  'M011',
                  'M012',
                  'M013',
                  'M014',
                  'M015',
                  'M016',
                  'M017',
                  'M018']

trainSubjects_BP = ['F001', 'F003', 'F005', 'F007', 'F009', 'F011', 'F013', 'F015', 'F019', 'F021', 'F023',
                    'M001', 'M003', 'M005', 'M007', 'M009', 'M011', 'M013', 'M015', 'M017']

testSubjects_BP = ['F002', 'F004', 'F006', 'F008', 'F010', 'F012', 'F014', 'F016', 'F018', 'F020', 'F022',
                   'M002', 'M004', 'M006', 'M008', 'M010', 'M012', 'M014', 'M016', 'M018']

allTasks_BP = ["T1",
               "T2",
               "T3",
               "T4",
               "T5",
               "T6",
               "T7",
               "T8"]

allOccAUs_BP = ["AU01",
                "AU02",
                "AU04",
                "AU05",
                "AU06",
                "AU07",
                "AU09",
                "AU10",
                "AU11",
                "AU12",
                "AU13",
                "AU14",
                "AU15",
                "AU16",
                "AU17",
                "AU18",
                "AU19",
                "AU20",
                "AU22",
                "AU23",
                "AU24",
                "AU27",
                "AU28"
                ]

DRMLOccAUs_BP = ["AU01",
                 "AU02",
                 "AU04",
                 "AU06",
                 "AU07",
                 "AU10",
                 "AU12",
                 "AU14",
                 "AU15",
                 "AU17",
                 "AU23",
                 "AU24"
                 ]

allIntAUs_BP = [
    "AU04",
    "AU06",
    "AU10",
    "AU12",
    "AU14",
    "AU17"
]

'''
INPUT_DATATYPE_IMAGE = "IMAGE"
INPUT_DATATYPE_OBJ = "OBJ"
INPUT_DATATYPE_H5_2D = "H5_2D"
INPUT_DATATYPE_H5_DEPTH = "H5_DEPTH"
INPUT_DATATYPE_H5_NORMAL = "H5_NORMAL"
'''


class BP4DDataGenerator(BaseDataGenerator):

    def __init__(self, faceParams):
        cloudCnt = 1
        if not faceParams.useFullModel:
            cloudCnt = len(faceParams.desiredFeaturePoints)
        self.localSequenceCache = Cached3DSequenceData(sampleIndex=0, cloudCnt=cloudCnt, frameCnt=faceParams.frameCnt)
        super(BP4DDataGenerator, self).__init__(faceParams)

    def getImageFilenames(self, lostFilelist):
        # NOTE: We can only do DATA_AU_SM_OCC with ONE AU
        # if len(self.params.desiredClasses) > 1 and self.params.desiredData == DATA_AU_SM_OCC:
        #	raise ValueError("DATA_AU_SM_OCC can only support ONE AU at this time.")

        # Max pain intensity (AU4 + AU6 + AU10)
        self.MAX_PAIN_INTENSITY = 15.0

        # Get the start folder for files
        if self.params.desiredData == DATA_AU_SM_OCC:
            startGroundFolder = join(self.params.baseGroundPath, DATA_AU_OCC)
        else:
            startGroundFolder = join(self.params.baseGroundPath, self.params.desiredData)

        # Grab first image directory path
        imagePath = self.params.baseDir
        sequenceLabels = []
        sequenceSubjects = []
        sequenceTask = []
        sequenceFilename = []

        print("Loading filenames and ground truth...")
        # For each valid subject
        for subject in self.params.validSubjects:
            # Get path to image subject folder
            currentSubjectPath = join(imagePath, subject)
            print(currentSubjectPath)

            # OLD TASK ITERATION CODE
            '''
            # Get all tasks in subject directory
            taskDirs = listdir(currentSubjectPath)
            # subject + task
            for task in taskDirs:
            '''

            # NEW TASK ITERATION CODE
            for task in allTasks_BP:
                # If this task is requested OR we didn't make any special requests for tasks
                # print (task)
                if (task in self.params.desiredTasks) or (len(self.params.desiredTasks) == 0):

                    digitCnt = self.getZeroPadNumber(currentSubjectPath, task)

                    if (self.params.desiredData == DATA_AU_INT
                            or self.params.desiredData == DATA_AU_OCC
                            or self.params.desiredData == DATA_AU_SM_OCC):
                        chosenClasses = self.params.desiredClasses
                    elif self.params.desiredData == DATA_PAIN:
                        chosenClasses = ["pain"]
                    else:
                        raise ValueError("Invalid desired data!")

                    classIndex = 0
                    # STEP ONE
                    # Initialize the filename and labels
                    Filename = []
                    Labels = []
                    GoodFrame = []

                    # Figure out correct filename
                    currentClass = chosenClasses[0]
                    if self.params.desiredData == DATA_AU_INT or self.params.desiredData == DATA_PAIN:
                        labelfilename = subject + "_" + task + "_" + currentClass + ".csv"
                    elif self.params.desiredData == DATA_AU_OCC or self.params.desiredData == DATA_AU_SM_OCC:
                        labelfilename = subject + "_" + task + ".csv"

                    # Figure out correct folder (INT vs OCC/PAIN)
                    if self.params.desiredData == DATA_AU_INT:
                        temgroundfolder = join(startGroundFolder, currentClass)
                    else:
                        temgroundfolder = startGroundFolder

                    # Open file to:
                    # - Create label placeholders (all zeros initially)
                    # - Add "image" filenames you need
                    # NOTE:
                    # OCC 		--> first row has label
                    # INT/PAIN 	--> first row is real data
                    with open(join(temgroundfolder, labelfilename), 'rt') as csvfile:
                        csvReader = csv.reader(csvfile, delimiter=",")
                        firstRow = True
                        for row in csvReader:
                            if self.params.desiredData == DATA_AU_OCC or self.params.desiredData == DATA_AU_SM_OCC:
                                if firstRow:
                                    firstRow = False
                                    continue

                            # Create placeholder for labels
                            perFrameLabels = [0] * len(chosenClasses)
                            Labels.append(perFrameLabels)

                            # Get frame
                            index = row[0]
                            index = padZero(index, digitCnt)

                            # Get filename for image
                            fileName = index  # + self.params.IMAGE_FILE_EXT

                            # Get file path
                            if not "H5" in self.params.imageDataType:
                                filePath = join(subject, join(task, fileName))
                            else:
                                filePath = "_" + subject + "_" + task + ".h5"

                                filePath = self.params.H5_3D_Data + "_" + subject + "_" + task

                            # Append to list of filenames
                            Filename.append(filePath)

                            # Mark as good frame (for now)
                            GoodFrame.append(True)

                            # NOT first row anymore
                            firstRow = False

                    # STEP TWO
                    # For each possible class, get appropriate labels

                    AUIndex = 0
                    for currentClass in chosenClasses:

                        # Get filename and appropriate column
                        if self.params.desiredData == DATA_AU_INT:
                            filename = currentClass + "/" + subject + "_" + task + "_" + currentClass + ".csv"
                            MAX_VALUE = MAX_AU_INTENSITY
                            auColIndex = 1
                        elif self.params.desiredData == DATA_AU_OCC or self.params.desiredData == DATA_AU_SM_OCC:
                            filename = subject + "_" + task + ".csv"
                            MAX_VALUE = 1.0
                            auColIndex = int(currentClass.split("AU")[1])
                        elif self.params.desiredData == DATA_PAIN:
                            filename = subject + "_" + task + "_" + currentClass + ".csv"
                            MAX_VALUE = self.params.MAX_PAIN_INTENSITY
                            auColIndex = 1

                        # Open the actual file
                        with open(join(startGroundFolder, filename), 'rt') as csvfile:
                            csvReader = csv.reader(csvfile, delimiter=",")
                            rowIndex = 0
                            firstRow = True

                            for row in csvReader:
                                # Skip first row if OCC
                                if self.params.desiredData == DATA_AU_OCC or self.params.desiredData == DATA_AU_SM_OCC:
                                    if firstRow:
                                        firstRow = False
                                        continue

                                # Get the actual label
                                label = row[auColIndex]

                                # Convert the label to float type
                                label = float(label)

                                # Store label
                                Labels[rowIndex][AUIndex] = label

                                # Increment row index
                                rowIndex += 1

                                # Not first row anymore
                                firstRow = False

                        # Go to next AU
                        AUIndex += 1

                    # STEP THREE
                    # Make sure the label is valid
                    for frameIndex in range(len(Labels)):
                        # print("\t", frameIndex)
                        for classIndex in range(len(Labels[frameIndex])):
                            # Get int label
                            currentLabel = int(Labels[frameIndex][classIndex])

                            # Is this INT or OCC?
                            if (self.params.desiredData == DATA_AU_INT
                                    or self.params.desiredData == DATA_AU_OCC
                                    or self.params.desiredData == DATA_AU_SM_OCC):
                                # Checking if 9; if it is, reject whole frame
                                if currentLabel > 7:
                                    GoodFrame[frameIndex] = False
                                else:
                                    # Otherwise, normalize labels if requested
                                    if self.params.normalizeLabels:
                                        Labels[frameIndex][classIndex] /= MAX_VALUE
                            elif self.params.desiredData == DATA_PAIN:
                                # Checking if negative
                                if currentLabel < 0:
                                    GoodFrame[frameIndex] = False
                                else:
                                    # Otherwise, normalize
                                    if self.params.normalizeLabels:
                                        Labels[frameIndex][classIndex] /= MAX_VALUE

                    # STEP FOUR
                    # print("Lost files:", end=" ")
                    for i in range(0, len(Filename), self.params.stride):
                        if (i + self.params.frameCnt - 1) < len(Filename):
                            rowkeeper = []
                            rowlabel = []
                            goodSub = True
                            for j in range(0, self.params.frameCnt):
                                # Check the file in the lost file list
                                if Filename[int(i) + int(j)] in lostFilelist:
                                    # print(Filename[int(i) + int(j)], end=",")
                                    goodSub = False
                                    break
                                # Check if not a good frame
                                if not GoodFrame[int(i) + int(j)]:
                                    goodSub = False
                                    break

                                # If we're here, add the frame for the sequence
                                fileToAppend = Filename[int(i) + int(j)]
                                # fileToAppend = fileToAppend[:fileToAppend.find('.')]
                                rowkeeper.append(fileToAppend)
                                rowlabel.append(Labels[int(i) + int(j)])

                            # If this is a good sequence, cut off labels on ends if using time filter
                            if goodSub:
                                if self.params.timeFilter == 1:
                                    timeRowlabel = rowlabel
                                else:
                                    delist = int(self.params.timeFilter / 2)
                                    timeRowlabel = rowlabel[delist:-delist]

                                # Finally add whole sequence
                                sequenceLabels.append(timeRowlabel)
                                sequenceFilename.append(rowkeeper)
                                sequenceSubjects.append(subject)
                                sequenceTask.append(task)
                # print("")
            '''
            for i in range(len(sequenceLabels)):
                for j in range(len(sequenceLabels[i])):
                    print ("\t\t\t",len(sequenceLabels[i][j]))
                print ("\t\t",len(sequenceLabels[i]))
            print ("\t","sequenceLabels", len(sequenceLabels))
            '''

        # If DATA_AU_SM_OCC, make categorical for each AU
        if self.params.desiredData == DATA_AU_SM_OCC:
            outputCnt = np.array(sequenceLabels).shape[-1]
            for i in range(len(sequenceLabels)):
                for j in range(len(sequenceLabels[i])):
                    # Get current label row
                    labelRow = sequenceLabels[i][j]
                    # Create new label row
                    newLabelRow = []
                    for k in range(outputCnt):
                        if labelRow[k] > 0:
                            newLabelRow += [0, 1]
                        else:
                            newLabelRow += [1, 0]
                        # Store new label row
                    sequenceLabels[i][j] = newLabelRow

            # Also, change desired classes
            newDesiredClasses = []
            for i in range(len(self.params.desiredClasses)):
                newDesiredClasses += ["NO_" + self.params.desiredClasses[i], self.params.desiredClasses[i]]
            self.params.desiredClasses = newDesiredClasses
            print("New desired classes:", self.params.desiredClasses)

        print("Sequence label sizes:", np.asarray(sequenceLabels).shape)
        print("All filenames and ground truth loaded")
        print(len(sequenceFilename), "sample loaded.")

        # print("ALL LABELS:")
        # print(sequenceLabels)

        return sequenceFilename, sequenceLabels, sequenceSubjects, sequenceTask

    def clearLocalLoadingCache(self):
        self.localSequenceCache.reset(0)

    def getSample(self, sampleIndex, frameIndex):
        if self.params.imageDataType == INPUT_DATATYPE_IMAGE:
            return getImage(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]) + ".png")
        elif self.params.imageDataType == INPUT_DATATYPE_OBJ:
            cloud = np.array(
                getPointCloud(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]) + ".obj",
                              self.params.tbndBaseDir + '/' + str(
                                  self.allImageFiles[sampleIndex][frameIndex]) + ".tbnd",
                              self.params))

            if self.params.useDPCCFormat:
                # Need to convert to DPCC format
                cloud = convertToDPCC(cloud, self.params, sampleIndex, frameIndex,
                                      self.localSequenceCache,
                                      self.allImageFiles, ".obj", ".tbnd")

            return cloud
        elif "H5" in self.params.imageDataType:
            return np.array(getH5Data(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]),
                                      self.params.H5DatasetName, frameIndex, frameIndex + 1, self.params.maxPointCount))

    def getZeroPadNumber(self, currentSubjectPath, task):
        if not "H5" in self.params.imageDataType:

            currentTaskPath = join(currentSubjectPath, task)

            # OLD ZERO-PADDING CODE
            '''
            # Get all image files
            imageFiles = [f for f in listdir(currentTaskPath) if isfile(join(currentTaskPath, f))]
            # Get count of files
            imageFileCnt = len(imageFiles)					
            # Get digit count for zero padding
            digitCnt = getDigitCnt(imageFileCnt)
            '''

            # NEW ZERO-PADDING CODE
            # Get iterator for files
            imageFiles = glob.iglob(currentTaskPath + "/" + "*" + self.params.IMAGE_FILE_EXT)
            # Grab first filename
            firstItem = next(imageFiles)
            firstItem = os.path.basename(firstItem)
            # print("FIRST ITEM:", firstItem)
            # Remove the extension (assumed to be 4 characters)
            itemLen = len(firstItem) - 4
            # print("ITEM LEN:", itemLen)
            # Get the maximum number of filenames
            maxItemCnt = pow(10, itemLen) - 1
            # print("MAX ITEM CNT:", maxItemCnt)
            # Get digit count for zero padding
            digitCnt = getDigitCnt(maxItemCnt)
        # print("DIGIT CNT:", digitCnt)

        else:
            digitCnt = 0
        return digitCnt

    def getSequence(self, frameStart, frameEnd, sampleIndex):
        pass

    def getLabel(self, sampleIndex, frameIndex):
        return self.allLabels[sampleIndex][frameIndex]
