import numpy as np


class ObjLoader(object):

    def __init__(self, fileName):
        self.vertices = np.array([])
        self.tex_cords = np.array([])
        self.vert_norms = np.array([])

        self.vertex_index = []
        self.texture_index = []
        self.normal_index = []

        vertices = []
        tex_cords = []
        vert_norms = []

        try:
            f = open(fileName)
            for line in f:
                line = line.split()
                if len(line) <= 0:
                    pass
                elif line[0] == "mtllib":
                    mtl = line[1]
                elif line[0] == 'v':
                    vertices.append(line[1:])
                elif line[0] == "vt":
                    tex_cords.append(line[1:])
                elif line[0] == "vn":
                    vert_norms.append(line[1:])
                elif line[0] == 'f':
                    vHold = []
                    tHold = []
                    nHold = []
                    for face in line[1:]:
                        face = face.split('/')
                        vHold.append(int(face[0])-1)
                        tHold.append(int(face[1])-1)
                        nHold.append(int(face[2])-1)
                    self.vertex_index.append(vHold)
                    self.texture_index.append(tHold)
                    self.normal_index.append(nHold)

            self.vertices = np.array(vertices, np.float32)
            self.tex_cords = np.array(tex_cords, np.float32)
            self.vert_norms = np.array(vert_norms, np.float32)

            self.vertex_index = np.array(self.vertex_index, np.uint32)
            self.texture_index = np.array(self.vertex_index, np.uint32)
            self.normal_index = np.array(self.vertex_index, np.uint32)

        except IOError as error:
            print(".obj file not found.")
            raise IOError(error)

    def __getMtlData__(self, mtlName):

        f = open(mtlName)
        for line in f:
            line = line.split()
            if len(line) <= 0:
                pass
            elif line[0] == "map_Kd":
                jpg = line[1]
