# MIT LICENSE
#
# Copyright 2018 Michael J. Reale, Xiaomai Liu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function
import numpy as np
import numpy.random
import copy
from random import sample
import datetime


###########################################################
# CONSTANTS
###########################################################


###########################################################
# FUNCTIONS
###########################################################

# Updates how many samples we have
def updateCounts(indicesToAdd, labels, frameCnt, uniqueLabelValues, sampleCnts, zeroSampleCnts):
    subLabels = labels[indicesToAdd]
    # print("SUBLABELS SIZE:", subLabels.shape)
    # print("SUBLABELS COUNT:", len(indicesToAdd))

    labelCnt = subLabels.shape[-1]

    subLabelCnts = countCurrentSamples(subLabels, labelCnt, uniqueLabelValues)

    sampleCnts += subLabelCnts

    return sampleCnts

    '''
    # OLD WAY
    for realIndex in indicesToAdd:
        for subIndex, x in np.ndenumerate(labels[realIndex]):

            # Does this match what we are looking for?
            closestValueIndex = 0
            closestValueDiff = -1
            for valueIndex in range(len(uniqueLabelValues)):
                diff = abs(labels[realIndex][subIndex] - uniqueLabelValues[valueIndex])
                if diff < closestValueDiff or closestValueDiff == -1:
                    closestValueDiff = diff
                    closestValueIndex = valueIndex

            labelIndex = subIndex[-1]
            sampleCnts[labelIndex][closestValueIndex] += 1
    '''


# Grab a sample for each subject
def findSampleForEachSubject(searchPair, samplesLeft, labels, frameCnt, uniqueSubjects, uniqueLabelValues,
                             sampleCntsPerSampleLeft):
    indicesToAdd = []

    chosenLabelIndex = searchPair[0]
    chosenValueIndex = searchPair[1]

    # For each subject
    for subjectIndex in range(len(uniqueSubjects)):
        # Try to find a sample that matches what we're looking for
        foundOneForSubject = False

        for sampleIndex in range(len(samplesLeft[subjectIndex])):
            # Get the REAL index
            realIndex = samplesLeft[subjectIndex][sampleIndex]
            # print("Shape of sampleCntsPerSampleLeft", np.array(sampleCntsPerSampleLeft).shape)
            # print("realIndex", realIndex, "chosenLableIndex", chosenLabelIndex, "chosenValueIndex", chosenValueIndex)
            if sampleCntsPerSampleLeft[realIndex][chosenLabelIndex][chosenValueIndex] > 0:
                # Add this sample
                indicesToAdd.append(realIndex)
                # Found one for subject
                foundOneForSubject = True
                # Break out
                break

            '''
            # OLD CODE
            for subIndex, x in np.ndenumerate(labels[realIndex]):
                # Remove last dimension
                subIndex = subIndex[:-1]

                # Does this match what we are looking for?
                closestValueIndex = 0
                closestValueDiff = -1
                for valueIndex in range(len(uniqueLabelValues)):
                    diff = abs(labels[realIndex][subIndex][chosenLabelIndex] - uniqueLabelValues[valueIndex])
                    if diff < closestValueDiff or closestValueDiff == -1:
                        closestValueDiff = diff
                        closestValueIndex = valueIndex

                # Did we find the same closest match?
                if closestValueIndex == chosenValueIndex:
                    # Add this sample
                    indicesToAdd.append(realIndex)
                    # Found one for subject
                    foundOneForSubject = True
                    # Break out
                    break
            '''

            '''
            # OLD CODE
            for frameIndex in range(frameCnt):
                # Does this match what we are looking for?
                closestValueIndex = 0
                closestValueDiff = -1
                for valueIndex in range(len(uniqueLabelValues)):
                    diff = abs(labels[realIndex][frameIndex][chosenLabelIndex] - uniqueLabelValues[valueIndex])
                    if diff < closestValueDiff or closestValueDiff == -1:
                        closestValueDiff = diff
                        closestValueIndex = valueIndex

                # Did we find the same closest match?
                if closestValueIndex == chosenValueIndex:
                    # Add this sample
                    indicesToAdd.append(realIndex)
                    # Found one for subject
                    foundOneForSubject = True
                    # Break out
                    break
            '''

            # Did we find something?
            if foundOneForSubject:
                break

    return indicesToAdd


# Removes indices
def removeIndices(indicesToRemove, samplesLeft):
    for deleteIndex in sorted(indicesToRemove, reverse=True):
        for i in range(len(samplesLeft)):
            if deleteIndex in samplesLeft[i]:
                samplesLeft[i].remove(deleteIndex)
                break


# Finds the class/value pair with the lowest count
def findLowestCount(sampleCnts, maxPositiveSampleCnt, alreadyDone):
    # For each label...
    minVal = -1
    chosenPair = [-1, -1]

    for labelIndex in range(len(sampleCnts)):
        # ...and for each label value
        for valueIndex in range(len(sampleCnts[labelIndex])):
            if [labelIndex, valueIndex] not in alreadyDone and sampleCnts[labelIndex][
                valueIndex] < maxPositiveSampleCnt:
                if minVal == -1 or minVal > sampleCnts[labelIndex][valueIndex]:
                    minVal = sampleCnts[labelIndex][valueIndex]
                    chosenPair = [labelIndex, valueIndex]

    return chosenPair, minVal


# Figures out class distribution
def countCurrentSamples(labels, labelCnt, uniqueLabelValues):
    DEBUG_PRINT = False

    labels = np.array(labels)

    sampleCnts = [[0 for x in range(len(uniqueLabelValues))] for y in range(labelCnt)]

    # print(labels.shape)

    totalEverything = np.prod(labels.shape) / labelCnt

    # print("ALL ITERS:", totalEverything)

    startTime = datetime.datetime.now()

    # FLATTEN AND SUM

    labels = np.reshape(labels, (-1, labelCnt))

    positiveCounts = np.sum(labels, axis=0)

    # print("POSITIVE COUNTS:", positiveCounts)
    # print("LABEL CNT:", labelCnt)
    #
    #    #print("*********************************************")
    #    #print("*********************************************")
    #    #print("WARNING: uniqueLabelValues effectively ignored!!!!")
    #    #print("*********************************************")
    #    #print("*********************************************")
    #
    # print("SAMPLECNTS SHAPE:", np.array(sampleCnts).shape)

    for labelIndex in range(labelCnt):
        sampleCnts[labelIndex][0] = totalEverything - positiveCounts[labelIndex]
        sampleCnts[labelIndex][1] = positiveCounts[labelIndex]

    sampleCnts = np.array(sampleCnts).astype(int)

    # FLATTEN METHOD 1

    '''
    labels = np.reshape(labels, (-1, labelCnt))

    itr = 0

    for index in range(len(labels)):
        if itr%50000 == 0:
            print (itr)
        itr += 1
        #print (index)
        for labelIndex in range(labelCnt):
            # Find closest value
            closestValueIndex = 0
            closestValueDiff = -1
            for valueIndex in range(len(uniqueLabelValues)):
                if DEBUG_PRINT: print("\t\t", "VALUE", valueIndex, ":", uniqueLabelValues[valueIndex])
                #print(index)
                diff = abs(labels[index][labelIndex] - uniqueLabelValues[valueIndex])
                if DEBUG_PRINT: print("\t\t", "DIFFERENCE:", labels[index][labelIndex], uniqueLabelValues[valueIndex])
                if diff < closestValueDiff or closestValueDiff == -1:
                    closestValueDiff = diff
                    closestValueIndex = valueIndex
                    if DEBUG_PRINT: print("\t\t\t", "New closest!", closestValueIndex, uniqueLabelValues[closestValueIndex])
                #itr+=1

            if DEBUG_PRINT: print("\t", "BEST INDEX:", closestValueIndex, uniqueLabelValues[closestValueIndex])

            sampleCnts[labelIndex][closestValueIndex] += 1
    '''
    '''

    # OLD METHOD
    itr = 0
    for index, x in np.ndenumerate(labels):
        if itr%50000 == 0:
            print (itr)
        itr += 1
        #print (index)

        if DEBUG_PRINT: print("SAMPLE", index, ":", x)
        # Find closest value
        closestValueIndex = 0
        closestValueDiff = -1
        for valueIndex in range(len(uniqueLabelValues)):
            if DEBUG_PRINT: print("\t\t", "VALUE", valueIndex, ":", uniqueLabelValues[valueIndex])
            #print(index)
            diff = abs(labels[index] - uniqueLabelValues[valueIndex])
            if DEBUG_PRINT: print("\t\t", "DIFFERENCE:", labels[index], uniqueLabelValues[valueIndex])
            if diff < closestValueDiff or closestValueDiff == -1:
                closestValueDiff = diff
                closestValueIndex = valueIndex
                if DEBUG_PRINT: print("\t\t\t", "New closest!", closestValueIndex, uniqueLabelValues[closestValueIndex])
            #itr+=1

        if DEBUG_PRINT: print("\t", "BEST INDEX:", closestValueIndex, uniqueLabelValues[closestValueIndex])

        labelIndex = index[-1]
        sampleCnts[labelIndex][closestValueIndex] += 1
    '''

    endTime = datetime.datetime.now()
    # print("TIME TAKEN:", (endTime - startTime))

    return sampleCnts


def checkNeedMoreSamples(labelCnt, uniqueLabelValues, sampleCnts, maxPositiveSampleCnt):
    # Do we need more samples?
    # For each label...
    needMoreSamples = False
    for labelIndex in range(labelCnt):
        # ...and for each label value
        for valueIndex in range(len(uniqueLabelValues)):
            # Have we hit max positive sample count?
            if sampleCnts[labelIndex][valueIndex] < maxPositiveSampleCnt:
                needMoreSamples = True
                break
        if needMoreSamples:
            break

    return needMoreSamples


def sampleDatabase(samples, labels,
                   subjects, tasks,
                   uniqueValueModes,
                   maxPositiveSampleCnt):
    # Get unique subjects
    uniqueSubjects = list(set(subjects))
    uniqueSubjects.sort()
    print("UNIQUE SUBJECTS:", uniqueSubjects)

    print("UNIQUE LABEL MODES:", uniqueValueModes)

    # Get label counts
    # labelCnt = len(labels[0][0])
    labels = np.array(labels)
    labelCnt = labels.shape[-1]

    # Make list of sample counts to update
    sampleCnts = [[0 for x in range(len(uniqueValueModes))] for y in range(labelCnt)]
    zeroSampleCnts = [[0 for x in range(1)] for y in range(labelCnt)]

    # Create list of samples left
    samplesLeft = []
    for subjectIndex in range(len(uniqueSubjects)):
        samplesLeft.append([])
        for i in range(len(samples)):
            if subjects[i] == uniqueSubjects[subjectIndex]:
                samplesLeft[subjectIndex].append(i)

    # print("Samples left:", samplesLeft)

    # Also grab number of frames
    frameCnt = len(labels[0])

    # If the number of frames is greater than 1, we need to shuffle the samples per subject
    # if frameCnt > 1:
    # print("Frame cnt > 1; shuffling...")
    print("Suffling samples per subject")
    seed = 5280
    np.random.seed(seed)
    for subjectIndex in range(len(uniqueSubjects)):
        np.random.shuffle(samplesLeft[subjectIndex])
        # print("Samples left (after shuffling):", samplesLeft)

    # Create output list
    outputIndexList = []

    # Get current total counts
    totalSampleCnts = countCurrentSamples(labels, labelCnt, uniqueValueModes)  # uniqueLabelValues)
    # totalZeroCnts = countCurrentSamples(labels, labelCnt, [0.0])
    print("TOTAL SAMPLE CNTS OVERALL:", totalSampleCnts)
    # print("TOTAL ZERO CNTS OVERALL:", totalZeroCnts)
    _, minVal = findLowestCount(totalSampleCnts, maxPositiveSampleCnt, [])

    # if minVal < maxPositiveSampleCnt:
    # print("WARNING: Max positive sample count", maxPositiveSampleCnt, "too large!  Shrinking to", minVal)
    # maxPositiveSampleCnt = minVal

    # TODO: Call countCurrentSamples on each of samplesLeft
    sampleCntsPerSampleLeft = []

    for sample in range(len(labels)):
        # print(sample)
        sampleCntsPerSampleLeft.append(countCurrentSamples([labels[sample]], labelCnt, uniqueValueModes))

    alreadyDone = []
    itr = 0
    while checkNeedMoreSamples(labelCnt, uniqueValueModes, sampleCnts, maxPositiveSampleCnt):
        # Find label and value pair with current lowest counts
        searchPair, _ = findLowestCount(sampleCnts, maxPositiveSampleCnt, alreadyDone)
        if searchPair[0] < 0:
            break

        # Grab a sample from each subject
        indicesToAdd = findSampleForEachSubject(searchPair, samplesLeft, labels, frameCnt,
                                                uniqueSubjects, uniqueValueModes, sampleCntsPerSampleLeft)
        # uniqueSubjects, uniqueLabelValues)

        # Add to list of output samples
        outputIndexList += indicesToAdd

        # Did we add any?
        # print("\tAdded", len(indicesToAdd))

        if len(indicesToAdd) == 0:
            alreadyDone.append(searchPair)

        # Increment current counts
        sampleCnts = updateCounts(indicesToAdd, labels, frameCnt, uniqueValueModes, sampleCnts, zeroSampleCnts)

        # Remove these sample labels
        removeIndices(indicesToAdd, samplesLeft)
        itr += 1

    # Results
    # print("SAMPLE COUNTS:", sampleCnts)
    ##print("ZERO SAMPLE COUNTS:", zeroSampleCnts)
    # print("TOTAL NUMBER OF SAMPLES BEFORE:", len(labels))
    # print("TOTAL NUMBER OF SAMPLES AFTER:", len(outputIndexList))

    aveStd = 0
    for realIndex in outputIndexList:
        oneStd = np.std(labels[realIndex])
        # print("\t", oneStd, ":", labels[realIndex])
        aveStd += oneStd
    aveStd /= len(labels)
    print("AVE STD:", aveStd)

    # print("FINAL LABEL LIST:")
    # for realIndex in outputIndexList:
    #    for frameIndex in range(frameCnt):
    #        print("\t",realIndex, ",", frameIndex, ":", labels[realIndex][frameIndex])

    # Generate output lists
    outputSamples = []
    outputLabels = []
    outputSubjects = []
    outputTasks = []
    for realIndex in outputIndexList:
        outputSamples.append(samples[realIndex])
        outputLabels.append(labels[realIndex])
        outputSubjects.append(subjects[realIndex])
        outputTasks.append(tasks[realIndex])

    totalSampleCntsAfter = countCurrentSamples(outputLabels, labelCnt, uniqueValueModes)
    print("TOTAL SAMPLE CNTS AFTER:", totalSampleCntsAfter)

    return outputSamples, outputLabels, outputSubjects, outputTasks


def __getLabelsetIndex(labelList, uniqueValueModes):
    valueCnt = len(uniqueValueModes)
    labelCnt = len(labelList)

    index = 0
    for i in range(labelCnt):
        valueIndex = uniqueValueModes.index(labelList[i])
        index *= valueCnt
        index += valueIndex

    return index


def balanceClasses(samples, labels,
                   subjects, tasks,
                   uniqueValueModes,
                   percentageToRemove):
    # Get label counts
    labelCnt = len(labels[0][0])
    # labelCnt = np.asarray(labels[0]).shape[-1]
    print("Label cnt:", labelCnt)

    # Get number of unique values per label
    valueCnt = len(uniqueValueModes)
    print("Value cnt:", valueCnt)

    # Get class counts
    classSampleCnts = countCurrentSamples(labels, labelCnt, uniqueValueModes)
    print("Class counts before:", classSampleCnts)

    # DEBUG: How many zeros?
    zeroCnt = 0
    for sampleIndex in range(len(labels)):
        for frameIndex in range(len(labels[sampleIndex])):
            # Get labelset index
            labelSetIndex = __getLabelsetIndex(labels[sampleIndex][frameIndex], uniqueValueModes)
            if labelSetIndex == 0:
                zeroCnt += 1
    print("ZERO COUNT BEFORE:", zeroCnt)
    # END DEBUG

    # The data could either be:
    # - mutually-exclusive classes:
    #       * Remove first class (assumed to be most frequent)
    # - non-exclusive classes:
    #       * Assume all-zeros answer is most frequent
    # - regression:
    #       * Assume all-zeros answer is most frequent

    # Do we have any all-zeros?  (Impossible in first case)
    allZeroIndices = []
    allFirstOneIndices = []

    for sampleIndex in range(len(labels)):
        for frameIndex in range(len(labels[sampleIndex])):
            sumLabel = np.sum(labels[sampleIndex][frameIndex])

            # Is this all zeros?
            if sumLabel == 0:
                allZeroIndices.append(sampleIndex)  # (sampleIndex, frameIndex))

            # Is this one in the first spot (and zero everywhere else)?
            if sumLabel == 1 and labels[sampleIndex][frameIndex][0] == 1:
                allFirstOneIndices.append(sampleIndex)  # (sampleIndex, frameIndex))

    # Did we have any all-zeros?
    if len(allZeroIndices) > 0:
        # All-zeros most frequent
        indexPool = allZeroIndices
    else:
        # Just remove first class
        indexPool = allFirstOneIndices

    # Get remove count
    removeCnt = int(percentageToRemove * float(len(indexPool)))

    print("Removing", removeCnt, "samples...")

    # Randomly remove samples from list
    indicesToRemove = np.random.choice(indexPool, size=(removeCnt), replace=False)

    # Generate output lists
    outputSamples = []
    outputLabels = []
    outputSubjects = []
    outputTasks = []
    for realIndex in range(len(samples)):
        if realIndex not in indicesToRemove:
            outputSamples.append(samples[realIndex])
            outputLabels.append(labels[realIndex])
            outputSubjects.append(subjects[realIndex])
            outputTasks.append(tasks[realIndex])

    totalSampleCntsAfter = countCurrentSamples(outputLabels, labelCnt, uniqueValueModes)
    print("Total sample counts after:", totalSampleCntsAfter)

    # DEBUG: How many zeros AFTER?
    zeroCnt = 0
    for sampleIndex in range(len(outputLabels)):
        for frameIndex in range(len(outputLabels[sampleIndex])):
            # Get labelset index
            labelSetIndex = __getLabelsetIndex(outputLabels[sampleIndex][frameIndex], uniqueValueModes)
            if labelSetIndex == 0:
                zeroCnt += 1
    print("ZERO COUNT AFTER:", zeroCnt)
    # END DEBUG

    print("Balancing complete.")

    return outputSamples, outputLabels, outputSubjects, outputTasks


class LabelValuePair:
    def __init__(self, label, value, count):
        self.label = label
        self.value = value
        self.count = count

    def getCount(self):
        return self.count

    def __rep__(self):
        return self.__str__()

    def __str__(self):
        return str(self.label) + "," + str(self.value) + ":" + str(self.count)


class LabelSetScore:
    def __init__(self, labels, score):
        self.labels = labels
        self.score = score

    def getScore(self):
        return self.score

    def getLabels(self):
        return self.labels

    def __rep__(self):
        return self.__str__()

    def __str__(self):
        s = str(self.score) + ": ["
        for item in self.labels:
            s += str(item) + ","
        s += "]"
        return s


def findMostNeeded(sampleCnts, valueWeights, uniqueValueModes, maxPositiveSampleCnt):
    # - Add sample that:
    #       * Covers most of those values
    #       * Does NOT add to label-values that have hit cap

    # Get label counts
    labelCnt = len(sampleCnts)

    # Get value count
    valueCnt = len(sampleCnts[0])

    # Which (label-value) pairs need more samples?
    allScores = []
    anyLess = False

    for i in range(labelCnt):
        for j in range(valueCnt):
            diff = maxPositiveSampleCnt - sampleCnts[i][j]

            weight = valueWeights[i][j] * float(diff > 0)

            allScores.append(LabelValuePair(i, j, weight))
            # print(weight)

            if diff > 0:
                anyLess = True

    if not anyLess:
        # All have been dealt with
        return []

        # print("SCORES:")
    # print([str(item) for item in allScores])

    # Figure out best labels
    bestLabels = []

    # Find all combinations and score them
    totalComboCnt = int(pow(valueCnt, labelCnt))
    for index in range(totalComboCnt):
        # Get label and score
        labels = []
        score = 0

        subScoreIndex = 0
        leftIndex = index
        for i in range(labelCnt):
            valueIndex = leftIndex % valueCnt
            leftIndex = leftIndex // valueCnt

            # Append to label
            labels.append(uniqueValueModes[valueIndex])

            # Get score for this particular part
            score += allScores[subScoreIndex + valueIndex].getCount()

            # Go to next value
            subScoreIndex += valueCnt

        # Append to list of all labels
        bestLabels.append(LabelSetScore(labels, score))

    # Sort!
    bestLabels = sorted(bestLabels, key=LabelSetScore.getScore, reverse=True)

    # Remove any labels with negative scores
    # finalLabels = [x for x in bestLabels if x.getScore() > 0]

    # print("BEST LABELS:")
    # print([str(item) for item in bestLabels])

    # Return list of possible labels to get
    return bestLabels


def findBestMatching(labels, bestLabels, validSampleIndices):
    indicesToAdd = []

    # Can we find a label?
    found = False

    for labelSet in bestLabels:
        # Get actual labels
        searchLabel = labelSet.getLabels()

        # See if we can find this one...
        for index in validSampleIndices:
            # TODO: Need to do this more intelligently...
            for frame in range(len(labels[index])):
                if labels[index][frame] == searchLabel:
                    # print("\t\tFOUND:", searchLabel, "with score", labelSet.getScore())
                    # Add to list
                    indicesToAdd.append(index)
                    found = True
                    break

            if found:
                break

        if found:
            break

    # print("ADDING", len(indicesToAdd),"INDICES")

    return indicesToAdd


# NOTE: Assumes samples, lables, etc. only have data for the ONE video (subject/task)
def sampleOneVideo(samples, labels,
                   subjects, tasks,
                   uniqueValueModes,
                   maxPositiveSampleCnt,
                   chosenSubject, chosenTask):
    # Get label counts
    labelCnt = len(labels[0][0])
    # labelCnt = np.asarray(labels[0]).shape[-1]
    # Get value count
    valueCnt = len(uniqueValueModes)

    # Get all valid indices for this subject and task
    validSampleIndices = [i for i in range(len(subjects)) if subjects[i] == chosenSubject and tasks[i] == chosenTask]
    print("NUMBER OF SAMPLES for", chosenSubject, ",", chosenTask, ":", len(validSampleIndices))

    # Compute per-class-value weights
    labelsUsed = [labels[i] for i in range(len(labels)) if i in validSampleIndices]
    beforeSampleCnts = countCurrentSamples(labelsUsed, labelCnt, uniqueValueModes)
    print("\tSAMPLE COUNTS BEFORE:", beforeSampleCnts)
    valueWeights = 1.0 - np.array(beforeSampleCnts) / float(len(validSampleIndices))
    print("\tWEIGHTS:", valueWeights)

    # Basic algorithm:
    # - Check which label-value combo(s) need more values
    # - Add sample that:
    #       * Covers most of those values
    #       * Does NOT add to label-values that have hit cap
    # - Repeat until no more values can be added

    # Make list of sample counts to update
    sampleCnts = [[0 for x in range(len(uniqueValueModes))] for y in range(labelCnt)]
    sampleCnts = np.array(sampleCnts)
    # print(sampleCnts.shape)

    # Make list of sample indices to keep
    samplesToKeep = []
    keepGoing = True

    # - Repeat until no more values can be added
    while (keepGoing):
        # - Check which label-value combo(s) need more values
        bestLabels = findMostNeeded(sampleCnts, valueWeights, uniqueValueModes, maxPositiveSampleCnt)

        # If we somehow don't need any values, stop
        if len(bestLabels) == 0:
            keepGoing = False
            break

        # Find best matching sample
        sampleIndicesToAdd = findBestMatching(labels, bestLabels, validSampleIndices)

        # Remove from valid indices
        indicesLeft = [x for x in validSampleIndices if x not in sampleIndicesToAdd]
        validSampleIndices = indicesLeft

        # If we couldn't FIND any values, stop
        if len(sampleIndicesToAdd) == 0:
            keepGoing = False
            break

        # Append new indices
        samplesToKeep += sampleIndicesToAdd
        # print("SAMPLES TO KEEP:", samplesToKeep)

        # Update counts
        labelsUsed = [labels[i] for i in range(len(labels)) if i in samplesToKeep]
        # print("LABELS USED:", labelsUsed)
        sampleCnts = countCurrentSamples(labelsUsed, labelCnt, uniqueValueModes)
        # print("\t\tSAMPLE COUNTS SO FAR:", sampleCnts)

    # Print sample counts
    print("\tSAMPLE COUNTS AFTER:", sampleCnts)

    return samplesToKeep


def sampleDatabasePerVideo(samples, labels,
                           subjects, tasks,
                           uniqueValueModes,
                           maxPositiveSampleCnt):
    # Get unique subjects
    uniqueSubjects = list(set(subjects))
    uniqueSubjects.sort()

    # Get unique tasks
    uniqueTasks = list(set(tasks))
    uniqueTasks.sort()

    # For each subject and task
    samplesToKeep = []
    for subject in uniqueSubjects:
        for task in uniqueTasks:
            videoSamplesToKeep = sampleOneVideo(samples, labels,
                                                subjects, tasks,
                                                uniqueValueModes,
                                                maxPositiveSampleCnt,
                                                subject, task)
            samplesToKeep.append(videoSamplesToKeep)

    raise ValueError("STOP")

    # END DEBUG

    # Get label counts
    labelCnt = len(labels[0][0])
    # labelCnt = np.asarray(labels[0]).shape[-1]
    # Get value count
    valueCnt = len(uniqueValueModes)

    # Make list of sample counts to update
    sampleCnts = [[0 for x in range(len(uniqueValueModes))] for y in range(labelCnt)]
    zeroSampleCnts = [[0 for x in range(1)] for y in range(labelCnt)]

    # Create list of samples left
    samplesLeft = []
    for subjectIndex in range(len(uniqueSubjects)):
        samplesLeft.append([])
        for i in range(len(samples)):
            if subjects[i] == uniqueSubjects[subjectIndex]:
                samplesLeft[subjectIndex].append(i)

    # print("Samples left:", samplesLeft)

    # Also grab number of frames
    frameCnt = len(labels[0])

    # If the number of frames is greater than 1, we need to shuffle the samples per subject
    if frameCnt > 1:
        print("Frame cnt > 1; shuffling...")
        seed = 5280
        np.random.seed(seed)
        for subjectIndex in range(len(uniqueSubjects)):
            np.random.shuffle(samplesLeft[subjectIndex])
        # print("Samples left (after shuffling):", samplesLeft)

    # Create output list
    outputIndexList = []

    # Get current total counts
    totalSampleCnts = countCurrentSamples(labels, labelCnt, uniqueValueModes)  # uniqueLabelValues)
    totalZeroCnts = countCurrentSamples(labels, labelCnt, [0.0])
    print("TOTAL SAMPLE CNTS OVERALL:", totalSampleCnts)
    # print("TOTAL ZERO CNTS OVERALL:", totalZeroCnts)
    _, minVal = findLowestCount(totalSampleCnts, maxPositiveSampleCnt, [])

    # if minVal < maxPositiveSampleCnt:
    # print("WARNING: Max positive sample count", maxPositiveSampleCnt, "too large!  Shrinking to", minVal)
    # maxPositiveSampleCnt = minVal

    alreadyDone = []

    # while checkNeedMoreSamples(labelCnt, uniqueLabelValues, sampleCnts, maxPositiveSampleCnt):
    while checkNeedMoreSamples(labelCnt, uniqueValueModes, sampleCnts, maxPositiveSampleCnt):
        # Find label and value pair with current lowest counts
        searchPair, _ = findLowestCount(sampleCnts, maxPositiveSampleCnt, alreadyDone)
        if searchPair[0] < 0:
            break

        # Grab a sample from each subject
        indicesToAdd = findSampleForEachSubject(searchPair, samplesLeft, labels, frameCnt,
                                                uniqueSubjects, uniqueValueModes)
        # uniqueSubjects, uniqueLabelValues)

        # Add to list of output samples
        outputIndexList += indicesToAdd

        # Did we add any?
        # print("\tAdded", len(indicesToAdd))

        if len(indicesToAdd) == 0:
            alreadyDone.append(searchPair)

        # Increment current counts
        # updateCounts(indicesToAdd, labels, frameCnt, uniqueLabelValues, sampleCnts, zeroSampleCnts)
        updateCounts(indicesToAdd, labels, frameCnt, uniqueValueModes, sampleCnts, zeroSampleCnts)

        # Remove these sample labels
        removeIndices(indicesToAdd, samplesLeft)

    # Results
    print("SAMPLE COUNTS:", sampleCnts)
    # print("ZERO SAMPLE COUNTS:", zeroSampleCnts)
    print("TOTAL NUMBER OF SAMPLES BEFORE:", len(labels))
    print("TOTAL NUMBER OF SAMPLES AFTER:", len(outputIndexList))

    aveStd = 0
    for realIndex in outputIndexList:
        oneStd = np.std(labels[realIndex])
        # print("\t", oneStd, ":", labels[realIndex])
        aveStd += oneStd
    aveStd /= len(labels)
    print("AVE STD:", aveStd)

    # print("FINAL LABEL LIST:")
    # for realIndex in outputIndexList:
    #    for frameIndex in range(frameCnt):
    #        print("\t",realIndex, ",", frameIndex, ":", labels[realIndex][frameIndex])

    # Generate output lists
    outputSamples = []
    outputLabels = []
    outputSubjects = []
    outputTasks = []
    for realIndex in outputIndexList:
        outputSamples.append(samples[realIndex])
        outputLabels.append(labels[realIndex])
        outputSubjects.append(subjects[realIndex])
        outputTasks.append(tasks[realIndex])

    totalSampleCntsAfter = countCurrentSamples(outputLabels, labelCnt, uniqueValueModes)
    print("TOTAL SAMPLE CNTS AFTER:", totalSampleCntsAfter)

    return outputSamples, outputLabels, outputSubjects, outputTasks














