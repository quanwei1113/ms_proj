# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from keras.preprocessing.image import ImageDataGenerator
import numpy as np
import os.path


class DataPreprocessor:

    def __init__(self, preprocessArgs, sourceDataGen=None, precomputedMean=None, precomputedStd=None,
                 precomputedPCA=None, filename=None):
        # Create the ImageDataGenerator object
        self.innerPreprocessor = ImageDataGenerator(**preprocessArgs)

        # Do we need to do any fitting?
        if (preprocessArgs.get('featurewise_center', False)
                or preprocessArgs.get('featurewise_std_normalization', False)
                or preprocessArgs.get('zca_whitening', False)):

            # Do we have a mean/std/PCA provided?
            if ((preprocessArgs.get('featurewise_center', False) and precomputedMean is None)
                    or (preprocessArgs.get('featurewise_std_normalization', False) and precomputedStd is None)
                    or (preprocessArgs.get('zca_whitening', False) and precomputedPCA is None)):
                # Can we get this from a file?
                if (filename is None) or (not self.load(filename)):
                    # Must load dataset
                    if sourceDataGen is None:
                        raise ValueError("sourceDataGen cannot be None!")

                    print("FITTING TO WHOLE DATASET...")

                    # Load ENTIRE dataset
                    allData = sourceDataGen.getEntireDataset()

                    # Compute fit on this data
                    self.innerPreprocessor.fit(allData)
                else:
                    print("LOADDING FROM FILE:", filename)
            else:
                print("USING PREVIOUS VALUES")

                self.innerPreprocessor.mean = precomputedMean
                self.innerPreprocessor.std = precomputedStd
                self.innerPreprocessor.principal_components = precomputedPCA

            # print("MEAN:", self.innerPreprocessor.mean)
            # print("STD:", self.innerPreprocessor.std)
            # print("PCA:", self.innerPreprocessor.principal_components)

    def __augmentAndStandardize(self, video):

        # Get shape
        videoShape = video.shape

        # Get the SAME transformation
        transformation = self.innerPreprocessor.get_random_transform(videoShape[1:])

        # Apply the transformation to each frame of the video
        for b in range(len(video)):
            for i in range(len(video[b])):
                # Normalize first
                video[b][i] = self.innerPreprocessor.standardize(video[b][i])
                # THEN do data augmentation
                video[b][i] = self.innerPreprocessor.apply_transform(video[b][i], transformation)

        return video

    def __standardizeOnly(self, video):
        for b in range(len(video)):
            for i in range(len(video[b])):
                video[b][i] = self.innerPreprocessor.standardize(video[b][i])

        return video

    def preprocessSequence(self, video, doDataAugmentation):

        if doDataAugmentation:
            # Do data augmentation
            video = self.__augmentAndStandardize(video)
        else:
            # Just normalize
            video = self.__standardizeOnly(video)

        return video

    def getPrecomputed(self):
        return self.innerPreprocessor.mean, self.innerPreprocessor.std, self.innerPreprocessor.principal_components

    def save(self, filename):
        np.savez(filename, mean=self.innerPreprocessor.mean, std=self.innerPreprocessor.std,
                 pca=self.innerPreprocessor.principal_components)

    def load(self, filename):
        if os.path.exists(filename):
            # Open file and grab stuff
            npzfile = np.load(filename)
            self.innerPreprocessor.mean = npzfile['mean']
            self.innerPreprocessor.std = npzfile['std']
            self.innerPreprocessor.principal_components = npzfile['pca']
            return True
        else:
            return False
