# MIT LICENSE
#
# Copyright 2018 Micah A. Church, Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import absolute_import, print_function

import copy
from scipy import ndimage
import keras
from keras.preprocessing.image import ImageDataGenerator
import keras.backend as K
from os.path import join
import os.path
import numpy as np
from abc import ABCMeta, abstractmethod
from Data.DataSampler import *
from Data.DataLoadParams import *
from Network.NetworkDataParams import *
from Training.AssessmentFunctions import *
import cv2
import random
import datetime
import sys
from joblib import Parallel, delayed
import multiprocessing
import tempfile


class BaseDataGenerator(keras.utils.Sequence):
    __metaclass__ = ABCMeta

    # DATA ####################################################################

    # params = DataLoadParams (or one of its descendants)
    # MAX_PAIN_INTENSITY = used by certain databases (set later, mostly here to make linting happy)

    # batch_size = batch size for getting data

    # allImageFiles = list of all image paths
    # allLabels = list of all label data
    # allSubjects = list of all subjects per sample
    # allTasks = list of all subjects per task
    # uniqueSubjects = list of (sorted) unique subjects

    # imageWidth = width AFTER cropping
    # imageHeight = height AFTER cropping

    # origImageWidth = original image width
    # origImageHeight = original image height

    # totalChannelCnt = channel count of image

    # dataPreprocessor = initially None, used for "image" preprocessing
    #                    (EXCEPT for cropping)

    # indexes = actual sample indices to use
    #               (can be shuffled with params.doShuffleIndices)

    # IF we are using params.preloadAllData:
    #   allPreloadedData = all image data loaded into memory
    #   allPreloadedLabels = all label data loaded into memory

    # CONSTRUCTORS ############################################################

    def __init__(self, dataParams):

        self.params = copy.deepcopy(dataParams)

        # Setting this for linting checks; will be set later
        self.MAX_PAIN_INTENSITY = 1.0

    def initialize(self, batch_size):
        # Set batch size
        self.batch_size = batch_size

        # Go through the first base directory and get all filenames based on ground-truth type
        self.allImageFiles, self.allLabels, self.allSubjects, self.allTasks \
            = self.getImageFilenames(self.params.lostFileList)

        # Get unique subjects
        self.uniqueSubjects = list(set(self.allSubjects))
        self.uniqueSubjects.sort()

        self.origImageHeight, self.origImageWidth, self.totalChannelCnt = self.__getTrueImageSize()
        print("DataLoader: true image size:", self.origImageHeight, self.origImageWidth)
        print("Cropping percentage:", self.params.cropPercentage)

        # Compute cropped size
        self.imageWidth = int(np.floor(self.params.cropPercentage * self.origImageWidth))
        self.imageHeight = int(np.floor(self.params.cropPercentage * self.origImageHeight))

        print("Cropped DataLoader image sizes:", self.imageHeight, self.imageWidth)

        # Unique possible values; used for sampling
        uniqueValueModes = self.getUniqueValueModes()

        # Do class balancing (effectively getting rid of first class)
        if self.params.doDataBalancing:
            print("CLASS DISTRIBUTION:")
            totalSampleCnts = countCurrentSamples(self.allLabels, self.getOutputCnt(), uniqueValueModes)
            print(totalSampleCnts)

            print("Data balancing requested...")

            self.allImageFiles, self.allLabels, self.allSubjects, self.allTasks \
                = balanceClasses(self.allImageFiles, self.allLabels, self.allSubjects, self.allTasks, uniqueValueModes,
                                 self.params.balanceRemoveRatio)

        # Do sampling?
        if self.params.samplingCount > 0:

            foundSamplingCache = False

            if self.params.useCachedData:
                # Can we load up the sampling used directly?
                if self.__doesSamplingCacheExist():
                    self.__loadSamplingFromCache()
                    foundSamplingCache = True

            if not foundSamplingCache:
                print("Performing sampling...")

                print("CLASS DISTRIBUTION BEFORE:")
                totalSampleCnts = countCurrentSamples(self.allLabels, self.getOutputCnt(), uniqueValueModes)
                print(totalSampleCnts)

                self.allImageFiles, self.allLabels, self.allSubjects, self.allTasks \
                    = sampleDatabase(samples=self.allImageFiles,
                                     labels=self.allLabels,
                                     subjects=self.allSubjects,
                                     tasks=self.allTasks,
                                     uniqueValueModes=uniqueValueModes,
                                     maxPositiveSampleCnt=self.params.samplingCount)

                # If we didn't find the cache BUT we are using cache, save it for future generations
            if not foundSamplingCache and self.params.useCachedData:
                self.__saveSamplingToCache()

                # Get unique subjects again, just in case
            self.uniqueSubjects = list(set(self.allSubjects))
            self.uniqueSubjects.sort()

        print("CLASS DISTRIBUTION NOW:")
        totalSampleCnts = countCurrentSamples(self.allLabels, self.getOutputCnt(), uniqueValueModes)
        print(totalSampleCnts)

        print("UNIQUE SUBJECTS:", self.uniqueSubjects)

        # Set data preprocessor to None initially
        self.dataPreprocessor = None

        # Do we need to preload the data?
        if self.params.preloadAllData:
            foundCache = False

            # Can we use cached data?
            if self.params.useCachedData:
                # This means two things:
                # 1) If we can we will load data from a cached temporary file
                # 2) We will save the data as a cached temporary file

                # Does the cache exist?
                if (self.__doesDataCacheExist() and self.__doesLabelCacheExist()):

                    # Load data from cached data
                    starttime = datetime.datetime.now()
                    self.__loadDataAndLabelsFromCache()
                    foundCache = True
                    endtime = datetime.datetime.now()
                    print("Time to load from cache:", (endtime - starttime))
                else:
                    print("Cached files do not exist!  Must load again...")

            if not foundCache:
                print("** PRELOADING DATA *************")
                starttime = datetime.datetime.now()
                self.allPreloadedData = self.loadEntireDataset()
                self.allPreloadedData = np.reshape(self.allPreloadedData,
                                                   (self.getTotalSampleCnt(),
                                                    self.params.frameCnt,
                                                    self.origImageHeight,
                                                    self.origImageWidth,
                                                    self.totalChannelCnt))

                print("** PRELOADING LABELS *************")
                self.allPreloadedLabels = self.loadEntireLabelset()

                endtime = datetime.datetime.now()
                print("Time to preload:", (endtime - starttime))

            if not foundCache and self.params.useCachedData:
                # Save the data for future generations
                starttime = datetime.datetime.now()
                self.__saveDataAndLabelsToCache()
                endtime = datetime.datetime.now()
                print("Time to save to cache:", (endtime - starttime))

        self.on_epoch_end()

    # METHODS ############################################################

    def __getCachedBasename(self):
        baseName = self.params.getHashKey()
        # print("CACHED BASENAME:", baseName)
        return baseName

    def __getCachedSamplingFilename(self):
        return self.__getCachedBasename() + "_SAMPLING.npz"

    def __getCachedDataFilename(self):
        return self.__getCachedBasename() + "_DATA.npy"

    def __getCachedLabelFilename(self):
        return self.__getCachedBasename() + "_LABELS.npy"

    def __getCachedPath(self, filename):
        tempDir = tempfile.gettempdir()
        return join(tempDir, filename)

    def __getCachedSamplingPath(self):
        return self.__getCachedPath(self.__getCachedSamplingFilename())

    def __getCachedDataPath(self):
        return self.__getCachedPath(self.__getCachedDataFilename())

    def __getCachedLabelPath(self):
        return self.__getCachedPath(self.__getCachedLabelFilename())

    def __doesCacheExist(self, filepath):
        print("Looking for:", filepath)
        return os.path.isfile(filepath)

    def __doesSamplingCacheExist(self):
        return self.__doesCacheExist(self.__getCachedSamplingPath())

    def __doesDataCacheExist(self):
        return self.__doesCacheExist(self.__getCachedDataPath())

    def __doesLabelCacheExist(self):
        return self.__doesCacheExist(self.__getCachedLabelPath())

    def __loadDataAndLabelsFromCache(self):
        print("** PRELOADING CACHED DATA *************")
        inDataFile = self.__getCachedDataPath()
        print(inDataFile)
        self.allPreloadedData = np.load(inDataFile)
        print("** PRELOADING CACHED LABELS *************")
        inLabelFile = self.__getCachedLabelPath()
        print(inLabelFile)
        self.allPreloadedLabels = np.load(inLabelFile)

    def __saveDataAndLabelsToCache(self):
        print("** SAVING CACHED DATA *************")
        outDataFile = self.__getCachedDataPath()
        print(outDataFile)
        np.save(outDataFile, self.allPreloadedData)
        print("** SAVING CACHED LABELS *************")
        outLabelFile = self.__getCachedLabelPath()
        print(outLabelFile)
        np.save(outLabelFile, self.allPreloadedLabels)

    def __loadSamplingFromCache(self):
        print("** PRELOADING CACHED SAMPLING *************")
        # Get file path
        inSamplingFile = self.__getCachedSamplingPath()
        # Load file
        npzfile = np.load(inSamplingFile)
        print("Sampling headers:", npzfile.files)
        # Load data (converting to list as necessary)
        self.allImageFiles = npzfile['allImageFiles'].tolist()
        self.allLabels = npzfile['allLabels'].tolist()
        self.allSubjects = npzfile['allSubjects'].tolist()
        self.allTasks = npzfile['allTasks'].tolist()

    def __saveSamplingToCache(self):
        print("** SAVING CACHED SAMPLING *************")
        # Get file path
        outSamplingFile = self.__getCachedSamplingPath()
        # Save arrays
        np.savez(outSamplingFile,
                 allImageFiles=np.array(self.allImageFiles),
                 allLabels=np.array(self.allLabels),
                 allSubjects=np.array(self.allSubjects),
                 allTasks=np.array(self.allTasks))

    def getClassWeights(self):
        uniqueValueModes = self.getUniqueValueModes()
        labels = self.getEntireLabelset()
        classSampleCnts = countCurrentSamples(labels, self.getOutputCnt(), uniqueValueModes)
        totalSampleCnt = self.getTotalSampleCnt()
        # Based on: "Fusing Multilabel Deep Networks for Facial Action Unit Detection", FG2017
        if self.params.desiredData == DATA_AU_OCC or self.params.desiredData == DATA_AU_SM_OCC:
            sampleWeights = []
            for i in range(self.getOutputCnt()):
                if int(classSampleCnts[i][0]) > 0 and int(classSampleCnts[i][1]) > 0:
                    sampleWeights.append(float(classSampleCnts[i][0]) / float(classSampleCnts[i][1]))
                    # if (classSampleCnts[i][0] > classSampleCnts[i][1]):
                    #    sampleWeights.append(float(classSampleCnts[i][0])/float(classSampleCnts[i][1]))
                    # else:
                    #    sampleWeights.append(float(classSampleCnts[i][1])/float(classSampleCnts[i][0]))
                else:
                    sampleWeights.append(1.0)
        else:
            sampleWeights = [1.0] * int(self.getOutputCnt())

        print("SAMPLE WEIGHTS:", sampleWeights)
        return sampleWeights

    def setDataPreprocessor(self, dataPreprocessor):
        self.dataPreprocessor = dataPreprocessor

    def getDataPreprocessor(self):
        return self.dataPreprocessor

    def getEntireDataset(self):
        if self.params.preloadAllData:
            # Can just return preloaded data
            return self.allPreloadedData
        else:
            return self.loadEntireDataset()

    def getAllSubjects(self):
        return self.allSubjects

    def getAllTasks(self):
        return self.allTasks

    def getAllFilenames(self):
        return self.allImageFiles

    def __loadEntireDataset_SingleThread(self):
        print("Loading all data")
        # Load ENTIRE dataset
        print("Loading ENTIRE dataset...")

        printFreq = 10  # 100
        allData = np.empty((self.getTotalSampleCnt(), self.params.frameCnt, self.origImageHeight, self.origImageWidth,
                            self.totalChannelCnt))
        for i in range(self.getTotalSampleCnt()):
            # print(i);

            for j in range(self.params.frameCnt):
                oneSample = self.getSample(i, j)

                if self.params.imageDataType == INPUT_DATATYPE_IMAGE:
                    if oneSample.shape[0] != self.origImageHeight or oneSample.shape[1] != self.origImageWidth:
                        oneSample = cv2.resize(oneSample, (self.origImageWidth, self.origImageHeight))

                allData[i, j,] = oneSample

            if i % printFreq == 0:
                percent = 100 * float(float(i) / self.getTotalSampleCnt())
                print("\r[Percent loaded: %f%% ]" % percent, end="")
                sys.stdout.flush()

        percent = 100.0000
        print("\r[Percent loaded: %f%% ]" % percent, end="")
        sys.stdout.flush()
        print("")
        allData = np.reshape(allData, (
        self.getTotalSampleCnt() * self.params.frameCnt, self.origImageHeight, self.origImageWidth,
        self.totalChannelCnt))
        print("Loading complete.")
        return allData

    def loadEntireDataset(self):
        return self.__loadEntireDataset_SingleThread()

    def __getEmptyLabelArray(self, sampleCnt, frameCnt):
        outputFormat = self.getDesiredOutFormat()

        if outputFormat == OUT_FLAT or outputFormat == OUT_MULTICLASS_FLAT:
            labels = np.empty((sampleCnt, frameCnt, self.getOutputCnt()), dtype=int)
        elif outputFormat == OUT_SP:
            # Structured Prediction
            labels = np.empty((sampleCnt, frameCnt, self.getStructuredPreditionDim(), self.getStructuredPreditionDim(),
                               self.getOutputCnt()), dtype=int)
        else:
            labels = np.empty(
                (sampleCnt, frameCnt, self.getFullImageDim()[0], self.getFullImageDim()[1], self.getOutputCnt()),
                dtype=int)

        return labels

    def loadEntireLabelset(self):

        labels = self.__getEmptyLabelArray(self.getTotalSampleCnt(), self.params.frameCnt)

        for i in range(self.getTotalSampleCnt()):
            for j in range(self.getFrameCnt()):
                labels[i, j,] = self.getLabel(i, j)

        return labels

    def getEntireLabelset(self):
        if self.params.preloadAllData:
            # Can just return preloaded data
            return self.allPreloadedLabels
        else:
            return self.loadEntireLabelset()

    def __len__(self):
        'Denotes the number of batches per epoch'
        # return int(np.floor(len(self.allImageFiles) / self.batch_size))
        return int(np.ceil(float(self.getTotalSampleCnt()) / float(self.batch_size)))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        totalSampleCnt = self.getTotalSampleCnt()
        if ((index + 1) * self.batch_size) <= totalSampleCnt:
            indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]
        else:
            indexes = self.indexes[index * self.batch_size:totalSampleCnt]

        # Generate data
        X, y = self.__data_generation(indexes)

        '''
        # Do we need dummy outputs?
        extraFakeOutputs = self.params.extraFakeOutputs

        if len(extraFakeOutputs) > 0:
            newY = [y]
            for extra in extraFakeOutputs:
                newY.append(np.zeros([y.shape[0], y.shape[1]] + extra))
            y = newY
        '''

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(self.getTotalSampleCnt())
        if self.params.doShuffleIndices == True:
            np.random.shuffle(self.indexes)

    def __getTrueImageSize(self):
        # Load "image"
        image = self.getSample(0, 0)
        # Clear the local loading cache if we have one
        self.clearLocalLoadingCache()
        # Return true image size
        return image.shape[0], image.shape[1], image.shape[-1]

    def clearLocalLoadingCache(self):
        pass

    def getUniqueValueModes(self):
        if self.params.normalizeLabels:
            return [0, 1]
        else:
            if self.params.desiredData == DATA_AU_INT:
                return list(range(0, int(MAX_AU_INTENSITY) + 1))
            elif self.params.desiredData == DATA_PAIN:
                return list(range(0, int(self.MAX_PAIN_INTENSITY) + 1))
            elif self.params.desiredData == DATA_AU_OCC:
                return [0, 1]
            elif self.params.desiredData == DATA_AU_SM_OCC:
                return [0, 1]
            elif self.params.desiredData == DATA_MICRO_EXP:
                return [0, 1]
            elif self.params.desiredData == DATA_PROTO_EXP:
                return [0, 1]
            else:
                raise ValueError("INVALID DATA MODE REQUESTED")

    # Get total number of images we have
    def getTotalSampleCnt(self):
        return len(self.allImageFiles)

    def getOutputCnt(self):
        return (np.asarray(self.allLabels[0]).shape[-1])

    def getImageWidth(self):
        return self.imageWidth

    def getImageHeight(self):
        return self.imageHeight

    def getFrameCnt(self):
        return self.params.frameCnt

    def getNumChannels(self):
        return self.totalChannelCnt

    def toggleDataAugmentation(self, doDataAugmentation):
        self.params.doDataAugmentation = doDataAugmentation

    def toggleShuffleIndices(self, doShuffleIndices):
        self.params.doShuffleIndices = doShuffleIndices
        self.on_epoch_end()

    def getParams(self):
        return copy.deepcopy(self.params)

    '''
    def getAllLabels(self):

        outputFormat = self.getDesiredOutFormat()
        if outputFormat == OUT_FLAT:
            labels = np.empty((self.getTotalSampleCnt(), self.params.frameCnt, self.getOutputCnt()), dtype=int)
        elif outputFormat == OUT_SP:
            # Structured Prediction
            labels = np.empty((self.getTotalSampleCnt(), self.params.frameCnt, self.getStructuredPreditionDim(), self.getStructuredPreditionDim(), self.getOutputCnt()), dtype=int)
            # Full patch
            #labels = np.empty((self.getTotalSampleCnt(), self.params.frameCnt, self.getFullImageDim()[0], self.getFullImageDim()[1], self.getOutputCnt()), dtype=int)
        else:
            labels = np.empty((self.getTotalSampleCnt(), self.params.frameCnt, self.getFullImageDim()[0], self.getFullImageDim()[1], self.getOutputCnt()), dtype=int)

        for i in range(self.getTotalSampleCnt()):            
            for j in range(self.getFrameCnt()):                     
                labels[i,j,] = self.getLabel(self.indexes[i], j)            

        return labels
    '''

    def generateNetworkDataParams(self):
        netDataParams = NetworkDataParams()
        netDataParams.frameCnt = self.getFrameCnt()
        netDataParams.origImageHeight = self.getImageHeight()
        netDataParams.origImageWidth = self.getImageWidth()
        netDataParams.numChannels = self.getNumChannels()
        netDataParams.outputCnt = self.getOutputCnt()
        netDataParams.lastActivationFunction = self.getActivationFunction()
        netDataParams.labelBounds = self.getLabelValueBounds()
        netDataParams.fullImageDim = self.getFullImageDim()
        netDataParams.desiredOutFormat = self.getDesiredOutFormat()
        netDataParams.s = self.getStructuredPreditionDim()
        return netDataParams

    def getDesiredOutFormat(self):
        if self.params.desiredData == DRIVE_DATA_FULL or \
                self.params.desiredData == RITE_DATA_FULL:
            return OUT_IMAGE
        elif self.params.desiredData == DRIVE_DATA_PATCH_FULL or \
                self.params.desiredData == RITE_DATA_PATCH_FULL:
            return OUT_SP
        elif self.params.desiredData == DATA_AU_SM_OCC:
            return OUT_MULTICLASS_FLAT
        else:
            return OUT_FLAT

    def getStructuredPreditionDim(self):
        return 1

    def getFullImageDim(self):
        if self.params.desiredData == DRIVE_DATA_FULL or \
                self.params.desiredData == RITE_DATA_FULL:
            return (self.getImageHeight(), self.getImageWidth())
        elif self.params.desiredData == DRIVE_DATA_PATCH_FULL or \
                self.params.desiredData == RITE_DATA_PATCH_FULL:
            return (self.params.subimageHeight, self.params.subimageWidth)
        else:
            return (0, 0)

    def getActivationFunction(self):
        if self.params.desiredData == DATA_AU_INT:
            return ACTIVATE_NOTHING  # ACTIVATE_SIGMOID
        elif self.params.desiredData == DATA_PAIN:
            return ACTIVATE_NOTHING  # ACTIVATE_THRESHOLD #ACTIVATE_SIGMOID
        elif self.params.desiredData == DATA_AU_OCC:
            return ACTIVATE_SIGMOID
        elif self.params.desiredData == DATA_AU_SM_OCC:
            return ACTIVATE_SOFTMAX
        elif self.params.desiredData == DRIVE_DATA_PATCH or \
                self.params.desiredData == DRIVE_DATA_PATCH_FULL or \
                self.params.desiredData == RITE_DATA_PATCH or \
                self.params.desiredData == RITE_DATA_PATCH_FULL or \
                self.params.desiredData == DRIVE_DATA_FULL or \
                self.params.desiredData == RITE_DATA_FULL:
            return ACTIVATE_SOFTMAX
        elif self.params.desiredData == DATA_MICRO_EXP:
            return ACTIVATE_SOFTMAX
        elif self.params.desiredData == DATA_PROTO_EXP:
            return ACTIVATE_SOFTMAX
        else:
            raise ValueError("INVALID DATA MODE REQUESTED")

    def getEvaluationFunction(self):
        if self.params.desiredData == DATA_AU_INT:
            return evaluateAllRegression
        elif self.params.desiredData == DATA_PAIN:
            return evaluateAllRegression
        elif self.params.desiredData == DATA_AU_OCC:
            return evaluateAllMultiClassification
        elif self.params.desiredData == DATA_AU_SM_OCC:
            return evaluateAllIndExclusiveClassification
        elif self.params.desiredData == DRIVE_DATA_PATCH or \
                self.params.desiredData == DRIVE_DATA_PATCH_FULL or \
                self.params.desiredData == RITE_DATA_PATCH or \
                self.params.desiredData == RITE_DATA_PATCH_FULL or \
                self.params.desiredData == DRIVE_DATA_FULL or \
                self.params.desiredData == RITE_DATA_FULL:
            return evaluateAllExclusiveClassification
        elif self.params.desiredData == DATA_MICRO_EXP:
            return evaluateAllExclusiveClassification
        elif self.params.desiredData == DATA_PROTO_EXP:
            return evaluateAllExclusiveClassification
        else:
            raise ValueError("INVALID DATA MODE REQUESTED")

    def getLabelValueBounds(self):
        if self.params.normalizeLabels:
            return (0, 1)
        else:
            if self.params.desiredData == DATA_AU_INT:
                return (0, MAX_AU_INTENSITY)
            elif self.params.desiredData == DATA_PAIN:
                print("MAX PAIN INTENSITY:", self.MAX_PAIN_INTENSITY)
                return (0, self.MAX_PAIN_INTENSITY)
            elif self.params.desiredData == DRIVE_DATA_PATCH or \
                    self.params.desiredData == DRIVE_DATA_PATCH_FULL or \
                    self.params.desiredData == RITE_DATA_PATCH_FULL or \
                    self.params.desiredData == RITE_DATA_PATCH or \
                    self.params.desiredData == DRIVE_DATA_FULL or \
                    self.params.desiredData == RITE_DATA_FULL:
                return (0, 1)
            elif self.params.desiredData == DATA_AU_OCC:
                return (0, 1)
            elif self.params.desiredData == DATA_AU_SM_OCC:
                return (0, 1)
            elif self.params.desiredData == DATA_MICRO_EXP:
                return (0, 1)
            elif self.params.desiredData == DATA_PROTO_EXP:
                return (0, 1)
            else:
                raise ValueError("INVALID DATA MODE REQUESTED")

    def crop_video(self, video):
        return self.__crop_video(video)

    def __crop_video(self, video):

        # Crop images
        offX = int((self.origImageWidth - self.imageWidth) / 2.0)
        offY = int((self.origImageHeight - self.imageHeight) / 2.0)

        croppedVideo = video[..., offY:(self.imageHeight + offY), offX:(self.imageWidth + offX), :]

        return croppedVideo

    def __data_generation(self, indexes):
        'Generates data containing batch_size samples'  # X : (n_samples, *dim, n_channels)
        # Initialization
        # print("**********************************************************************")
        # print("In data generation")
        # print("**********************************************************************")
        indexCnt = len(indexes)

        # Prepare data array (or load it entirely)
        if not self.params.preloadAllData:
            X = np.empty(
                (indexCnt, self.params.frameCnt, self.origImageHeight, self.origImageWidth, self.totalChannelCnt))
        else:
            X = self.allPreloadedData[indexes, 0:self.params.frameCnt]

        # Determine output of labels
        y = self.__getEmptyLabelArray(indexCnt, self.params.frameCnt)

        # Generate data
        for i in range(indexCnt):
            # print (ID)
            for j in range(self.params.frameCnt):
                # Store sample
                if not self.params.preloadAllData:
                    X[i, j,] = self.getSample(indexes[i], j)

                # Store class
                y[i, j] = self.getLabel(indexes[i], j)

                # Do we have any preprocessing to do?
        if self.dataPreprocessor is not None:
            X = self.dataPreprocessor.preprocessSequence(X, self.params.doDataAugmentation)

        # Crop video
        X = self.__crop_video(X)

        return X, y

    @abstractmethod
    def getImageFilenames(self, lostFilelist):
        pass

    @abstractmethod
    def getSample(self, sampleIndex, frameIndex):
        pass
        # return ndimage.imread(self.params.baseDir + '/' + str(self.allImageFiles[sampleIndex][frameIndex]))/255.0

    @abstractmethod
    def getLabel(self, sampleIndex, frameIndex):
        pass
        # return self.allLabels[sampleIndex][frameIndex]
