# MIT LICENSE
#
# Copyright 2018 Ben Klinghoffer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from Data.BaseDataGenerator import *
from Data.CommonFunctions import *
from Data.Cached3DSequenceData import *

ethnicity = ['F0001', 'WH', 'F0002', 'BL', 'F0003', 'LA', 'F0004', 'LA', 'F0005', 'AE', 'F0006', 'WH',
             'F0007', 'WH', 'F0008', 'WH', 'F0009', 'WH', 'F0010', 'WH', 'F0011', 'AE', 'F0012', 'WH',
             'F0013', 'AE', 'F0014', 'WH', 'F0015', 'WH', 'F0016', 'WH', 'F0017', 'WH', 'F0018', 'WH',
             'F0019', 'AE', 'F0020', 'WH', 'F0021', 'WH', 'F0022', 'WH', 'F0023', 'WH', 'F0024', 'LA',
             'F0025', 'WH', 'F0026', 'WH', 'F0027', 'WH', 'F0028', 'AE', 'F0029', 'WH', 'F0030', 'BL',
             'F0031', 'WH', 'F0032', 'WH', 'F0033', 'WH', 'F0034', 'WH', 'F0035', 'AE', 'F0036', 'AE',
             'F0037', 'WH', 'F0038', 'AE', 'F0039', 'AE', 'F0040', 'WH', 'F0041', 'AE', 'F0042', 'AE',
             'F0043', 'BL', 'F0044', 'LA', 'F0045', 'BL', 'F0046', 'AE', 'F0047', 'LA', 'F0048', 'AE',
             'F0049', 'WH', 'F0050', 'BL', 'F0051', 'WH', 'F0052', 'WH', 'F0053', 'BL', 'F0054', 'WH',
             'F0055', 'BL', 'F0056', 'LA', 'M0001', 'AM', 'M0002', 'BL', 'M0003', 'WH', 'M0004', 'WH',
             'M0005', 'IN', 'M0006', 'AE', 'M0007', 'AE', 'M0008', 'WH', 'M0009', 'WH', 'M0010', 'AE',
             'M0011', 'WH', 'M0012', 'AE', 'M0013', 'AE', 'M0014', 'WH', 'M0015', 'WH', 'M0016', 'WH',
             'M0017', 'WH', 'M0018', 'WH', 'M0019', 'IN', 'M0020', 'IN', 'M0021', 'WH', 'M0022', 'WH',
             'M0023', 'IN', 'M0024', 'AE', 'M0025', 'AE', 'M0026', 'AE', 'M0027', 'AE', 'M0028', 'WH',
             'M0029', 'WH', 'M0030', 'WH', 'M0031', 'LA', 'M0032', 'WH', 'M0033', 'AM', 'M0034', 'WH',
             'M0035', 'WH', 'M0036', 'WH', 'M0037', 'IN', 'M0038', 'BL', 'M0039', 'AE', 'M0040', 'WH',
             'M0041', 'WH', 'M0042', 'LA', 'M0043', 'AE', 'M0044', 'IN']

allSubjects_BU3 = ethnicity[::2]

trainSet_BU3 = allSubjects_BU3[::2]
testSet_BU3 = allSubjects_BU3[1::2]


class BU3DDataGenerator(BaseDataGenerator):

    def __init__(self, BU3DParams):
        cloudCnt = 1
        if not BU3DParams.useFullModel:
            cloudCnt = len(BU3DParams.desiredFeaturePoints)
        self.localSequenceCache = Cached3DSequenceData(sampleIndex=0, cloudCnt=cloudCnt, frameCnt=BU3DParams.frameCnt)

        self.tasks = ['AN', 'DI', 'FE', 'HA', 'SA', 'SU']
        super(BU3DDataGenerator, self).__init__(BU3DParams)

    def getImageFilenames(self, lostFilelist):

        imagePath = self.params.baseDir
        sequenceLabels = []
        sequenceSubjects = []
        sequenceTask = []
        sequenceFilename = []

        if self.params.loadPoseCorrected:
            fileEnding = "_F3D"
        else:
            fileEnding = "_RAW"

        zeroLabels = np.zeros(len(self.params.desiredClasses))

        for subject in self.params.validSubjects:
            currentSubjectPath = join(imagePath, subject)
            subjectIndex = ethnicity.index(subject)

            classIndex = 0

            for classes in self.params.desiredClasses:
                subClasses = classes.split('-')

                for sClass in subClasses:
                    filename = subject + '_' + sClass + ethnicity[subjectIndex + 1] + fileEnding

                    label = zeroLabels.copy()
                    label[classIndex] = 1

                    sequenceFilename.append([join(currentSubjectPath, filename)])
                    sequenceLabels.append([label])
                    sequenceSubjects.append(subject)
                    sequenceTask.append(sClass)
                classIndex += 1

        return sequenceFilename, sequenceLabels, sequenceSubjects, sequenceTask

    def clearLocalLoadingCache(self):
        self.localSequenceCache.reset(0)

    def getSample(self, sampleIndex, frameIndex):
        # startTime = datetime.datetime.now()

        cloud = np.array(getPointCloud(self.allImageFiles[sampleIndex][frameIndex] + ".wrl",
                                       self.allImageFiles[sampleIndex][frameIndex] + ".bnd",
                                       self.params))
        if self.params.useDPCCFormat:
            # Need to convert to DPCC format
            cloud = convertToDPCC(cloud, self.params, sampleIndex, frameIndex,
                                  self.localSequenceCache, self.allImageFiles, ".wrl", ".bnd")

        # endTime = datetime.datetime.now()
        # print("CLOUD TIME:", (endTime - startTime))

        return cloud

    def getLabel(self, sampleIndex, frameIndex):
        return self.allLabels[sampleIndex][frameIndex]
