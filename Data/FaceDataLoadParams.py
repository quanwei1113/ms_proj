from Data.DataLoadParams import *

desiredPoints = (
    20, 24, 28, 32,  # eye corners
    36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,  # nose
    48, 49, 50, 51, 52, 53, 54,
    55, 56, 57, 58, 59,
    60, 61, 62, 63, 64,
    65, 66, 67,  # mouth points
    74, 75, 76,  # chin points
    69, 70, 71,  # left side of face
    79, 80, 81  # right side of face
)


class FaceDataLoadParams(DataLoadParams):
    def __init__(self):
        super(FaceDataLoadParams, self).__init__()

        # If adding nearest neighbor indices, number of nearest neighbors to retain
        self.nearestNeighborCnt = 10

        self.desiredFeaturePoints = desiredPoints

        self.useFullModel = True

        self.cloudRadius = 1

        self.maxPointCount = 4096

        self.isRelativeToPoints = False

        self.doPoseNormalize = False

        self.doFaceCrop = False

        self.cropFaceCenterPoints = [39, 44, 20, 28]

        self.cropFaceChinPoint = 75

        self.tbndBaseDir = "/media/Data/BP4D/3DFeatures"

        self.H5_3D_Data = "ColorData"

        # preciseData
        # normalData
        # colorData
        # depthData
        self.H5DatasetName = "colorData"









