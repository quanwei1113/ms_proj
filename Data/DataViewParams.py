# MIT LICENSE
#
# Copyright 2018 Michael J. Reale, Micah A. Church
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

from os.path import join, isfile
from os import listdir
import csv
import numpy as np
import pickle
from General.BaseParams import *


class DataViewParams(BaseParams):

    def __init__(self):
        # Desired subjects
        self.selectedSubjects = ["F001", "F002", "F003", "F004", "F005", "F006",
                                 "F007", "F008", "F009", "F010", "F011", "F012", "F013",
                                 "F014", "F015", "F016", "F017", "F018", "F019", "F020",
                                 "F021", "F022", "F023"
                                 ]

        # Shuffle indices for batches?
        self.doShuffleIndices = False

        # Do data augmentation at all?
        self.doDataAugmentation = False


