# MIT LICENSE
#
# Copyright 2018 Ben Klinghoffer, Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

import numpy as np
import pyrr

rigidPoints = (
    20, 24, 28, 32,  # eye corners

    36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,  # nose
)

allMouthPoints = (
    48, 49, 50, 51, 52, 53, 54,
    55, 56, 57, 58, 59,
    60, 61, 62, 63, 64,
    65, 66, 67
)

allChinPoints = (
    74, 75, 76
)

allSidePoints = (
    69, 70, 71,  # left side of face
    79, 80, 81  # right side of face
)


class BP4DFeaturePoints3D:

    def __init__(self, filename):

        temp = []

        try:
            f = open(filename)
            for line in f:
                line = line.split()
                line += tuple("1")
                temp.append(line)
            self.correctionPoints = np.array(temp[0][:3], float)
            self.featurePoints = np.array(temp[1:], float)
            self.meanPoint = np.array([0, 0, 0, 0], float)

            for point in rigidPoints:
                self.meanPoint += self.featurePoints[point]
            self.meanPoint /= len(rigidPoints)

        except IOError as error:
            print(".tbnd file not found. ", filename)
            raise IOError(error)

    def getBackwardPoseMatrix(self):

        x_rotation = pyrr.matrix44.create_from_x_rotation(np.deg2rad(self.correctionPoints[0]))
        y_rotation = pyrr.matrix44.create_from_y_rotation(np.deg2rad(self.correctionPoints[1]))
        z_rotation = pyrr.matrix44.create_from_z_rotation(np.deg2rad(-self.correctionPoints[2]))

        x_rotation = np.transpose(x_rotation)
        y_rotation = np.transpose(y_rotation)
        z_rotation = np.transpose(z_rotation)

        return np.matmul(np.matmul(z_rotation, y_rotation), x_rotation)

    def getBackwardCenterTranslateMatrix(self):

        return pyrr.matrix44.create_from_translation(-self.meanPoint)

    def computeScalingMatrix(self, width, height, chinSidesWeight):

        eyePoints = []
        for point in rigidPoints[:4]:
            eyePoints.append(self.featurePoints[point])
        eyePoints = np.array(eyePoints)

        mouthPoints = []
        for point in allMouthPoints:
            mouthPoints.append(self.featurePoints[point])
        mouthPoints = np.array(mouthPoints)

        chinPoints = []
        for point in allChinPoints:
            chinPoints.append(self.featurePoints[point])
        chinPoints = np.array(chinPoints)

        sidePoints = []
        for point in allSidePoints:
            sidePoints.append(self.featurePoints[point])
        sidePoints = np.array(sidePoints)

        backCenter = self.getBackwardCenterTranslateMatrix()
        backPose = self.getBackwardPoseMatrix()
        backward = np.matmul(backPose, backCenter)

        for i in range(len(eyePoints)):
            eyePoints[i] = np.dot(eyePoints[i], backward)

        for i in range(len(mouthPoints)):
            mouthPoints[i] = np.dot(mouthPoints[i], backward)

        for i in range(len(chinPoints)):
            chinPoints[i] = np.dot(chinPoints[i], backward)

        for i in range(len(sidePoints)):
            sidePoints[i] = np.dot(sidePoints[i], backward)

        meanMouth = mouthPoints.mean(axis=0)

        leftEye = eyePoints[:2].mean(axis=0)
        rightEye = eyePoints[2:].mean(axis=0)

        meanEye = (leftEye + rightEye) / 2

        centerPoint = (meanEye + meanMouth) / 2

        centerTranslateMat = pyrr.matrix44.create_from_translation((-centerPoint[0], -centerPoint[1], 0))

        eyeDist = pyrr.vector.length(rightEye[:2] - leftEye[:2])

        scaleX = (width / 3) / eyeDist

        eyeMouthDist = pyrr.vector.length(meanEye[:2] - meanMouth[:2])

        scaleY = (height / 3) / eyeMouthDist

        leftSide = sidePoints[:3].mean(axis=0)
        rightSide = sidePoints[3:].mean(axis=0)

        sideDist = pyrr.vector.length(leftSide[:2] - rightSide[:2])

        sideScaleX = width / sideDist

        meanChin = chinPoints.mean(axis=0)

        chinDist = pyrr.vector.length(centerPoint[:2] - meanChin[:2])

        chinScaleY = (height / 2) / chinDist

        altWeight = 1 - chinSidesWeight
        scaleX = (scaleX * altWeight + sideScaleX * chinSidesWeight)
        scaleY = (scaleY * altWeight + chinScaleY * chinSidesWeight)

        scaleMat = pyrr.matrix44.create_from_scale((scaleX, scaleY, 1))

        return np.matmul(centerTranslateMat, scaleMat)