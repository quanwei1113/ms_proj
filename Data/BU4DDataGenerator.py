from Data.BaseDataGenerator import *
from Data.CommonFunctions import *
from Data.Cached3DSequenceData import *

allSubjects_BU4 = ['F001', 'F002', 'F003', 'F004', 'F005', 'F006',
                   'F007', 'F008', 'F009', 'F010', 'F011', 'F012',
                   'F013', 'F014', 'F015', 'F016', 'F017', 'F018',
                   'F019', 'F020', 'F021', 'F022', 'F023', 'F024',
                   'F025', 'F026', 'F027', 'F028', 'F029', 'F030',
                   'F031', 'F032', 'F033', 'F034', 'F035', 'F036',
                   'F037', 'F038', 'F039', 'F040', 'F041', 'F042',
                   'F043', 'F044', 'F045', 'F046', 'F047', 'F048',
                   'F049', 'F050', 'F051', 'F052', 'F053', 'F054',
                   'F055', 'F056', 'F057', 'F058', 'M001', 'M002',
                   'M003', 'M004', 'M005', 'M006', 'M007', 'M008',
                   'M009', 'M010', 'M011', 'M012', 'M013', 'M014',
                   'M015', 'M016', 'M017', 'M018', 'M019', 'M020',
                   'M021', 'M022', 'M023', 'M024', 'M025', 'M026',
                   'M027', 'M028', 'M029', 'M030', 'M031', 'M032',
                   'M033', 'M034', 'M035', 'M036', 'M037', 'M038',
                   'M039', 'M040', 'M041', 'M042', 'M043']

allTasks_BU4 = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise']

endFrames = [[111, 107, 110, 110, 113, 109], [108, 107, 104, 110, 112, 101], [109, 110, 109, 109, 110, 109],
             [110, 114, 107, 113, 111, 108], [91, 100, 93, 96, 97, 82], [114, 110, 111, 110, 109, 112],
             [106, 103, 103, 107, 103, 100], [106, 105, 109, 113, 103, 106], [107, 109, 103, 103, 110, 98],
             [108, 107, 105, 111, 108, 101], [110, 106, 110, 98, 118, 111], [111, 105, 110, 110, 108, 105],
             [105, 110, 110, 99, 107, 108], [110, 110, 111, 100, 111, 107], [109, 113, 110, 110, 109, 109],
             [109, 110, 108, 108, 114, 110], [106, 106, 111, 105, 114, 110], [109, 109, 103, 111, 109, 102],
             [94, 91, 93, 84, 98, 91], [71, 98, 103, 88, 84, 80], [127, 103, 96, 97, 95, 93],
             [109, 109, 107, 110, 112, 109], [92, 99, 97, 92, 95, 90], [99, 109, 108, 106, 107, 104],
             [110, 111, 106, 104, 105, 105], [86, 82, 93, 94, 90, 85], [103, 102, 106, 100, 99, 102],
             [99, 106, 104, 94, 104, 85], [99, 100, 104, 107, 108, 100], [103, 110, 107, 109, 110, 113],
             [134, 141, 125, 102, 104, 107], [133, 98, 94, 100, 87, 95], [91, 95, 95, 95, 89, 91],
             [102, 89, 88, 95, 89, 98], [100, 98, 94, 95, 89, 94], [108, 100, 96, 96, 99, 103],
             [102, 109, 104, 102, 104, 105], [105, 104, 107, 100, 105, 101], [90, 86, 79, 73, 72, 90],
             [88, 77, 78, 73, 78, 75], [103, 106, 108, 104, 114, 103], [87, 113, 78, 86, 88, 73],
             [103, 97, 94, 99, 97, 88], [104, 114, 99, 109, 94, 99], [95, 96, 103, 101, 100, 96],
             [100, 102, 96, 98, 101, 83], [98, 100, 99, 102, 104, 107], [89, 88, 95, 94, 95, 89],
             [94, 99, 93, 93, 96, 96], [96, 103, 94, 95, 98, 97], [93, 91, 100, 99, 93, 97],
             [96, 97, 96, 105, 100, 97], [99, 98, 100, 97, 98, 100], [91, 95, 92, 94, 100, 91],
             [92, 97, 91, 95, 95, 96], [92, 93, 86, 94, 93, 93], [118, 109, 108, 112, 111, 108],
             [100, 68, 72, 76, 87, 89], [110, 111, 109, 110, 112, 110], [110, 113, 111, 110, 110, 110],
             [110, 104, 111, 114, 118, 112], [110, 116, 110, 111, 109, 112], [114, 105, 110, 110, 114, 113],
             [107, 106, 100, 108, 109, 109], [108, 110, 112, 103, 108, 101], [107, 107, 105, 110, 112, 111],
             [91, 93, 98, 90, 96, 87], [82, 85, 94, 90, 96, 87], [80, 87, 83, 86, 91, 95],
             [83, 88, 86, 87, 90, 86], [87, 97, 94, 82, 100, 97], [79, 94, 100, 89, 97, 90],
             [78, 73, 73, 68, 78, 77], [101, 100, 100, 102, 105, 101], [106, 107, 103, 102, 103, 107],
             [96, 95, 95, 93, 95, 92], [105, 96, 104, 102, 103, 100], [79, 81, 79, 84, 81, 85],
             [77, 87, 97, 86, 95, 87], [80, 95, 99, 89, 97, 95], [91, 99, 86, 88, 88, 87],
             [92, 83, 92, 80, 97, 90], [108, 91, 89, 84, 93, 85], [99, 101, 99, 95, 94, 100],
             [94, 99, 93, 99, 93, 106], [99, 99, 99, 100, 98, 99], [99, 105, 95, 97, 100, 98],
             [95, 96, 94, 92, 85, 99], [99, 88, 97, 99, 97, 100], [95, 94, 89, 91, 96, 88],
             [98, 90, 90, 99, 93, 97], [95, 93, 93, 95, 94, 94], [91, 94, 85, 94, 84, 88],
             [94, 104, 95, 94, 94, 97], [88, 95, 95, 87, 89, 89], [92, 94, 83, 90, 96, 84],
             [95, 98, 99, 92, 97, 87], [86, 90, 97, 96, 95, 92], [92, 90, 92, 87, 100, 98],
             [92, 98, 93, 95, 86, 97], [95, 95, 100, 100, 100, 102]]


class BU4DDataGenerator(BaseDataGenerator):

    def __init__(self, faceParams):
        cloudCnt = 1
        if not faceParams.useFullModel:
            cloudCnt = len(faceParams.desiredFeaturePoints)
        self.localSequenceCache = Cached3DSequenceData(sampleIndex=0, cloudCnt=cloudCnt, frameCnt=faceParams.frameCnt)

        super(BU4DDataGenerator, self).__init__(faceParams)

    def getImageFilenames(self, lostFilelist):
        sequenceLabels = []
        sequenceSubjects = []
        sequenceTask = []
        sequenceFilename = []

        groundFile = open(self.params.baseGroundPath)
        lines = [line.split() for line in groundFile]

        for subject in self.params.validSubjects:
            currentSubjectPath = subject
            print(currentSubjectPath)
            subjectIndex = allSubjects_BU4.index(subject)

            for task in self.params.desiredTasks:
                currentTaskPath = join(currentSubjectPath, task)
                taskTruth = lines[subjectIndex * 6 + taskIndex]
                zeroTruth = np.zeros(len(self.params.desiredClasses))
                taskIndex = allTasks_BU4.index(task)
                files = []
                truths = []

                for classes in self.params.desiredClasses:
                    if (task + "-Onset") == classes:
                        for frame in range(taskTruth[2], taskTruth[3]):
                            frameNumber = str(frame).zfill(3)
                            files.append(join(currentTaskPath, frameNumber))
                            sequenceSubjects.append(subject)
                            sequenceTask.append(task)
                            truth = zeroTruth.copy()
                            classIndex = self.params.desiredClasses.index(task + "-Onset")
                            truth[classIndex] = 1
                            truths.append(truth)
                    if (task + "-Peak") == classes:
                        for frame in range(taskTruth[3], taskTruth[4]):
                            frameNumber = str(frame).zfill(3)
                            files.append(join(currentTaskPath, frameNumber))
                            sequenceSubjects.append(subject)
                            sequenceTask.append(task)
                            truth = zeroTruth.copy()
                            classIndex = self.params.desiredClasses.index(task + "-Peak")
                            truth[classIndex] = 1
                            truths.append(truth)
                    if (task + "-Offset") == classes:
                        for frame in range(taskTruth[4], taskTruth[5]):
                            frameNumber = str(frame).zfill(3)
                            files.append(join(currentTaskPath, frameNumber))
                            sequenceTask.append(task)
                            truth = zeroTruth.copy()
                            classIndex = self.params.desiredClasses.index(task + "-Offset")
                            truth[classIndex] = 1
                            truths.append(truth)
                sequenceLabels.append(truths)
                sequenceFilename.append(files)

        return sequenceFilename, sequenceLabels, sequenceSubjects, sequenceTask

    def clearLocalLoadingCache(self):
        self.localSequenceCache.reset(0)

    def getSample(self, sampleIndex, frameIndex):
        cloud = np.array(
            getPointCloud(join(self.params.baseDir, self.allImageFiles[sampleIndex][frameIndex] + ".wrl"),
                          join(self.params.tbndBaseDir, self.allImageFiles[sampleIndex][frameIndex] + ".bnd"),
                          self.params))

        if self.params.useDPCCFormat:
            # Need to convert to DPCC format
            cloud = convertToDPCC(cloud, self.params, sampleIndex, frameIndex,
                                  self.localSequenceCache, self.allImageFiles, ".wrl", ".bnd")

        return cloud

    def getLabel(self, sampleIndex, frameIndex):
        return self.allLabels[sampleIndex][frameIndex]

