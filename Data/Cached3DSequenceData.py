# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

import numpy as np

DEBUG_PRINT = False


class Cached3DSequenceData:

    def __init__(self, sampleIndex, frameCnt, cloudCnt):
        self.cloudCnt = cloudCnt
        self.frameCnt = frameCnt
        if DEBUG_PRINT: print("Cached3DSequenceData::init: cloudCnt =", self.cloudCnt)
        if DEBUG_PRINT: print("Cached3DSequenceData::init: frameCnt =", self.frameCnt)
        self.reset(sampleIndex)

    def reset(self, sampleIndex):
        # treeList[frame][cloud]
        self.sampleIndex = sampleIndex
        self.treeList = np.full((self.frameCnt, self.cloudCnt), None)
        self.dataList = np.full((self.frameCnt, self.cloudCnt), None)
        if DEBUG_PRINT: print("Cached3DSequenceData::reset: sampleIndex =", self.sampleIndex)
        if DEBUG_PRINT: print("Cached3DSequenceData::reset: treeList =", self.treeList.shape)

    def setTree(self, frame, cloud, tree):
        self.treeList[frame][cloud] = tree
        if DEBUG_PRINT: print("Cached3DSequenceData::setTree(frame =", frame, ", cloud =", cloud)

    def setData(self, frame, cloud, data):
        self.dataList[frame][cloud] = data

    def getData(self, frame, cloud):
        return self.dataList[frame][cloud]

    def isSameSample(self, otherSample):
        if DEBUG_PRINT: print("Cached3DSequenceData::isSameSample:", (self.sampleIndex == otherSample))
        return (self.sampleIndex == otherSample)

    def getTree(self, frame, cloud):
        if DEBUG_PRINT: print("Cached3DSequenceData::getTree(frame =", frame, ", cloud =", cloud, ") -->",
                              self.treeList[frame][cloud])
        return self.treeList[frame][cloud]

    def hasTree(self, frame, cloud):
        # print("TREE?", frame, cloud, self.treeList[frame][cloud])
        if DEBUG_PRINT: print("Cached3DSequenceData::hasTree(frame =", frame, ", cloud =", cloud, ") -->",
                              (self.treeList[frame][cloud] is not None))

        return self.treeList[frame][cloud] is not None

    def getFrameCnt(self):
        if DEBUG_PRINT: print("Cached3DSequenceData::getFrameCnt -->", self.frameCnt)

        return self.frameCnt
