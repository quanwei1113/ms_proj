# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import absolute_import, print_function

import copy
from scipy import ndimage
import keras
from keras.preprocessing.image import ImageDataGenerator
import keras.backend as K
from os.path import join
import numpy as np
from abc import ABCMeta, abstractmethod
from Data.DataSampler import *
from Data.DataLoadParams import *
from Data.BaseDataGenerator import *
from Network.NetworkDataParams import *
from Training.AssessmentFunctions import *
import cv2
import random
import datetime


class DataViewGenerator(keras.utils.Sequence):
    __metaclass__ = ABCMeta

    # The purpose of this class is to provide a wrapper around a BaseDataGenerator
    # (or one of its descendents).
    # It exposes the same functionality as a BaseDataGenerator, BUT:
    # - It doesn't actually inherit from it
    # - Uses ONLY data from selected subjects
    # - Contains a BaseDataGenerator that ACTUALLY has all of the data
    # It is expected it will handle:
    # - Index shuffling
    # - Cropping (although how much and the actual function is in the inner generator)
    # - All other data preprocessing

    # DATA ####################################################################

    # viewParams = Data VIEW params (not Data load params)
    # innerGenerator = the actual generator
    # dataPreprocessor = the data preprocessor
    # validIndices = indices of data (relative to ALL data) that this DataView handles (based on subjects)
    # indices = actual indices (relative to validIndices) used (may be shuffled)

    # CONSTRUCTORS ############################################################

    def __init__(self, viewParams, generator):

        self.viewParams = copy.deepcopy(viewParams)
        self.innerGenerator = generator

    def initialize(self, batch_size):
        # Set batch size
        self.batch_size = batch_size

        # Determine which indices are relevant to us
        allSubjects = self.innerGenerator.getAllSubjects()
        # print("Number of subjects:", len(allSubjects))
        # print("Number of TOTAL samples:", self.innerGenerator.getTotalSampleCnt())
        self.validIndices = []
        for index in range(len(allSubjects)):
            subject = allSubjects[index]
            # print("SUBJECT:", subject)

            if subject in self.viewParams.selectedSubjects:
                # print("\tIN LIST:", self.viewParams.selectedSubjects)
                self.validIndices.append(index)

        print("VIEW INDEX COUNT:", len(self.validIndices))
        self.validIndices = np.array(self.validIndices)
        print("TYPE:", self.validIndices.dtype)

        # Set data preprocessor to None initially
        self.dataPreprocessor = None

        # Shuffle indices if necessary
        self.on_epoch_end()

    # METHODS ############################################################

    def getClassWeights(self):
        uniqueValueModes = self.getUniqueValueModes()
        labels = self.getEntireLabelset()
        classSampleCnts = countCurrentSamples(labels, self.getOutputCnt(), uniqueValueModes)
        totalSampleCnt = self.getTotalSampleCnt()
        # Based on: "Fusing Multilabel Deep Networks for Facial Action Unit Detection", FG2017
        if self.innerGenerator.params.desiredData == DATA_AU_OCC or self.innerGenerator.params.desiredData == DATA_AU_SM_OCC:
            sampleWeights = []
            for i in range(self.getOutputCnt()):
                if int(classSampleCnts[i][0]) > 0 and int(classSampleCnts[i][1]) > 0:
                    sampleWeights.append(float(classSampleCnts[i][0]) / float(classSampleCnts[i][1]))
                    # if (classSampleCnts[i][0] > classSampleCnts[i][1]):
                    #    sampleWeights.append(float(classSampleCnts[i][0])/float(classSampleCnts[i][1]))
                    # else:
                    #    sampleWeights.append(float(classSampleCnts[i][1])/float(classSampleCnts[i][0]))
                else:
                    sampleWeights.append(1.0)
        else:
            sampleWeights = [1.0] * int(self.getOutputCnt())

        print("SAMPLE WEIGHTS:", sampleWeights)
        return sampleWeights

    def setDataPreprocessor(self, dataPreprocessor):
        self.dataPreprocessor = dataPreprocessor

    def getDataPreprocessor(self):
        return self.dataPreprocessor

    def getEntireDataset(self):
        # Get data
        allData = self.innerGenerator.getEntireDataset()
        # Return valid subject
        return allData[self.validIndices]

    def getAllSubjects(self):
        # Get subjects
        allSubjects = self.innerGenerator.getAllSubjects()
        allSubjects = np.array(allSubjects)
        # Return valid subjects
        return allSubjects[self.validIndices]

    def getAllTasks(self):
        # Get tasks
        allTasks = self.innerGenerator.getAllTasks()
        allTasks = np.array(allTasks)
        # Return valid tasks
        return allTasks[self.validIndices]

    def getAllFilenames(self):
        # Get filenames
        allFilenames = self.innerGenerator.getAllFilenames()
        allFilenames = np.array(allFilenames)
        # Return valid filenames
        return allFilenames[self.validIndices]

    def loadEntireDataset(self):
        raise ValueError("NOT IMPLEMENTED")

    def loadEntireLabelset(self):
        raise ValueError("NOT IMPLEMENTED")

    def getEntireLabelset(self):
        # Get labels
        allLabels = self.innerGenerator.getEntireLabelset()
        # Return valid labels
        return allLabels[self.validIndices]

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.ceil(float(self.getTotalSampleCnt()) / float(self.batch_size)))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        totalSampleCnt = self.getTotalSampleCnt()
        if ((index + 1) * self.batch_size) <= totalSampleCnt:
            indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]
        else:
            indexes = self.indexes[index * self.batch_size:totalSampleCnt]

        # Generate data
        X, y = self.__data_generation(indexes)

        '''
        # Do we need dummy outputs?
        extraFakeOutputs = self.innerGenerator.params.extraFakeOutputs

        if len(extraFakeOutputs) > 0:
            print("Original y shape:", y.shape)

            newY = [y]
            for extra in extraFakeOutputs:
                newY.append(np.zeros([y.shape[0], y.shape[1]] + extra))
            y = newY

            print("New y shape:")
            for extra in newY:
                print("\t", extra.shape)
        '''

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(self.getTotalSampleCnt())
        if self.viewParams.doShuffleIndices == True:
            np.random.shuffle(self.indexes)

    def getUniqueValueModes(self):
        return self.innerGenerator.getUniqueValueModes()

    # Get total number of images we have
    def getTotalSampleCnt(self):
        return len(self.validIndices)

    def getOutputCnt(self):
        return self.innerGenerator.getOutputCnt()

    def getImageWidth(self):
        return self.innerGenerator.getImageWidth()

    def getImageHeight(self):
        return self.innerGenerator.getImageHeight()

    def getFrameCnt(self):
        return self.innerGenerator.getFrameCnt()

    def getNumChannels(self):
        return self.innerGenerator.getNumChannels()

    def toggleDataAugmentation(self, doDataAugmentation):
        self.viewParams.doDataAugmentation = doDataAugmentation

    def toggleShuffleIndices(self, doShuffleIndices):
        self.viewParams.doShuffleIndices = doShuffleIndices
        self.on_epoch_end()

    def getParams(self):
        return self.innerGenerator.getParams()

    def getViewParams(self):
        return copy.deepcopy(self.viewParams)

    def generateNetworkDataParams(self):
        return self.innerGenerator.generateNetworkDataParams()

    def getDesiredOutFormat(self):
        return self.innerGenerator.getDesiredOutFormat()

    def getFullImageDim(self):
        return self.innerGenerator.getFullImageDim()

    def getActivationFunction(self):
        return self.innerGenerator.getActivationFunction()

    def getEvaluationFunction(self):
        return self.innerGenerator.getEvaluationFunction()

    def getLabelValueBounds(self):
        return self.innerGenerator.getLabelValueBounds()

    def __data_generation(self, indexes):
        'Generates data containing batch_size samples'  # X : (n_samples, *dim, n_channels)
        # Initialization
        # print("**********************************************************************")
        # print("In data generation")
        # print("**********************************************************************")
        indexCnt = len(indexes)

        # Get the ACTUAL indices
        # print("indexes:", indexes)
        # print("valid:", self.validIndices)
        actualIndices = self.validIndices[indexes]

        # print("actualIndices:", actualIndices)

        # Get the whole dataset
        allData = self.innerGenerator.getEntireDataset()
        allLabels = self.innerGenerator.getEntireLabelset()

        # Get the batch
        X = allData[actualIndices]
        y = allLabels[actualIndices]

        # Do we have any preprocessing to do?
        if self.dataPreprocessor is not None:
            X = self.dataPreprocessor.preprocessSequence(X, self.viewParams.doDataAugmentation)

        # Crop video
        X = (self.innerGenerator).crop_video(X)

        return X, y

