# MIT LICENSE
#
# Copyright 2018 Michael J. Reale
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, print_function

import keras.utils
import numpy as np
from keras import backend as K
import tensorflow as tf
from keras.models import Sequential
from Network.BaseNetworkGenerator import *
from Data.DataLoadParams import *
from Data.BP4DDataGenerator import *
from Data.CASME2DataGenerator import *
from Data.DataPreprocessor import *
from Network.BaseNetworkParams import *
from Network.DRML.DRMLNetworkParams import *
from Network.SimpleNetwork import *
from Network.DRML.DRMLNetwork import *
from Training.TrainingParams import *
from Training.CommonFunctions import *
from Training.TrainingFunctions import *
from Data.FaceDataLoadParams import *
from Data.CASMEDataLoadParams import *
from Network.LocalSimpleNetwork import *

import copy


def main():
    print("Training LocalSimple Network...")

    # machine = MACHINE_LITTLE_BEAR
    machine = MACHINE_PCWEIQUAN  # FOR LOCAL
    desiredDatabase = DATABASE_CASME2_COLOR

    # Set up data parameters
    baseDataParams = CASMEDataLoadParams()
    baseDataParams.baseDir = getBaseDirectoryPath(machine, desiredDatabase)
    baseDataParams.baseGroundPath = getBaseGroundPath(machine, desiredDatabase)
    baseDataParams.lostFileList = getLostFileList(machine, desiredDatabase)
    baseDataParams.validSubjects = allSubjects_C2
    # baseDataParams.desiredClasses = allExp_C2 #allAU_C2 # TODO: ALSO TEST MICRO_EXP
    baseDataParams.desiredClasses = allExp_C2[1:]  # EXCEPT neutral
    baseDataParams.desiredData = DATA_MICRO_EXP  # DATA_AU_OCC
    baseDataParams.imageDataType = INPUT_DATATYPE_IMAGE
    baseDataParams.frameCnt = 5
    baseDataParams.desiredChannels = 3
    baseDataParams.samplingCount = 10  # 500
    baseDataParams.doDataBalancing = False
    baseDataParams.doDataAugmentation = True
    # baseDataParams.cropPercentage = 0.854
    baseDataParams.cropPercentage = 1.0

    baseDataParams.useCachedData = False

    # Get correct data generator type
    DataGeneratorType = getCorrectDataGeneratorType(desiredDatabase)

    # Set the batch size
    batchSize = 5

    # Load entire dataset ONCE
    allDataGen = DataGeneratorType(baseDataParams)  ######
    allDataGen.initialize(batchSize)  #######

    # Set data augmentation parameters
    useDataAugmentation = True

    # Set up training params
    trainParams = TrainingParams()
    trainParams.batchSize = 1
    trainParams.numEpochs = 10  # 200
    trainParams.visibleDevices = '0'
    trainParams.optimizerName = 'adam'  # SGD(lr=0.0001, momentum=0.9) # WARNING: Problems with Adam: https://github.com/pierluigiferrari/ssd_keras/issues/84 # Adam(lr=0.0001)
    trainParams.optimizerParams = dict(lr=0.0001)
    trainParams.loss = "categorical_crossentropy"  # TODO: Consider alternative ways to deal with loss
    trainParams.metrics = ['categorical_accuracy']
    trainParams.trainType = TRAIN_TYPE_PRE_TRAIN
    trainParams.totalFoldCnt = 3
    trainParams.totalTrialCnt = 5
    trainParams.TensorboardParams = dict(batch_size=trainParams.batchSize,
                                         write_graph=True,
                                         write_images=True)

    # Make data preprocessor params
    dataPreprocessParams = dict(
        # featurewise_center=True,
        # featurewise_std_normalization=True,
        samplewise_center=True,
        samplewise_std_normalization=True,
        rotation_range=5.,
        width_shift_range=0.073,
        height_shift_range=0.073,
        zoom_range=0.0,
        horizontal_flip=True)

    # Create network params
    # networkParams = DRMLNetworkParams()
    # networkParams.type = DRMLNetwork.NAME
    networkParams = BaseNetworkParams()
    networkParams.type = LocalSimpleNetwork.NAME
    networkParams.workers = 5
    networkParams.use_multiprocessing = True

    # Make network generator
    networkGenerator = BaseNetworkGenerator()

    # Get output directory
    outputDirectory = getExperimentOutputDirectory(machine,
                                                   desiredDatabase) + "/" + "DRML"  # str(datetime.datetime.now())

    # Actually perform training!
    allTrainResults, allValidResults, allTestResults = performTrainingUpdated(
        networkParams=networkParams,
        networkGenerator=networkGenerator,
        trainParams=trainParams,
        allDataGen=allDataGen,
        useDataAugmentation=useDataAugmentation,
        dataPreprocessParams=dataPreprocessParams,
        outputDirectory=outputDirectory,
        writeToExperimentDrive=True
    )


if __name__ == "__main__": main()

